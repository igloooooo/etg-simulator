import java.time.Clock

import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport
import playback.PlaybackCtrlActor
import playback.channel.ChannelActor
import service.ApplicationTimer
import simulator.command.CommandController
import simulator.stopnode.NodeController
import simulator.trip.{TripController, TripMonitorActor}

/**
  * module configuration for app
  */
class Module extends AbstractModule with AkkaGuiceSupport{

  override def configure() = {
    // Use the system clock as the default implementation of Clock
    bind(classOf[Clock]).toInstance(Clock.systemDefaultZone)
    // Ask Guice to create an instance of ApplicationTimer when the
    // application starts.
    bind(classOf[ApplicationTimer]).asEagerSingleton()

    /////////////////////////////////////////////////////////////
    //  Actors init
    /////////////////////////////////////////////////////////////
    bindActor[TripMonitorActor]("trip-monitor")
    bindActor[PlaybackCtrlActor]("etgPlayback")
    bindActor[TripController]("trip-controller")
    bindActor[NodeController]("node-controller")
    bindActor[CommandController]("command-controller")
  }

}
