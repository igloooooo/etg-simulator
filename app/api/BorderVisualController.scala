package api

import api.dto.{MarkLineResponse, SectorResponse}
import com.google.inject.Inject
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.dataset.{DataStore, PTSectorMap}

import scala.concurrent.ExecutionContext

class BorderVisualController @Inject() (dateStore: DataStore, cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){

  def sectorList(id:Int) = Action { implicit request =>

    Ok(Json.toJson(SectorResponse(PTSectorMap.findSectorListByBorder(id))))
  }
}
