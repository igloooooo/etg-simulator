package api

import com.google.inject.Inject
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.{ConflictService, PathService}
import service.network.NetworkStore

class ConflictController @Inject()(conflictService: ConflictService, cc: ControllerComponents, env: Environment) extends AbstractController(cc){
  def allConflictHandler() = Action { implicit request =>

    Ok(Json.toJson(conflictService.loadConflicts()))
  }
}
