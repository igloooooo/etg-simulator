package api

import javax.inject.Named

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import api.dto._
import com.google.inject.Inject
import org.joda.time.DateTime
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import playback.channel.{Channel, ConfigChannel, RequestChannelSOD, ResponseChannelSOD}
import wsclient.MiddlewareWSClient

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class FAConfig @Inject() (@Named("etgPlayback")ctrl: ActorRef, wSClient: MiddlewareWSClient, cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){
  implicit val timeout: Timeout = 5.seconds
  /**
    * this endpoint will accept the FA request and trigger FA response via data center API
    * @return
    */
  def faRequestHandler = Action.async { implicit request =>
    val req  = Json.fromJson[SODRequest](request.body.asJson.get).get
    val targetDate = DateTime.parse(req.requestDate)
    // ask status of channel
    (ctrl ? RequestChannelSOD(Channel.FARChannel, targetDate)).mapTo[ResponseChannelSOD].map(req =>
      if (req.startDate == null) {
        BadRequest("Play back has not start yet!")
      } else {
        val baselineDate = req.startDate.toString("yyyy-MM-dd")
        Ok(Json.toJson(SODResponse(targetDate.toString("yyyy-MM-dd"), req.startDate.toString("yyyy-MM-dd"), baselineDate)))
      }
    )
  }

  def loadFARDataSet = Action { implicit request =>
    val req  = Json.fromJson[DateSetRequest](request.body.asJson.get).get
    ctrl ! ConfigChannel(Channel.FARChannel, req.dataSetName, req.isLoad, req.totalCount, req.sentCount)
    if (req.isLoad) {
      // trigger JMS listener
      wSClient.client("api/v1/flow")
        .addQueryStringParameters("flowType"->"fa")
        .addQueryStringParameters("action" -> "start")
        .get()
    } else {
      // stop JMS listener
      wSClient.client("api/v1/flow")
        .addQueryStringParameters("flowType"->"fa")
        .addQueryStringParameters("action" -> "stop")
        .get()
    }

    Ok(Json.toJson(req))
  }

  def loadFAPDataSet = Action { implicit request =>
    val req  = Json.fromJson[DateSetRequest](request.body.asJson.get).get
    ctrl ! ConfigChannel(Channel.FAPChannel, req.dataSetName, req.isLoad, req.totalCount, req.sentCount)
    Ok(Json.toJson(req))
  }

}
