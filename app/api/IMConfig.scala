package api

import javax.inject.Named

import akka.actor.ActorRef
import api.dto._
import com.google.inject.Inject
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import playback.channel.{Channel, ConfigChannel}

class IMConfig @Inject() (@Named("etgPlayback")ctrl: ActorRef, cc: ControllerComponents, env: Environment) extends AbstractController(cc){

  def loadDataSet = Action { implicit request =>
    val req  = Json.fromJson[DateSetRequest](request.body.asJson.get).get
    ctrl ! ConfigChannel(Channel.IMChannel, req.dataSetName, req.isLoad, req.totalCount, req.sentCount)
    Ok(Json.toJson(req))
  }
}
