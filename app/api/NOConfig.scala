package api

import javax.inject.Named

import akka.actor.ActorRef
import api.dto._
import com.google.inject.Inject
import org.joda.time.DateTime
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import playback.channel.{Channel, ConfigChannel, RequestChannelSOD, ResponseChannelSOD}
import akka.pattern.ask
import akka.util.Timeout
import service.dataset.OutAgeStore
import util.PlaybackDateUtil
import wsclient.MiddlewareWSClient

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._


class NOConfig @Inject() (@Named("etgPlayback")ctrl: ActorRef, wSClient: MiddlewareWSClient, cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){
  implicit val timeout: Timeout = 5.seconds
  /**
    * this endpoint will accept the NO request and trigger NO response via data center API
    * @return
    */
  def noRequestHandler = Action.async { implicit request =>
    val faReq  = Json.fromJson[SODRequest](request.body.asJson.get).get
    val targetDate = DateTime.parse(faReq.requestDate)
    // ask status of channel
    (ctrl ? RequestChannelSOD(Channel.NOChannel, targetDate)).mapTo[ResponseChannelSOD].map(req =>
      if (req.startDate == null) {
        BadRequest("Play back has not start yet!")
      } else {
        val baselineDate = req.startDate.toString("yyyy-MM-dd")
        Ok(Json.toJson(SODResponse(targetDate.toString("yyyy-MM-dd"), req.startDate.toString("yyyy-MM-dd"), baselineDate)))
      }
      )
  }

  def loadDataSet = Action { implicit request =>
    val req  = Json.fromJson[DateSetRequest](request.body.asJson.get).get
    ctrl ! ConfigChannel(Channel.NOChannel, req.dataSetName, req.isLoad, req.totalCount, req.sentCount)
    // convert dataSet name to time
    val startDate = DateTime.parse(req.dataSetName).withTimeAtStartOfDay()
    val endDate = startDate.minusDays(-1)
    val targetDate = DateTime.now()

    if (req.isLoad) {
      // trigger JMS listener
      wSClient.client("api/v1/flow")
        .addQueryStringParameters("flowType"->"no")
        .addQueryStringParameters("action" -> "start")
        .get()

      // load no data
      wSClient.client("api/v1/no/sod")
        .addQueryStringParameters("startDate" -> PlaybackDateUtil.convertToDBFormat(startDate))
        .addQueryStringParameters("endDate" -> PlaybackDateUtil.convertToDBFormat(endDate))
        .addQueryStringParameters("targetDate" -> PlaybackDateUtil.convertToDBFormat(targetDate))
        .addQueryStringParameters("baselineDate" -> PlaybackDateUtil.convertToDBFormat(startDate))
        .withRequestTimeout(30000.millis)
        .get()
        .map {
          response =>
            val networkOutageResponse = Json.fromJson[NetworkOutageResponse](response.json).get
            //remove the timetable
            OutAgeStore.removeOutAges
            // load new timetable
            networkOutageResponse.noItems.foreach(noItem => {
              OutAgeStore.addOutAgeLink(noItem)
            })
        }
    } else {
      // stop JMS listener
      wSClient.client("api/v1/flow")
        .addQueryStringParameters("flowType"->"no")
        .addQueryStringParameters("action" -> "stop")
        .get()
    }
    Ok(Json.toJson(req))
  }
}
