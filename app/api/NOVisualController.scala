package api

import com.google.inject.Inject
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.dataset.{DataStore, OutAgeStore, PTStore}

import scala.concurrent.ExecutionContext

class NOVisualController @Inject() (dateStore: DataStore, cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){
  def networkOutageHandler(id:Int) = Action { implicit request =>
    Ok(Json.toJson(OutAgeStore.findOutAgeByBorder(id)))
  }

}
