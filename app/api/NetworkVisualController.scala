package api

import javax.inject.Named

import akka.actor.ActorRef
import com.google.inject.Inject
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.{NodeService, PathService}
import service.dataset.PTSectorMap
import service.network.{NetworkLinkMap, NetworkNodeMap, NetworkStore, NodeGeoMap}

class NetworkVisualController @Inject()(networkStore:NetworkStore, pathService:PathService, nodeService: NodeService, cc: ControllerComponents, env: Environment) extends AbstractController(cc){
  def nodeHandler() = Action { implicit request =>

    Ok(Json.toJson(NetworkNodeMap.networkNodeSet.toSeq))
  }

  def platformLabels() = Action { implicit request =>

    Ok(Json.toJson(NetworkNodeMap.generatePlatformLabels))
  }

  def linkHandler() = Action { implicit request =>

    Ok(Json.toJson(NetworkLinkMap.getAllRunningLine().toSeq))
  }

  def nodeDetails(stopRef:String) = Action { implicit request =>
    val nodeDetails = PTSectorMap.findNodeDetails(stopRef)
    if (nodeDetails.isDefined) {
      Ok(Json.toJson(nodeDetails.get))
    } else {
      BadRequest("")
    }
  }

  def loadGeoNodes() = Action { implicit request =>
    Ok(Json.toJson(NodeGeoMap.loadGeoNodes()))
  }

  def findPathHandler(fromNodeId:String, toNodeId:String) = Action { implicit request =>

    Ok(Json.toJson(pathService.findPath(fromNodeId, toNodeId).toSeq))
  }

  def findTripPathHandler(tripId:String) = Action { implicit request =>

    Ok(Json.toJson(pathService.findTripPath(tripId).toSeq))
  }

  def changePathOnNodeHandler(oldStartNodeId:String, nextNodeId:String, endNodeId:String) = Action { implicit request =>

    Ok(Json.toJson(pathService.reRoutePath(oldStartNodeId, nextNodeId, endNodeId)))
  }

  def changeTripPathOnNodeHandler(tripId:String, oldStartNodeId:String, nextNodeId:String, endNodeId:String) = Action { implicit request =>

    Ok(Json.toJson(pathService.reRouteTrip(tripId, oldStartNodeId, nextNodeId, endNodeId)))
  }

  def loadBlockNodes = Action { implicit request =>
    Ok(Json.toJson(nodeService.loadAllBlockNodes()))
  }

}
