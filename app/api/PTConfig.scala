package api

import javax.inject.Named

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import api.dto._
import com.google.inject.Inject
import org.joda.time.DateTime
import play.Environment
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import playback.channel.{Channel, ConfigChannel, RequestChannelSOD, ResponseChannelSOD}
import service.LoadDataSetService
import service.dataset.{PTOrderedCallDTO, PTStore}
import simulator.trip.TripController.RequestTrackTrip
import util.PlaybackDateUtil
import wsclient.MiddlewareWSClient

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

class PTConfig @Inject() (@Named("etgPlayback")ctrl: ActorRef, @Named("trip-controller")tripCtrl: ActorRef, wSClient: MiddlewareWSClient, loadDataSetService:LoadDataSetService, cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){
  implicit val timeout: Timeout = 5.seconds
  /**
    * this endpoint will accept the PT request and trigger PT response via data center API
    * @return
    */
  def ptRequestHandler = Action.async { implicit request =>
    val req  = Json.fromJson[SODRequest](request.body.asJson.get).get
    val targetDate = DateTime.parse(req.requestDate)
    // ask status of channel
    (ctrl ? RequestChannelSOD(Channel.PTChannel, targetDate)).mapTo[ResponseChannelSOD].map(req =>
      if (req.startDate == null) {
        BadRequest("Play back has not start yet!")
      } else {
        val baselineDate = req.startDate.toString("yyyy-MM-dd")
        Ok(Json.toJson(SODResponse(targetDate.toString("yyyy-MM-dd"), req.startDate.toString("yyyy-MM-dd"), baselineDate)))
      }
    )
  }

  /**
    * this api will list all possible dataset
    * @return
    */
  def requestAllDataSet = Action.async { implicit request =>
    val listDataSet = loadDataSetService.listDataSet()
    Future.sequence(List(listDataSet)) map { docLists =>
      // success
      Ok(Json.toJson(docLists.flatten))
    } recover {
      case e =>
        Logger.error("FAIL: " + e.getMessage)
        BadRequest("FAIL")
    }

  }

  def loadDataSet = Action.async { implicit request =>
    val req  = Json.fromJson[DateSetRequest](request.body.asJson.get).get
    ctrl ! ConfigChannel(Channel.PTChannel, req.dataSetName, req.isLoad, req.totalCount, req.sentCount)
    // convert dataSet name to time
    val startDate = DateTime.parse(req.dataSetName).withTimeAtStartOfDay()
    val endDate = startDate.minusDays(-1).minusMinutes(1)
    val targetDate = DateTime.now()
    if (req.isLoad) {
//      // trigger JMS listener
//      wSClient.client("api/v1/flow")
//        .addQueryStringParameters("flowType"->"pt")
//        .addQueryStringParameters("action" -> "start")
//        .get()
      // load pt data
      val ptResult = loadPTData(targetDate, startDate, endDate)
      ptResult.map(status => Ok(Json.toJson(req)))
    } else {
      Future {
        // stop JMS listener
        wSClient.client("api/v1/flow")
          .addQueryStringParameters("flowType"->"pt")
          .addQueryStringParameters("action" -> "stop")
          .get()
        //remove the timetable
        PTStore.removeTimeTable
        Ok(Json.toJson(req))
      }
    }

  }

  private def loadPTData(targetDate:DateTime, startDate:DateTime, endDate:DateTime): Future[Int] = {
    // load pt data
    wSClient.client("api/v1/pt/sod")
      .addQueryStringParameters("startDate" -> PlaybackDateUtil.convertToDBFormat(startDate))
      .addQueryStringParameters("endDate" -> PlaybackDateUtil.convertToDBFormat(endDate))
      .addQueryStringParameters("targetDate" -> PlaybackDateUtil.convertToDBFormat(targetDate))
      .addQueryStringParameters("baselineDate" -> PlaybackDateUtil.convertToDBFormat(startDate))
      .withRequestTimeout(1200000.millis)
      .get()
      .map {
        response =>
          val timeTableResponse = Json.fromJson[TimeTableResponse](response.json).get
          //remove the timetable
          PTStore.removeTimeTable
          // load new timetable
          timeTableResponse.trips.foreach(tripItem => {
            val trainType = tripItem.trainType
            val tripID = tripItem.trip + "_" + PlaybackDateUtil.convertToDate(targetDate) //TODO: need have way to generate tripID
            tripItem.calls
              .filter(p => p.a.isDefined && p.d.isDefined)
              .foreach(callItem => {
                val arrivalDTO = PTOrderedCallDTO(
                  tripId = tripID,
                  trip = tripItem.trip,
                  trainType = trainType,
                  time = callItem.a.get,
                  stopRef = callItem.s,
                  value = 0,
                  displayTime = "",
                  tripItem.speedBandName,
                  callType = 0
                )
                val departureDTO = PTOrderedCallDTO(
                  tripId = tripID,
                  trip = tripItem.trip,
                  trainType = trainType,
                  time = callItem.d.get,
                  stopRef = callItem.s,
                  value = 0,
                  displayTime = "",
                  tripItem.speedBandName,
                  callType = 1
                )
                // add arrival
                PTStore.addCall(arrivalDTO)
                // add departure
                PTStore.addCall(departureDTO)

                // send trip register to trip controller
                tripCtrl ! RequestTrackTrip("etg", tripID, tripItem.trip, callItem.a.get, arrivalDTO, departureDTO)
              })
          })
          Logger.info("load PT dataset: " + startDate)
          response.status
      }
  }
}
