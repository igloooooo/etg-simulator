package api

import javax.inject.Named

import api.dto.MarkLineResponse
import com.google.inject.Inject
import org.joda.time.DateTime
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.MarkLineService
import service.dataset.{DataStore, PTSectorMap, PTStore}

import scala.concurrent.ExecutionContext

class PTVisualController @Inject() (dateStore: DataStore, markLineService: MarkLineService,cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){

  def timeTableHandler(id:Int, startDate:Option[String], endDate:Option[String]) = Action { implicit request =>
    val queryStartTime = startDate.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endDate.map(DateTime.parse(_)).getOrElse(DateTime.now())
    Ok(Json.toJson(PTStore.findByBorderId(id, queryStartTime, queryEndTime)))
  }

  def timeTableBySectorHandler(borderId:Int, sectorIndex:Int, startDate:Option[String], endDate:Option[String]) = Action { implicit request =>
    val queryStartTime = startDate.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endDate.map(DateTime.parse(_)).getOrElse(DateTime.now())
    Ok(Json.toJson(PTStore.findByBorderAndSector(borderId, sectorIndex, queryStartTime, queryEndTime)))
  }

  def timeTableBySectorWithTripsHandler(borderId:Int, sectorIndex:Int, startDate:Option[String], endDate:Option[String], tripIdList:List[String]) = Action { implicit request =>
    val queryStartTime = startDate.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endDate.map(DateTime.parse(_)).getOrElse(DateTime.now())
    Ok(Json.toJson(PTStore.findByBorderAndSector(borderId, sectorIndex, queryStartTime, queryEndTime).filter(dto => tripIdList.contains(dto.tripId))))
  }

  def markLineHandler(id:Int) = Action { implicit request =>

    Ok(Json.toJson(MarkLineResponse(PTSectorMap.findMasterNodeByBorder(id))))
  }

  def convertToPTCallItem(borderId:Int, sectorId:Int, tripId:String, value:Int) = Action { implicit request =>
    val callItem = markLineService.convertMarkLineToCallItem(borderId, sectorId, tripId, value)
    callItem match {
      case Some(call) => Ok(Json.toJson(call))
      case _ => BadRequest("can not find node")
    }
  }

  def generateMarkLineByTrip(trip:String) = Action { implicit request =>

    Ok(Json.toJson(MarkLineResponse(Seq(PTSectorMap.findMarkLineByTrip(trip)))))
  }

  def generateRelatedTrips(trip:String) = Action { implicit request =>

    Ok(Json.toJson(PTStore.findRelatedTrip(trip)))
  }
}
