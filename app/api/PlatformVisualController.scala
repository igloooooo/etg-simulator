package api

import com.google.inject.Inject
import org.joda.time.DateTime
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.PlatformService

import scala.concurrent.ExecutionContext

class PlatformVisualController @Inject() (platformService:PlatformService, cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){

  def loadPlatFormStatus(masterNode:String, startTime:Option[String], endTime:Option[String]) = Action { implicit request =>
    val queryStartTime = startTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    Ok(Json.toJson(platformService.loadPlatFormStatus(masterNode, queryStartTime, queryEndTime)))
  }

  def loadPlatFormLane(stopRef:String) = Action { implicit request =>
    Ok(Json.toJson(platformService.loadPlatFormLane(stopRef)))
  }

  def changePlatform(tripId:String, originalPlatform:String, targetPlatform:String) = {

  }
}
