package api

import com.google.inject.Inject
import org.joda.time.DateTime
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.StatisticService
import service.dataset.DataStore

import scala.concurrent.ExecutionContext

class StatisticController @Inject() (dateStore: DataStore, statisticService: StatisticService, cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){

  def currentDelay(startTime:Option[String], endTime:Option[String]) = Action { implicit request =>
    val queryStartTime = startTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    Ok(Json.toJson(statisticService.findCurrentDelay(queryStartTime, queryEndTime)))
  }

  def currentSkipStop(startTime:Option[String], endTime:Option[String]) = Action { implicit request =>
    val queryStartTime = startTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    Ok(Json.toJson(statisticService.findCurrentSkipStops(queryStartTime, queryEndTime)))
  }

  def currentKPI(startTime:Option[String], endTime:Option[String]) = Action { implicit request =>
    val queryStartTime = startTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    Ok(Json.toJson(statisticService.loadCurrentKPI(queryStartTime, queryEndTime)))
  }

  def currentDelayAndSkip(startTime:Option[String], endTime:Option[String]) = Action { implicit request =>
    val queryStartTime = startTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    Ok(Json.toJson(statisticService.findCurrentDelayAndSkipStopStatis(queryStartTime, queryEndTime)))
  }

  def totalDelayAndSkip() = Action { implicit request =>
    Ok(Json.toJson(statisticService.findTotalDelayAndSkipStopStatis()))
  }

  def loadVMAndTripsCountPerMinute() = Action { implicit request =>
    Ok(Json.toJson(statisticService.loadVMAndTripsCountPerMinute()))
  }

  def totalRunStatus(startTime:Option[String], endTime:Option[String]) = Action { implicit request =>
    val queryStartTime = startTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    Ok(Json.toJson(statisticService.findTotalRunStatus(queryStartTime, queryEndTime)))
  }
}
