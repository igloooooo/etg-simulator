package api

import com.google.inject.Inject
import org.joda.time.DateTime
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.TripService
import service.dataset.{DataStore, VMStore}

import scala.concurrent.ExecutionContext

class TripController @Inject() (dateStore: DataStore, tripService:TripService, cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){

  def tripStatus(tripId:String) = Action { implicit request =>
    val status = tripService.findTripStatus(tripId)
    if(status.isDefined) {
      Ok(Json.toJson(status))
    } else {
      BadRequest("trip doesn't exist.")
    }
  }

  def tripDetails(tripId:String) = Action { implicit request =>
    val details = tripService.findTripDetails(tripId)
    if(details.isDefined) {
      Ok(Json.toJson(details.get))
    } else {
      BadRequest("trip doesn't exist.")
    }
  }

  def tripPlatformDetails(tripId:String) = Action { implicit request =>
    val details = tripService.findTripPlatformDetails(tripId)
    if(details.isDefined) {
      Ok(Json.toJson(details.get))
    } else {
      BadRequest("trip doesn't exist.")
    }
  }
}
