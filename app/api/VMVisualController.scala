package api

import com.google.inject.Inject
import org.joda.time.DateTime
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.dataset.{DataStore, PTStore, VMStore}
import service.network.NetworkLinkMap

import scala.concurrent.ExecutionContext

class VMVisualController @Inject() (dateStore: DataStore, cc: ControllerComponents, env: Environment)(implicit ec: ExecutionContext) extends AbstractController(cc){

  def incrementVMByBoardId(id:Int, startTime:Option[String], endTime:Option[String]) = Action { implicit request =>
    val queryStartTime = startTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endTime.map(DateTime.parse(_)).getOrElse(DateTime.now())

    Ok(Json.toJson(VMStore.findByBorderId(id, queryStartTime, queryEndTime)))
  }

  def incrementVMByTrip(trip:String, startTime:Option[String], endTime:Option[String]) = Action { implicit request =>
    val queryStartTime = startTime.map(DateTime.parse(_)).getOrElse(DateTime.now())
    val queryEndTime = endTime.map(DateTime.parse(_)).getOrElse(DateTime.now())

    Ok(Json.toJson(VMStore.findByTripAndRelatedTrips(trip, queryStartTime, queryEndTime)))
  }

  def loadVMForNetworkHandler() = Action { implicit request =>

    Ok(Json.toJson(VMStore.loadVMForNetwork))
  }

  def loadVMForNetwork3dHandler() = Action { implicit request =>

    Ok(Json.toJson(VMStore.loadVMForNetwork3d))
  }


}
