package api.dto

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

object ChannelAction {
  implicit val actionReads = Json.reads[ChannelAction]
  implicit val actionWrite = Json.writes[ChannelAction]

}

case class ChannelAction(actionType:String, startTime:DateTime, endTime:DateTime, startDate:DateTime, endDate:DateTime, speed:Int)


