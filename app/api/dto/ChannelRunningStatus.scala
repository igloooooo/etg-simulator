package api.dto

import play.api.libs.json.Json

object ChannelRunningStatus {
  implicit val residentReads = Json.reads[ChannelRunningStatus]
  implicit val residentWrite = Json.writes[ChannelRunningStatus]
}

case class ChannelRunningStatus(name:String, dataSetName:String) {

}
