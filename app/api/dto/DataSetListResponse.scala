package api.dto

import play.api.libs.json.Json

object DataSetListResponse {

}

case class DataSetListResponse()

object DataSetListItem {
  implicit val residentReads = Json.reads[DataSetListItem]
  implicit val residentWrite = Json.writes[DataSetListItem]
}

case class DataSetListItem(name:String, count:Int, messageType:Option[String])