package api.dto

import play.api.libs.json.Json

object DateSetRequest{
  implicit val residentReads = Json.reads[DateSetRequest]
  implicit val residentWrite = Json.writes[DateSetRequest]
}
case class DateSetRequest (dataSetName:String, isLoad:Boolean, totalCount:Int, sentCount:Int)
