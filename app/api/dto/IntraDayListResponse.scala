package api.dto

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

object IntraDayListResponse {

  implicit val residentReads = Json.reads[IntraDayListResponse]
  implicit val residentWrite = Json.writes[IntraDayListResponse]
}

case class IntraDayListResponse(id:String, timestamp:DateTime)
