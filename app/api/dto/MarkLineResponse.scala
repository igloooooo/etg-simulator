package api.dto

import play.api.libs.json.Json

object MarkLineItem {
  implicit val residentReads = Json.reads[MarkLineItem]
  implicit val residentWrite = Json.writes[MarkLineItem]
}
case class MarkLineItem(yAxis:Int, index:Int, name: String, KMPost: Double, masterNode:String)

object MarkLineResponse{
  implicit val residentReads = Json.reads[MarkLineResponse]
  implicit val residentWrite = Json.writes[MarkLineResponse]
}

case class MarkLineResponse(lines: Seq[Seq[MarkLineItem]])
