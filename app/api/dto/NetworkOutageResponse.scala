package api.dto

import org.joda.time.DateTime
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._
import play.api.libs.json.Json

object OutAgeLinkDTO{
  implicit val residentReads = Json.reads[OutAgeLinkDTO]
  implicit val residentWrite = Json.writes[OutAgeLinkDTO]
}
case class OutAgeLinkDTO(requestedStartDateTime: DateTime,
                         requestedEndDateTime: DateTime,
                         approvedStartDateTime: DateTime,
                         approvedEndDateTime: DateTime,
                         id: String,
                         name: Option[String],
                         comments: String,
                         fromNode: String,
                         toNode: String)

object NetworkOutageResponse{
  implicit val residentReads = Json.reads[NetworkOutageResponse]
  implicit val residentWrite = Json.writes[NetworkOutageResponse]
}
case class NetworkOutageResponse(noItems:List[OutAgeLinkDTO])
