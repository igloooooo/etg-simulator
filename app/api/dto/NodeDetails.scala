package api.dto

import play.api.libs.json.Json

object NodeDetails {
  implicit val residentReads = Json.reads[NodeDetails]
  implicit val residentWrite = Json.writes[NodeDetails]
}

case class NodeDetails(NodeID: String,
                       NodeName: String,
                       NodeDescription: String,
                       NetworkLevel: String,
                       ParentNodeID: String,
                       KMPost: Double)
