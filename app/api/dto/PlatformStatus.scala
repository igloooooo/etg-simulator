package api.dto

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

object PlatformStatus {
  implicit val residentReads = Json.reads[PlatformStatus]
  implicit val residentWrite = Json.writes[PlatformStatus]
}

case class PlatformStatus(stopRef:String, longName:String, tripId:String, trip:String, startTime:Long, endTime:Long, duration:Long, isActual:Boolean)

object PlatformLane {
  implicit val residentReads = Json.reads[PlatformLane]
  implicit val residentWrite = Json.writes[PlatformLane]
}

case class PlatformLane(stopRef:String, longName:String)

