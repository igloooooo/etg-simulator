package api.dto

import play.api.libs.json.Json

object SODRequest {
  implicit val residentReads = Json.reads[SODRequest]
  implicit val residentWrite = Json.writes[SODRequest]
}
case class SODRequest(requestDate:String)

object SODResponse {
  implicit val residentReads = Json.reads[SODResponse]
  implicit val residentWrite = Json.writes[SODResponse]
}

/**
  *
  * @param targetDate targetDate, will be equal request date for time being
  * @param originalDate for db query
  */
case class SODResponse(targetDate:String, originalDate:String, baselineDate:String)
