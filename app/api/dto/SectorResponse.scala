package api.dto

import play.api.libs.json.Json

object SectorItem {
  implicit val residentReads = Json.reads[SectorItem]
  implicit val residentWrite = Json.writes[SectorItem]
}

case class SectorItem(boardId:Int, index:Int, name:String, key:String)

object SectorResponse {
  implicit val residentReads = Json.reads[SectorResponse]
  implicit val residentWrite = Json.writes[SectorResponse]
}

case class SectorResponse(sectors:Seq[SectorItem])
