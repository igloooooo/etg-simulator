package api.dto

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

object PTCallItem {
  implicit val residentReads = Json.reads[PTCallItem]
  implicit val residentWrite = Json.writes[PTCallItem]
}

/**
  *
  * @param a arrival
  * @param d departure
  * @param s stopRef
  */
case class PTCallItem(a: Option[DateTime], d:Option[DateTime], s:String)

object PTTripItem {
  implicit val residentReads = Json.reads[PTTripItem]
  implicit val residentWrite = Json.writes[PTTripItem]
}

case class PTTripItem(trip:String, tripDate:String, trainType:String, speedBandName:String, isUp:Boolean, calls:List[PTCallItem])


object TimeTableRequest{
  implicit val residentReads = Json.reads[TimeTableRequest]
  implicit val residentWrite = Json.writes[TimeTableRequest]
}

case class TimeTableRequest(targetDate:DateTime)

object TimeTableResponse{
  implicit val residentReads = Json.reads[TimeTableResponse]
  implicit val residentWrite = Json.writes[TimeTableResponse]
}

case class TimeTableResponse(trips:List[PTTripItem])




