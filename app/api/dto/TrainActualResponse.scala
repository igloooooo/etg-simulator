package api.dto

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

object TrainActualDto {
  implicit val residentReads = Json.reads[TrainActualDto]
  implicit val residentWrite = Json.writes[TrainActualDto]
}

/**
  *
  * @param trip
  * @param time
  * @param actualType 0 - arrival 1 - departure
  * @param stopRef
  */
case class TrainActualDto(trip:String, time:DateTime, actualType:Int, stopRef:String)

object TrainActualResponse {
  implicit val residentReads = Json.reads[TrainActualResponse]
  implicit val residentWrite = Json.writes[TrainActualResponse]
}

case class TrainActualResponse(vm:List[TrainActualDto])


