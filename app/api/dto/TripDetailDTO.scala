package api.dto

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._



object TripDetailItem {
  implicit val residentReads = Json.reads[TripDetailItem]
  implicit val residentWrite = Json.writes[TripDetailItem]
}

case class TripDetailItem(timestamp:DateTime, stopRef:String, movementType:Int)

object TripDetailDTO {
  implicit val residentReads = Json.reads[TripDetailDTO]
  implicit val residentWrite = Json.writes[TripDetailDTO]
}

case class TripDetailDTO(tripId:String, trip:String, calls:Seq[TripDetailItem])


object TripPlatformDetailItem {
  implicit val residentReads = Json.reads[TripPlatformDetailItem]
  implicit val residentWrite = Json.writes[TripPlatformDetailItem]
}

case class TripPlatformDetailItem(stopRef:String, platform:String,
                                  plannedArrival:Option[DateTime], plannedDeparture:Option[DateTime],
                                  actualArrival:Option[DateTime], actualDeparture:Option[DateTime])



object TripPlatformDetailDTO {
  implicit val residentReads = Json.reads[TripPlatformDetailDTO]
  implicit val residentWrite = Json.writes[TripPlatformDetailDTO]
}
case class TripPlatformDetailDTO(tripId:String, trip:String, calls:Seq[TripPlatformDetailItem])


