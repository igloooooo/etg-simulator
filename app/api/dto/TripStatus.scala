package api.dto

import play.api.libs.json.Json

object TripStatus {
  implicit val residentReads = Json.reads[TripStatus]
  implicit val residentWrite = Json.writes[TripStatus]
}

case class TripStatus (trip: String, delay: Int, lastUpdateTime:String, status:String)
