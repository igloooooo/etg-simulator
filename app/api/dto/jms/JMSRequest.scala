package api.dto.jms

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

trait JMSRequest {

}

object VMJMSRequest {
  implicit val actionReads = Json.reads[VMJMSRequest]
  implicit val actionWrite = Json.writes[VMJMSRequest]
}

case class VMJMSRequest(startTime:DateTime, endTime:DateTime, tripList:List[String])
