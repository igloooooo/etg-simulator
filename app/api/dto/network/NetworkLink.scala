package api.dto.network

import play.api.libs.json.Json

object VerticesNode{
  implicit val actionReads = Json.reads[VerticesNode]
  implicit val actionWrite = Json.writes[VerticesNode]
}
case class VerticesNode(LocX: Int, LocY:Int, seq:Int)

object NetworkLink {
  implicit val actionReads = Json.reads[NetworkLink]
  implicit val actionWrite = Json.writes[NetworkLink]
}
case class NetworkLink(fromNode: String, toNode: String, fromLocX:Int, fromLocY:Int, toLocX:Int, toLocY:Int, isRunningLine:Boolean, vertices: Seq[VerticesNode] = Seq(), valid: Boolean = true)

