package api.dto.network

import play.api.libs.json.Json

object NetworkNode {
  implicit val actionReads = Json.reads[NetworkNode]
  implicit val actionWrite = Json.writes[NetworkNode]
}

case class NetworkNode(shortName: String, longName:String, locX:Int, locY:Int, isJunction:Boolean = false, isPlatform:Boolean = false)

object NetworkPlatformLabel {
  implicit val actionReads = Json.reads[NetworkPlatformLabel]
  implicit val actionWrite = Json.writes[NetworkPlatformLabel]
}

case class NetworkPlatformLabel(shortName: String, longName:String, locX:Int, locY:Int, rotate:Int)

object NodeGeo {
  implicit val actionReads = Json.reads[NodeGeo]
  implicit val actionWrite = Json.writes[NodeGeo]
}

case class NodeGeo(locationName:String,	systemName:String,	longitude:Float, 	latitude:Float,	nodeName:String)
