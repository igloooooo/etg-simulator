package api.dto.network

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

object PTNetworkDTO {
  implicit val actionReads = Json.reads[PTNetworkDTO]
  implicit val actionWrite = Json.writes[PTNetworkDTO]
}

case class PTNetworkDTO(tripId:String,
                        trip:String,
                        stopRef: String,
                        locX:Int,
                        locY:Int,
                        availableLinks:List[NetworkLink],// exclude next node and previous node
                        departureFromNode:DateTime,
                        arrivalToNode:DateTime,
                        isVertices: Boolean,
                        isChanged:Boolean = false)
