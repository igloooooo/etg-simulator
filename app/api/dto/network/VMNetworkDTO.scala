package api.dto.network

import play.api.libs.json.Json

object VMNetworkDTO {
  implicit val actionReads = Json.reads[VMNetworkDTO]
  implicit val actionWrite = Json.writes[VMNetworkDTO]
}

case class VMNetworkDTO(trip:String, fromNode: String, toNode: String, vertices: Seq[VerticesNode] = Seq(), delay:Int = 0)

object VMNetwork3dDTO {
  implicit val actionReads = Json.reads[VMNetwork3dDTO]
  implicit val actionWrite = Json.writes[VMNetwork3dDTO]
}

case class VMNetwork3dDTO(trip:String,
                          fromNode: String, toNode: String,
                          fromLon:Float, fromLat:Float,
                          toLon:Float, toLat:Float,
                          delay:Int = 0)
