package api.dto.statis

import play.api.libs.json.Json

object KPIDto {
  implicit val residentReads = Json.reads[KPIDto]
  implicit val residentWrite = Json.writes[KPIDto]
}

case class KPIDto(tripCount:Int, vmInLastTenSec:Int, delayCount:Int, skipStopCount:Int)
