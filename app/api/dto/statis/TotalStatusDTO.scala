package api.dto.statis

import play.api.libs.json.Json

object TotalStatusDTO {
  implicit val residentReads = Json.reads[TotalStatusDTO]
  implicit val residentWrite = Json.writes[TotalStatusDTO]
}

case class TotalStatusDTO(totalTrips:Int, currentTrips:Int, totalDelayTrips:Int, currentDelayTrips:Int, totalSkipStops:Int, currentSkipStops:Int)
