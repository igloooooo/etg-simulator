package api.dto.statis

import play.api.libs.json.Json

object VMAndTripCountPerMinDTO {
  implicit val residentReads = Json.reads[VMAndTripCountPerMinDTO]
  implicit val residentWrite = Json.writes[VMAndTripCountPerMinDTO]
}

/**
  *
  * @param tripCount list of pair (interval, count)
  * @param vmCount list of pair (interval, count)
  */
case class VMAndTripCountPerMinDTO(tripCount:List[List[Long]], vmCount:List[List[Long]])
