package api.dto.statis

import play.api.libs.json.Json

object VMDelayAndSkipStopStatisDTO{
  implicit val residentReads = Json.reads[VMDelayAndSkipStopStatisDTO]
  implicit val residentWrite = Json.writes[VMDelayAndSkipStopStatisDTO]
}
case class VMDelayAndSkipStopStatisDTO(borderId:Int, delayCount:List[Int], skipStopCount:List[Int])
