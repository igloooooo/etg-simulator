package controllers

import java.io.File

import com.google.inject.Inject
import com.typesafe.config.{Config, ConfigFactory}
import lib.WebpackBuildFile
import play.Environment
import play.api.Configuration
import play.api.mvc._

class FrontController @Inject() (cc: ControllerComponents, env: Environment, config: Configuration) extends AbstractController(cc){
  val frontConfig: Config = ConfigFactory.parseFile(new File("conf/frontend.conf")).resolve()

  def index = Action {
    val port = if (env.isDev) frontConfig.getInt("webpack.port") else 8080
    val apiHost = config.get[String]("etg.api.host")
    val apiPort = config.get[Int]("etg.api.port")
    val wsHost = config.get[String]("etg.ws.host")
    val wsPort = config.get[Int]("etg.ws.port")
    Ok(views.html.index.render(env, port, WebpackBuildFile.jsBundle(env), WebpackBuildFile.cssBundle(env), apiHost, apiPort, wsHost, wsPort))
  }
}
