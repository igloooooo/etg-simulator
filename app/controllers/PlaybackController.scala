package controllers

import javax.inject.Named

import akka.actor.ActorRef
import api.dto.ChannelAction
import com.google.inject.Inject
import play.Environment
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import playback.channel.{PausePlay, SpeedUp, StartPlay, StopPlay}
import play.api.Logger

class PlaybackController @Inject() (@Named("etgPlayback")ctrl: ActorRef, cc: ControllerComponents, env: Environment) extends AbstractController(cc){
  val logger: Logger = Logger(this.getClass)
  def actionHandler = Action { implicit request =>
    val action  = Json.fromJson[ChannelAction](request.body.asJson.get).get
    action.actionType match {
      case "Play" =>
        ctrl ! StartPlay(startDate = action.startDate, endDate = action.endDate, startTime = action.startTime, endTime = action.endTime)
      case "Pause" =>
        ctrl ! PausePlay()
      case "Stop" =>
        ctrl ! StopPlay()
      case "SpeedX" =>
        ctrl ! SpeedUp(action.speed)
      case _ =>
        logger.logger.warn("Unknow Action: {}", action.actionType)
    }
    Ok(Json.toJson(action))
  }

}
