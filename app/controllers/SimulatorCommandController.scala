package controllers

import javax.inject.Named

import akka.actor.ActorRef
import com.google.inject.Inject
import play.Environment
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import simulator.command._

class SimulatorCommandController @Inject() (@Named("command-controller")commandCtrl: ActorRef, cc: ControllerComponents, env: Environment) extends AbstractController(cc){
  val logger: Logger = Logger(this.getClass)
  def skipStopHandler = Action { implicit request =>
    val action  = Json.fromJson[SkipStopOnTrip](request.body.asJson.get).get
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }

  def delayMovementHandler = Action { implicit request =>
    val action  = Json.fromJson[DelayTrip](request.body.asJson.get).get
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }

  def delayTripPlanHandler = Action { implicit request =>
    val action  = Json.fromJson[DelayTripPlan](request.body.asJson.get).get
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }

  def terminateTripHandler = Action { implicit request =>
    val action  = Json.fromJson[TerminateTrip](request.body.asJson.get).get
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }

  def updateTripHandler = Action { implicit request =>
    val action  = Json.fromJson[UpdateTripPlan](request.body.asJson.get).get
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }

  def mergeTripHandler = Action { implicit request =>
    val action  = Json.fromJson[MergeTripPlan](request.body.asJson.get).get
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }

  def blockNodeHandler = Action { implicit request =>
    val action  = Json.fromJson[NodeBlocked](request.body.asJson.get).get
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }

  def releaseNodeHandler = Action { implicit request =>
    val action  = Json.fromJson[NodeRelease](request.body.asJson.get).get
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }

  def releaseAllNodesHandler = Action { implicit request =>
    val action  = NodeReleaseAll("etg")
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }

  def listBlockNodeHandler = Action { implicit request =>
    val action  = Json.fromJson[NodeRelease](request.body.asJson.get).get
    commandCtrl ! action.copy(group = "etg")
    Ok(Json.toJson(action))
  }
}
