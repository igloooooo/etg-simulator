package controllers

import javax.inject.Named

import akka.NotUsed
import akka.actor.ActorRef
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.google.inject.Inject
import play.Environment
import play.api.libs.json._
import play.api.libs.streams.ActorFlow
import play.api.mvc.WebSocket.MessageFlowTransformer
import play.api.mvc.{AbstractController, ControllerComponents, WebSocket}
import playback.channel.RegisterUser
import playback.ws.{WSInEvent, WSOutEvent}

class WSController @Inject() (@Named("etgPlayback")ctrl: ActorRef, cc: ControllerComponents, env: Environment) extends AbstractController(cc){
//  implicit val messageFlowTransformer = MessageFlowTransformer.jsonMessageFlowTransformer[WSInEvent, WSOutEvent]

  def ws = WebSocket.accept[JsValue, JsValue] { request =>
    val outGoingMsg = Source.actorRef[JsValue](10, OverflowStrategy.dropBuffer)
        .mapMaterializedValue{ outActor =>
          // give the user actor a way to send messages out
          ctrl ! RegisterUser(outActor)
        }

    Flow.fromSinkAndSource(Sink.ignore, outGoingMsg)
  }
}
