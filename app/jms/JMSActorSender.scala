package jms

import akka.actor.ActorContext

trait JMSActorSender {
  def jmsSender(msg: Any)(implicit context: ActorContext) = {
    context
      .actorSelection("akka://etgPlayback/user/jmsActor") ! msg
  }
}
