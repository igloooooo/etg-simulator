package lib

import java.io.File

import play.Environment

object WebpackBuildFile {
  def jsBundle(env: Environment): String = {
    val d = env.getFile("public/bundle")
    if(d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList.find(f => f.getName.contains("js.bundle.")).get.getName.replace(".gz", "")
    } else ""
  }

  def cssBundle(env: Environment): String = {
    val d = env.getFile("public/bundle")
    if(d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList.find(f => f.getName.contains("style.bundle.")).get.getName.replace(".gz", "")
    } else ""
  }
}
