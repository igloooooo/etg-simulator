package playback

import javax.inject.{Inject, Named}

import akka.actor.{ActorRef, ActorSystem, FSM, Terminated}
import org.joda.time.DateTime
import play.api.libs.concurrent.InjectedActorSupport
import playback.channel._
import playback.ws.{ChannelConfigStatusEvent, PlaybackStatusEvent}
import wsclient.MiddlewareWSClient

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import akka.pattern.ask
import service.dataset.VMStore
import simulator.command.{NodeReleaseAll, ResetTripAll}
import simulator.conflict.ConflictStore
import simulator.trip.ChangedTripStore

import scala.util.Success

/**
  * this is the playback controller
  */
object PlaybackCtrlActor {
}

class PlaybackCtrlActor @Inject()(wSClient: MiddlewareWSClient, system: ActorSystem,
                                  @Named("trip-controller")tripCtrl: ActorRef,
                                  @Named("node-controller")nodeCtrl: ActorRef) (implicit ec: ExecutionContext)
  extends FSM[PlaybackCtrlActorState, PlaybackCtrlActorStateData] with InjectedActorSupport{
  implicit val timeout: akka.util.Timeout = 5.seconds
  private val channelMap = Map(
    Channel.VMChannel -> context.actorOf(VMChannelActor.props(wSClient, Channel.VMChannel)),
    Channel.PTChannel -> context.actorOf(PTChannelActor.props(wSClient, Channel.PTChannel)),
    Channel.FAPChannel -> context.actorOf(FAPChannelActor.props(wSClient, Channel.FAPChannel)),
    Channel.FARChannel -> context.actorOf(FARChannelActor.props(wSClient, Channel.FARChannel)),
    Channel.NOChannel -> context.actorOf(FARChannelActor.props(wSClient, Channel.NOChannel)),
    Channel.IMChannel -> context.actorOf(FARChannelActor.props(wSClient, Channel.IMChannel))
  )
  // user client
  var clients: Set[ActorRef] = Set.empty

  private var count = 0
  val scheduler = system.scheduler.schedule(10 second, 1 second) {
    if (this.stateName == Active ) {
      count = count +1*stateData.speed
      val currentPlayTime = stateData.startTime.minusSeconds(-1 * count)
      val currentPlayEndTime = currentPlayTime.minusSeconds(-1 * stateData.speed)
      if (stateData.endDate != null && currentPlayTime.isAfter(stateData.endTime)) {
          stopHandler(StopPlay(), stateData)
      } else {

        val pollMsg = PollMsg(targetTime = currentPlayTime, baseLineTime = stateData.startTime,
          startTimeDiff = count, endTimeDiff = count + stateData.speed)
        // send polling message to all channel
//        channelMap.foreach(pair => pair._2 ! pollMsg)

        // send polling message to trip controller
        tripCtrl ! pollMsg
        // send update time to all client
        clients.foreach(actor => actor ! PlaybackStatusEvent(currentPlayTime, stateData.startTime, stateData.endTime, stateData.speed, getPlayStatus).toJsValue)
      }
    }
  }

  startWith(Init, new PlaybackCtrlActorStateData(DateTime.now(), DateTime.now(), DateTime.now().minusDays(-1),
    DateTime.now(), DateTime.now().minusDays(-1), 1))

  when(Init) {
    case Event(InitPlay(targetDate, startDate, endDate, startTime, endTime, speed), stateDate) =>
      stay() using stateDate.copy(targetDate, startDate, endDate, startTime, endTime, speed)

    case Event(event @ StartPlay(_,_,_,_), stateData) =>
      channelMap.foreach(pair => pair._2 ! event)
      count = 0
      goto(Active) using stateData.copy(
        targetDate = event.startDate,
        startDate = event.startDate,
        endDate = event.endDate,
        startTime = event.startTime,
        endTime = event.endTime,
        speed = 1)

    case Event(config @ ConfigChannel(name, _, _, _, _), _) =>
      channelMap.get(name) match {
        case Some(channel) =>
          channel forward config
        case None =>
          log.warning("Missing the channel {}", name)
      }
      stay()

    case Event(config @ ChannelConfigUpdated(_, _, _, _, _), _) =>
      // broadcast config changes to all clients
      clients.foreach(actor => actor ! ChannelConfigStatusEvent(config.name, config.dataSetName, config.isLoad, config.totalCount, config.sentCount))
      stay()

  }

  when(Active) {
    case Event(event @ PausePlay(), _) =>
      pauseHandler(event)

    case Event(SpeedUp(changedSpeed), stateData) =>
      stay() using stateData.copy(speed = changedSpeed)

    case Event(event @ StopPlay(), stateData) =>
      stopHandler(event, stateData)
  }

  when(Pause) {
    case Event(event @ StartPlay(_,_,_,_), _) =>
      channelMap.foreach(_._2 ! event)
      goto(Active)

    case Event(event @ StopPlay(), stateData) =>
      stopHandler(event, stateData)
  }

  whenUnhandled {
    case Event(register @ RegisterUser(_), _) =>
      handlerRegisterUser(register)

    case Event(Terminated(client), _) =>
      log.info("remove the clicke {}", client)
      clients -= client
      stay()

    case Event(event @ ChannelStatusReport(channelName, _), _) =>
      channelMap.get(channelName) match {
        case Some(channel) =>
          channel forward event
        case None =>
          log.warning("Missing the channel {}", channelName)
      }
      stay()
    case Event(event @ RequestChannelSOD(_,_), stateData) =>
      if(this.stateName == Active) {
        channelMap.get(event.name) match {
          case Some(channel) =>
            (channel ? event).onComplete {
              case Success(v) =>
                sender() ! v
              case _ =>
                sender() ! ResponseChannelSOD(event.name, null, null, false)
            }
          case None =>
            sender() ! ResponseChannelSOD(event.name, null, null, false)
        }
      } else {
        sender() ! ResponseChannelSOD(event.name, null, null, false)
      }
      stay()

    case Event(event, _) =>
//      log.warning("unexpected message: {}", event)
      stay()

  }

  private[this] def pauseHandler(event:PausePlay) = {
    channelMap.foreach(_._2 ! event)
    goto(Pause)
  }

  /**
    * stop the play reset all data
    * @param event
    * @param stateDate
    * @return
    */
  private[this] def stopHandler(event:StopPlay, stateDate:PlaybackCtrlActorStateData) = {
    count = 0
    channelMap.foreach(_._2 ! event)
    // remove all changed trip
    ChangedTripStore.resetAll
    // reset all trips
    tripCtrl ! ResetTripAll("etg")
    // release all nodes
    nodeCtrl ! NodeReleaseAll
    // clean vm
    VMStore.reset
    // clean conflict
    ConflictStore.reset
    goto(Init) using stateDate.copy(speed = 1)
  }

  private[this] def handlerRegisterUser(register:RegisterUser) = {
    clients += register.actorRef
    context.watch(register.actorRef)

    // send play status to register
    val currentPlayTime = stateData.startTime.minusSeconds(-1 * count * stateData.speed)
    register.actorRef ! PlaybackStatusEvent(currentPlayTime, stateData.startTime, stateData.endTime, stateData.speed, getPlayStatus).toJsValue

    channelMap.foreach(pair => {
      // send channel config to register
      pair._2 ! ChannelStatusReport(pair._1, register.actorRef)
    })
    stay()
  }

  /**
    * convert FMS state to play status
    */
  private[this] def getPlayStatus ={
    this.stateName match {
      case Active =>
        "Play"
      case Pause =>
        "Pause"
      case _ =>
        "Stop"
    }
  }
}
