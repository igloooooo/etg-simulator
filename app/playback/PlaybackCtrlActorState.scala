package playback

import org.joda.time.DateTime

sealed trait PlaybackCtrlActorState {

}

case object Init extends PlaybackCtrlActorState
case object Pause extends PlaybackCtrlActorState
case object Active extends PlaybackCtrlActorState

case class PlaybackCtrlActorStateData(targetDate:DateTime, startDate:DateTime, endDate:DateTime, startTime:DateTime, endTime:DateTime, speed:Int)