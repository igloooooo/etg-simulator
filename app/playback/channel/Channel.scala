package playback.channel

object Channel {
  val VMChannel = "vm"
  val PTChannel = "pt"
  val FAPChannel = "fap"
  val FARChannel = "far"
  val NOChannel = "no"
  val IMChannel = "im"
}
