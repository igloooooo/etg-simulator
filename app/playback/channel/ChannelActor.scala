package playback.channel

import javax.inject.Inject

import akka.actor.{Actor, FSM, Props}
import jms.JMSActorSender
import org.joda.time.DateTime
import playback.ws.ChannelConfigStatusEvent
import wsclient.MiddlewareWSClient

import scala.concurrent.ExecutionContext

object ChannelActor {
  def props(wSClient: MiddlewareWSClient, name: String)(implicit ec: ExecutionContext): Props = Props(new ChannelActor(wSClient, name)(ec))
}

case class ChannelInfo(jmsSender: JMSActorSender)

class ChannelActor (wSClient: MiddlewareWSClient, name: String) (implicit ec: ExecutionContext) extends FSM[ChannelActorState, ChannelActorStateData]{
  startWith(Init, new ChannelActorStateData("", false, 0, 0))

  when(Init) {
    case Event(ConfigChannel(`name`, dataSetName, isLoad, totalCount, sentCount), stateData) =>
      log.info("Channel {}: set data set: {}", name, dataSetName)
      sender() ! ChannelConfigUpdated(name, dataSetName, isLoad, totalCount, sentCount)
      stay() using stateData.copy(dataSetName = dataSetName, isLoad = isLoad, totalCount = totalCount, sentCount = sentCount)

    case Event(event @ StartPlay(_,_,_,_), stateData) =>
      if (stateData.isLoad) {
        startPlayHandler(event)
      } else {
        stay()
      }

  }

  when(Active) {
    case Event(msg@PollMsg(_,_,_,_), stateData) =>
      if (stateData.isLoad && !stateData.dataSetName.isEmpty) {
        generateJMSRequest(msg)
      } else {
        stay()
      }

    case Event(StopPlay(), _) =>
      goto(Init)
    case Event(PausePlay(), _) =>
      goto(Pause)
  }

  when(Pause) {
    case Event(StartPlay(_,_,_,_), _) =>
      goto(Active)

    case Event(StopPlay(), _) =>
      goto(Init)
  }

  whenUnhandled {
    case Event(ChannelStatusReport(`name`, register), stateData) =>
      register ! ChannelConfigStatusEvent(name, stateData.dataSetName, stateData.isLoad, stateData.totalCount, stateData.sentCount).toJsValue
      stay()
    case Event(event @ RequestChannelSOD(_,_), stateData) =>
      sender() ! ResponseChannelSOD(event.name, startTime, endTime, stateData.isLoad)
      stay()
    case Event(event @ _, _) =>
//      log.warning("unhandled message: {}", event)
      stay()

  }

  def generateJMSRequest(msg:PollMsg):State = {
    stay()
  }

  /**
    * this handle from Init to Active
    * @param event
    * @return
    */
  def startPlayHandler(event:StartPlay) = {
    goto(Active)
  }

  private def startTime = {
    DateTime.parse(stateData.dataSetName)
  }

  private def endTime = {
    DateTime.parse(stateData.dataSetName).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59)
  }
}
