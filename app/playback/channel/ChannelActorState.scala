package playback.channel

import org.joda.time.DateTime

sealed trait ChannelActorState {

}

case object Init extends ChannelActorState
case object Active extends ChannelActorState
case object Pause extends ChannelActorState

case class ChannelActorStateData(dataSetName:String, isLoad:Boolean, totalCount:Int, sentCount:Int)