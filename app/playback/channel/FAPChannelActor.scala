package playback.channel

import akka.actor.Props
import api.dto.IntraDayListResponse
import org.joda.time.DateTime
import play.api.libs.json.Json
import util.PlaybackDateUtil
import wsclient.MiddlewareWSClient

import scala.concurrent.ExecutionContext

object FAPChannelActor {
  def props(wSClient: MiddlewareWSClient, name: String)(implicit ec: ExecutionContext): Props = Props(new FAPChannelActor(wSClient, name)(ec))
}

/**
  * this actor will handle Fleet Allocation Publish
  * @param wSClient
  * @param key
  * @param ec
  */
class FAPChannelActor(wSClient: MiddlewareWSClient, key: String)(implicit ec: ExecutionContext) extends ChannelActor(wSClient, key)(ec) {
  private var intraDayList = List[IntraDayListResponse]()
  override def generateJMSRequest(msg: PollMsg): State = {
    log.debug("send FA request to data center.")
    val queryBaseLineTime = DateTime.parse(this.stateData.dataSetName).withMillisOfDay(msg.baseLineTime.getMillisOfDay)
    val queryStartTime = queryBaseLineTime.minusSeconds(-1*msg.startTimeDiff)
    val queryEndTime = queryBaseLineTime.minusSeconds(-1*msg.endTimeDiff)
    val pollMsg = pollMessageFromList(queryStartTime, queryEndTime)
    pollMsg.foreach(id => {
      wSClient.client("api/v1/sendJMS")
          .addQueryStringParameters("id" -> id)
          .addQueryStringParameters("targetDate" -> PlaybackDateUtil.convertToDBFormat(msg.targetTime))
          .addQueryStringParameters("type" -> "fa")
        .get()
    })
    // update PT status
    stay() using stateData.copy(sentCount = stateData.sentCount + pollMsg.size)
  }

  override def startPlayHandler(event: StartPlay): State = {
    // load the fa publish list
    val initDate = DateTime.parse(this.stateData.dataSetName)
    wSClient.client("api/v1/intraday")
      .addQueryStringParameters("datasetType" -> "fa")
      .addQueryStringParameters("startDate" -> PlaybackDateUtil.convertToDBFormat(initDate.withTimeAtStartOfDay()))
      .addQueryStringParameters("endDate" -> PlaybackDateUtil.convertToDBFormat(initDate.minusDays(-1).withTimeAtStartOfDay()))
      .get()
      .map {
        response =>
          intraDayList = Json.fromJson[Array[IntraDayListResponse]](response.json).get.toList
      }
    super.startPlayHandler(event)
  }

  private def pollMessageFromList(startTime:DateTime, endTime:DateTime) = {
    intraDayList.filter(p => p.timestamp.equals(startTime) || (p.timestamp.isAfter(startTime) && p.timestamp.isBefore(endTime))).map(_.id)
  }
}
