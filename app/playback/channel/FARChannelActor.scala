package playback.channel

import akka.actor.Props
import wsclient.MiddlewareWSClient

import scala.concurrent.ExecutionContext

object FARChannelActor {
  def props(wSClient: MiddlewareWSClient, name: String)(implicit ec: ExecutionContext): Props = Props(new FAPChannelActor(wSClient, name)(ec))
}

class FARChannelActor (wSClient: MiddlewareWSClient, key: String)(implicit ec: ExecutionContext) extends ChannelActor(wSClient, key)(ec) {
  override def generateJMSRequest(msg: PollMsg): State = {
    super.generateJMSRequest(msg)
  }
}
