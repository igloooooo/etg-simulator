package playback.channel

import akka.actor.Props
import wsclient.MiddlewareWSClient

import scala.concurrent.ExecutionContext

object NOChannelActor {
  def props(wSClient: MiddlewareWSClient, name: String)(implicit ec: ExecutionContext): Props = Props(new NOChannelActor(wSClient, name)(ec))
}

class NOChannelActor(wSClient: MiddlewareWSClient, key: String)(implicit ec: ExecutionContext) extends ChannelActor(wSClient, key)(ec) {
  override def generateJMSRequest(msg:PollMsg):State = {
    log.debug("NO doesn't support this.")
    super.generateJMSRequest(msg)
  }
}
