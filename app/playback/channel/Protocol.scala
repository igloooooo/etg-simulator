package playback.channel

import akka.actor.ActorRef
import org.joda.time.DateTime

// for all internal message
trait Protocol {

}

/**
  *
  * @param startTimeDiff different seconds since simulator start to play
  * @param endTimeDiff startTimeDiff + speed*1
  * @param targetTime equals currently calculate time
  * @param baseLineTime the simulator start time
  */
case class PollMsg(startTimeDiff: Int, endTimeDiff:Int, targetTime:DateTime, baseLineTime:DateTime) extends Protocol
case class PauseChannel(name:String) extends Protocol
case class TerminateChannel(name:String) extends Protocol
case class ConfigChannel(name:String, dataSetName: String, isLoad:Boolean, totalCount:Int, sentCount:Int) extends Protocol
case class InitPlay(targetDate:DateTime, startDate:DateTime, endDate:DateTime, startTime:DateTime, endTime:DateTime, speed:Int) extends Protocol
case class StartPlay(startDate:DateTime, endDate:DateTime, startTime:DateTime, endTime:DateTime) extends Protocol
case class StopPlay() extends Protocol
case class PausePlay() extends Protocol
case class SpeedUp(speed:Int) extends Protocol
case class ResetPlay() extends Protocol
case class ChannelStatusReport(name:String, register:ActorRef) extends Protocol
case class ChannelConfigUpdated(name:String, dataSetName:String, isLoad:Boolean, totalCount:Int, sentCount:Int) extends Protocol

case class RequestChannelSOD(name:String, startDate:DateTime) extends Protocol
case class ResponseChannelSOD(name:String, startDate:DateTime, endDate:DateTime, isLoad:Boolean) extends Protocol

// manage user client
case class RegisterUser(actorRef: ActorRef) extends Protocol