package playback.channel

import javax.inject.Inject

import akka.actor.Props
import api.dto.{IntraDayListResponse, TrainActualDto, TrainActualResponse}
import api.dto.jms.VMJMSRequest
import com.google.inject.assistedinject.Assisted
import org.joda.time.DateTime
import play.api.libs.json.Json
import playback.channel.VMChannelActor.SentCountUpdate
import playback.ws.VMEvent
import service.dataset.{PTSectorMap, VMStore}
import util.PlaybackDateUtil
import wsclient.MiddlewareWSClient

import scala.concurrent.ExecutionContext

object VMChannelActor {
  def props(wSClient: MiddlewareWSClient, name: String)(implicit ec: ExecutionContext): Props = Props(new VMChannelActor(wSClient, name)(ec))
  case class SentCountUpdate(count:Int)
}

class VMChannelActor (wSClient: MiddlewareWSClient, key: String)(implicit ec: ExecutionContext) extends ChannelActor(wSClient, key)(ec) {

  when(Active) {
    case Event(SentCountUpdate(count), stateData) =>
      stay using stateData.copy(sentCount = stateData.sentCount + count)
  }
  override def generateJMSRequest(msg:PollMsg):State = {
//    log.debug("send VM request to data center: [{}] {} - {}", msg.targetTime)
    // calculate the query time
    val queryBaseLineTime = DateTime.parse(this.stateData.dataSetName).withMillisOfDay(msg.baseLineTime.getMillisOfDay)
    val queryStartTime = queryBaseLineTime.minusSeconds(-1*msg.startTimeDiff)
    val queryEndTime = queryBaseLineTime.minusSeconds(-1*msg.endTimeDiff)
    wSClient.client("api/v1/vm")
      .addQueryStringParameters(
      "targetDate" -> PlaybackDateUtil.convertToDBFormat(msg.targetTime),
      "startTime" ->  PlaybackDateUtil.convertToDBFormat(queryStartTime),
      "endTime" -> PlaybackDateUtil.convertToDBFormat(queryEndTime)
    ).get()
      .map {
        response =>
          val trainActualResponse  = Json.fromJson[TrainActualResponse](response.json).get
          val currentStartTime = msg.targetTime.withMillisOfDay(queryStartTime.getMillisOfDay)
          val currentEndTime = msg.targetTime.withMillisOfDay(queryEndTime.getMillisOfDay)
          // add it into vm store
          trainActualResponse.vm.foreach(dto => {
            //TODO: some vm message are delay very much and response time does NOT match arrival/departure time, it should be resolve via data capture
            VMStore.addVM(
              TrainActualDto(
                trip = dto.trip,
                time = dto.time,
                actualType = dto.actualType,
                stopRef = dto.stopRef))
          })
          self ! SentCountUpdate(trainActualResponse.vm.size)
      }
    super.generateJMSRequest(msg)
  }

}
