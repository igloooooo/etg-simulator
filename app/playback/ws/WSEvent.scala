package playback.ws

import javax.swing.event.DocumentEvent.EventType

import org.joda.time.DateTime
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.WebSocket.MessageFlowTransformer

trait WSEvent {

}

trait WSOutEvent {
  def toJsValue:JsValue
}

object WSInEvent {
  implicit val actionReads = Json.reads[WSInEvent]
  implicit val actionWrite = Json.writes[WSInEvent]
  implicit val inEventFormat = Json.format[WSInEvent]

}
case class WSInEvent(name:String) extends WSEvent {

}

object PlaybackStatusEvent {
  implicit val actionReads = Json.reads[PlaybackStatusEvent]
  implicit val actionWrite = Json.writes[PlaybackStatusEvent]
  implicit val outEventFormat = Json.format[PlaybackStatusEvent]
}
case class PlaybackStatusEvent(playTime: DateTime, startTime:DateTime, endTime:DateTime, speed:Int, playStatus:String, eventType:String="PlaybackStatusEvent") extends WSOutEvent {
  override def toJsValue = Json.toJson(this)
}

object ChannelConfigStatusEvent {
  implicit val actionReads = Json.reads[ChannelConfigStatusEvent]
  implicit val actionWrite = Json.writes[ChannelConfigStatusEvent]
  implicit val outEventFormat = Json.format[ChannelConfigStatusEvent]
}
case class ChannelConfigStatusEvent(name: String, dataSetName:String, isLoad:Boolean, totalCount:Int, sentCount:Int, eventType:String="ChannelConfigStatus") extends WSOutEvent {
  override def toJsValue = Json.toJson(this)
}

object VMEvent {
  implicit val actionReads = Json.reads[VMEvent]
  implicit val actionWrite = Json.writes[VMEvent]
}
case class VMEvent(trip:String, value:Int, time:DateTime, borderId:Int, index:Int) extends WSOutEvent {
  override def toJsValue = Json.toJson(this)
}
