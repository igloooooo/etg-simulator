package schedule

import java.util.concurrent.TimeUnit
import javax.inject.Inject

import akka.actor.ActorSystem
import org.joda.time.DateTime
import play.api.Logger
import play.api.inject.{SimpleModule, bind}
import service.SiriETService

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

class FetchSIRIETTask @Inject() (actorSystem: ActorSystem, siriETService:SiriETService)(implicit executionContext: ExecutionContext) {
  private val interval:Int = 30
  Logger.info("Fetch Siri ET job start up...")
  actorSystem.scheduler.schedule(
    FiniteDuration(interval, TimeUnit.SECONDS),
    FiniteDuration(1, TimeUnit.MINUTES),
  ) {

    val endTime = DateTime.now()
    val startTime = endTime.minusSeconds(interval)
    siriETService.fetchSiriET(startTime, endTime)
  }
}
