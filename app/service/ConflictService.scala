package service

import java.time.Clock
import javax.inject.{Inject, Singleton}

import play.api.inject.ApplicationLifecycle
import service.network.NetworkNodeMap
import simulator.conflict.ConflictStore
import simulator.dto.{TripNodeConflictDrawItem, TripNodeConflictEvent}

@Singleton
class ConflictService @Inject()(clock: Clock, appLifecycle: ApplicationLifecycle) {
  def loadConflicts():List[TripNodeConflictDrawItem] = {
    ConflictStore.findAllConflicts().map(event => {
      NetworkNodeMap.findNetworkNode(event.stopRef) match {
        case Some(node) =>
          Some(TripNodeConflictDrawItem(
            stopRef = event.stopRef,
            conflictedList = event.conflictedList,
            locX = node.locX,
            locY = node.locY
          ))
        case None => None
      }
    }).flatten.toList
  }
}
