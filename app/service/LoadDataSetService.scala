package service

import java.time.Clock
import javax.inject.{Inject, Singleton}

import api.dto.DataSetListItem
import play.api.inject.ApplicationLifecycle
import play.api.libs.json.Json
import wsclient.MiddlewareWSClient

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class LoadDataSetService @Inject() (wSClient: MiddlewareWSClient, clock: Clock, appLifecycle: ApplicationLifecycle)(implicit ec: ExecutionContext){
  def listDataSet():Future[List[DataSetListItem]] = {
    wSClient.client("api/v1/dataset?datasetType=pt")
      .withRequestTimeout(60000.millis)
      .get()
      .map {
        response =>
          val listDataSet = Json.fromJson[List[DataSetListItem]](response.json).get
          listDataSet
      }
  }
}
