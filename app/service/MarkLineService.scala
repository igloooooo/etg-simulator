package service

import java.time.Clock
import javax.inject.{Inject, Singleton}

import api.dto.PTCallItem
import org.joda.time.DateTime
import play.api.inject.ApplicationLifecycle
import service.dataset.{PTSectorMap, PTStore}

@Singleton
class MarkLineService @Inject() (clock: Clock, appLifecycle: ApplicationLifecycle) {

  def convertMarkLineToCallItem(borderId:Int, sectorId:Int, tripId:String, value:Int):Option[PTCallItem] = {
    val sectorKey = PTSectorMap.findSectorListByBorder(borderId).find(_.index == sectorId).get.key

    PTStore.findTripAtSector(sectorKey, tripId).find(call => call.value == value && call.callType == 1).map(departure => {
      PTCallItem(None, Some(departure.time), departure.stopRef)
    })
  }
}
