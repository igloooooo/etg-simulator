package service

import java.time.Clock
import javax.inject.{Inject, Singleton}

import play.api.inject.ApplicationLifecycle
import service.network.NetworkNodeMap
import simulator.dto.{BlockNodeDrawItem, NodeUpdateEvent}
import simulator.stopnode.NodeStatusStore

@Singleton
class NodeService @Inject() (clock: Clock, appLifecycle: ApplicationLifecycle) {
  def loadAllBlockNodes():List[BlockNodeDrawItem] =
    NodeStatusStore.loadAllNodes.toList.map(nodeEvent => {
      NetworkNodeMap.findNetworkNode(nodeEvent.stopRef) match {
        case Some(node) =>
          Some(BlockNodeDrawItem(
            stopRef = nodeEvent.stopRef,
            locX = node.locX,
            locY = node.locY
          ))
        case None => None
      }
    }).flatten

}
