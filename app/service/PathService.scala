package service

import java.time.Clock
import javax.inject.{Inject, Singleton}

import api.dto.network.{PTNetworkDTO, VerticesNode}
import org.joda.time.DateTime
import play.api.inject.ApplicationLifecycle
import service.dataset.{ImportedTopologyNode, PTSectorMap, PTStore}
import service.network.{NetworkLinkMap, NetworkNodeMap, SectorRunTimeItem, SectorRunTimeMap}
import simulator.ai.PathFound

import scala.annotation.tailrec

@Singleton
class PathService @Inject() (clock: Clock, appLifecycle: ApplicationLifecycle) {
  /**
    * this function will handle the changes
    * from oldStartNode -> endNode
    * to oldStartNode ->nextNode -> endNode
    * @param oldStartNodeId redirect node
    * @param nextNodeId change to oldStartNode -> nextNode
    * @param endNodeId the last node
    * @return
    */
  def reRoutePath(oldStartNodeId:String, nextNodeId:String, endNodeId:String):Seq[PTNetworkDTO] = {
    val newPath = findPath(nextNodeId, endNodeId)
    val oldStartNode = PTSectorMap.microNodeMap.get(oldStartNodeId).get
    val nextNode = PTSectorMap.microNodeMap.get(nextNodeId).get
    removeDuplipcatedDropNode(generateLink(oldStartNode, nextNode, "", "", DateTime.now(), DateTime.now()) ::: newPath.toList).seq
  }

  /**
    * this function will handle the changes
    * from oldStartNode -> endNode
    * to oldStartNode ->nextNode -> endNode
    * @param tripId
    * @param oldStartNodeId
    * @param nextNodeId
    * @param endNodeId
    * @return full trip stops
    */
  def reRouteTrip(tripId:String, oldStartNodeId:String, nextNodeId:String, endNodeId:String):Seq[PTNetworkDTO] = {
    val oldTripPath = findTripPath(tripId)
    val oldStartNode = PTSectorMap.microNodeMap.get(oldStartNodeId).get
    val nextNode = PTSectorMap.microNodeMap.get(nextNodeId).get
    val index = oldTripPath.indexWhere(_.stopRef == oldStartNodeId)
    val oldStartDto = oldTripPath.find(_.stopRef == oldStartNodeId).get
    val newPath = findPath(nextNodeId, endNodeId)
    val trip = oldTripPath.head.trip
    val startPart = generateLink(oldStartNode, nextNode, tripId, trip, oldStartDto.arrivalToNode, oldStartDto.departureFromNode)
    removeDuplipcatedDropNode(
      oldTripPath.slice(0, index).toList
        ::: startPart
        ::: populateTripTime(tripId, startPart.head, newPath.toList).toList).seq
  }

  def findTripPath(tripId:String):Seq[PTNetworkDTO] = {
    val tripList = PTStore.tripMap(tripId).groupBy(_.stopRef).toList.sortBy(_._2.head.time.getMillis)
    val lastTripCall = tripList.last
    val lastTripNetworkNode = NetworkNodeMap.findNetworkNode(lastTripCall._1).get

    removeDuplipcatedDropNode(
      ((tripList zip tripList.tail) flatMap {
      case (from, to) =>
        val fromNodeId = PTSectorMap.microNodeMap(from._1)
        val toNodeId = PTSectorMap.microNodeMap(to._1)
        val arrivalFromNode = from._2.find(_.callType == 0).get.time
        val departureFromNode = from._2.find(_.callType == 1).get.time
        generateLink(fromNodeId, toNodeId, from._2.head.tripId, from._2.head.trip, arrivalFromNode, departureFromNode)

    }) :+ PTNetworkDTO(
        trip = lastTripCall._2.head.trip,
        tripId = tripId,
        stopRef = lastTripCall._1,
        locX = lastTripNetworkNode.locX,
        locY = lastTripNetworkNode.locY,
        availableLinks = List(),
        arrivalToNode = DateTime.now(),
        departureFromNode = DateTime.now(),
        isVertices = false
      ) )
  }

  /**
    * this method only using for find path between node
    * @param startNodeId
    * @param endNodeId
    * @return
    */
  def findPath(startNodeId:String, endNodeId:String):Seq[PTNetworkDTO] = {
    val startNode = PTSectorMap.microNodeMap.get(startNodeId).get
    val endNode = PTSectorMap.microNodeMap.get(endNodeId).get
    val possiblePath = PathFound.foundPath(startNode, endNode)
    if(possiblePath.isEmpty) {
      Seq()
    }else{
      // this is the last node
      val lastNetworkNode = NetworkNodeMap.findNetworkNode(possiblePath.last.NodeID).get
      removeDuplipcatedDropNode(
        (possiblePath zip possiblePath.tail).flatMap {
        case (from, to) =>
          generateLink(from, to, "test", "test", DateTime.now(), DateTime.now())
      } :+ PTNetworkDTO(trip = "test",
        tripId = "test",
        stopRef = lastNetworkNode.shortName,
        locX = lastNetworkNode.locX,
        locY = lastNetworkNode.locY,
        availableLinks = List(),
        arrivalToNode = DateTime.now(),
        departureFromNode = DateTime.now(),
        isVertices = false
      ))
    }
  }

  private def generateLink(from:ImportedTopologyNode, to: ImportedTopologyNode, tripId:String, trip:String, arrivalStartNode:DateTime, departureStartNode:DateTime):List[PTNetworkDTO] = {
    val fromNetworkNode = NetworkNodeMap.findNetworkNode(from.NodeID).get
    val availableLinks = NetworkLinkMap.findAvailableLinkFromNode(from.NodeID).filter(link => link.toNode != to.NodeID)
    val vertices = NetworkLinkMap.findVerticesLink(from.NodeID, to.NodeID)
    val startPTNetworkDto = PTNetworkDTO(trip = trip,
      tripId = tripId,
      stopRef = from.NodeID,
      locX = fromNetworkNode.locX,
      locY = fromNetworkNode.locY,
      availableLinks = availableLinks,
      arrivalToNode = arrivalStartNode,
      departureFromNode = departureStartNode,
      isVertices = false
    )
    List(startPTNetworkDto) ::: vertices.map(vertice => {
      PTNetworkDTO(tripId = tripId,
        trip = trip,
        stopRef = "",
        locX = vertice.LocX,
        locY = vertice.LocY,
        availableLinks = List(),
        arrivalToNode = DateTime.now(),
        departureFromNode = DateTime.now(),
        isVertices = true
      )
    }).toList
  }

  /**
    * populate the trip time for new plan
    * @param tripId
    * @param startNode
    * @param unplannedNode
    * @return
    */
  private def populateTripTime(tripId:String, startNode:PTNetworkDTO, unplannedNode:Seq[PTNetworkDTO]):Seq[PTNetworkDTO]={
    val oldTripList = PTStore.tripMap(tripId)
    val speedBandName = oldTripList.head.speedBandName
    populateTripCall(unplannedNode, Seq(), startNode.departureFromNode, startNode.stopRef, speedBandName)
  }

  @tailrec
  private def populateTripCall(unplannedNode:Seq[PTNetworkDTO], plannedNode:Seq[PTNetworkDTO], currentTime:DateTime, fromNodeId:String, speedBan:String):Seq[PTNetworkDTO] = {
    unplannedNode match {
      case head::Nil =>
        if(head.isVertices) {
          plannedNode :+ head
        } else {
          SectorRunTimeMap.minPassTime(speedBan, fromNodeId, head.stopRef) match {
            case Some(runTimeItem) =>
              val arrivalTime = currentTime.minusSeconds(-1*runTimeItem.minPassPassDuration)
              val departureTIme = currentTime.minusSeconds(-1*runTimeItem.startStopDuration)
              plannedNode :+ head.copy(arrivalToNode = arrivalTime, departureFromNode = departureTIme)
            case None =>
              val arrivalTime = currentTime.minusSeconds(-1*30)
              val departureTIme = currentTime.minusSeconds(-1*30)
              plannedNode :+ head.copy(arrivalToNode = arrivalTime, departureFromNode = departureTIme)
          }
        }
      case head::others =>
        if(head.isVertices) {
          populateTripCall(others, plannedNode :+ head, currentTime, head.stopRef, speedBan)
        } else {
          SectorRunTimeMap.minPassTime(speedBan, fromNodeId, head.stopRef) match {
            case Some(runTimeItem) =>
              val arrivalTime = currentTime.minusSeconds(-1*runTimeItem.minPassPassDuration)
              val departureTime = currentTime.minusSeconds(-1*runTimeItem.startStopDuration)
              populateTripCall(others,
                plannedNode :+ head.copy(arrivalToNode = arrivalTime, departureFromNode = departureTime),
                departureTime,
                head.stopRef,
                speedBan)

            case None =>
              val arrivalTime = currentTime.minusSeconds(-1*60)
              val departureTime = currentTime.minusSeconds(-1*60)
              populateTripCall(others,
                plannedNode :+ head.copy(arrivalToNode = arrivalTime, departureFromNode = departureTime),
                departureTime,
                head.stopRef,
                speedBan)
          }
        }
    }
  }

  private def removeDuplipcatedDropNode(ptDtos: Seq[PTNetworkDTO]):Seq[PTNetworkDTO] ={
    ptDtos.map(dto => {
      val availableLinks = dto.availableLinks.filter(link => {
        !ptDtos.exists(_.stopRef == link.toNode)
      })
      dto.copy(availableLinks = availableLinks)
    })
  }

}
