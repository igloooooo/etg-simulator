package service

import java.time.Clock
import javax.inject.Inject

import api.dto.{PlatformLane, PlatformStatus}
import api.dto.network.PTNetworkDTO
import org.joda.time.DateTime
import play.api.inject.ApplicationLifecycle
import service.dataset.{ImportedTopologyNode, PTSectorMap, PTStore, VMStore}

class PlatformService @Inject() (pathService: PathService, clock: Clock, appLifecycle: ApplicationLifecycle) {
  def loadPlatFormLane(stopRef:String):List[PlatformLane] = {
    PTSectorMap.findNodeByMasterNode(stopRef).map(_.filter(_.IsPlatform).map(node => {
      PlatformLane(node.NodeID, node.NodeName)
    })).getOrElse(List())
  }

  def loadPlatFormStatus(masterNode:String, startTime:DateTime, endTime:DateTime):Seq[PlatformStatus] = {
    val vmPlatformStatus = VMStore.findPlatformDetailsByMasterNode(masterNode, startTime, endTime)
    val ptPlatformStatus = PTStore.findPlatformDetailsByStop(masterNode)
    // merge
    ptPlatformStatus.map(platformStatus => {
      vmPlatformStatus.find(_.trip == platformStatus.trip).getOrElse(platformStatus)
    })
  }

  /**
    * find the possible path if change platform is available
    * @param tripId
    * @param originalPlatform original platform
    * @param targetPlatform change to new platform
    * @return possible path
    */
  def changePlatform(tripId:String, originalPlatform:String, targetPlatform:String):Option[Seq[PTNetworkDTO]] = {
    PTStore.findTripDetails(tripId) match {
      case Some(calls) =>
        val (pre, after) = calls.toList.sortBy(_.time.getMillis).span(_.stopRef != originalPlatform)
        val startChangeNode = pre.reverse.find(dto => {
          val possiblePath = pathService.findPath(dto.stopRef, targetPlatform)
          !possiblePath.isEmpty && possiblePath.size < 20
        })

        startChangeNode match {
          case Some(dto) =>
            val possiblePath = pathService.findPath(dto.stopRef, targetPlatform)
            Some(pathService.reRouteTrip(tripId, dto.stopRef, possiblePath.head.stopRef, after.last.stopRef))
          case None => None
        }
      case None => None
    }
  }

}
