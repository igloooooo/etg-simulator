package service

import javax.inject.{Named, Singleton}

import akka.actor.ActorRef
import api.dto.TimeTableResponse
import com.google.inject.Inject
import org.joda.time.DateTime
import play.api.libs.json.Json
import service.dataset.{PTOrderedCallDTO, PTStore}
import simulator.trip.TripController.RequestTrackTrip
import util.PlaybackDateUtil
import wsclient.MiddlewareWSClient

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

@Singleton
class SiriETService @Inject() (@Named("etgPlayback")ctrl: ActorRef, @Named("trip-controller")tripCtrl: ActorRef, tripService:TripService, wSClient: MiddlewareWSClient)(implicit ec: ExecutionContext){

  def fetchSiriET(startDate:DateTime, endDate:DateTime) = {
    if (PTStore.isTimetableLoad) {
      wSClient.client("api/v1/et/itd")
        .addQueryStringParameters("startDate" -> PlaybackDateUtil.convertToDBFormat(startDate))
        .addQueryStringParameters("endDate" -> PlaybackDateUtil.convertToDBFormat(endDate))
        .withRequestTimeout(10000.millis)
        .get()
        .map {
          response =>
            val timeTableResponse = Json.fromJson[TimeTableResponse](response.json).get

            // load siri et timetable
            timeTableResponse.trips.foreach(tripItem => {

              val trainType = tripItem.trainType
              val tripID = tripItem.trip + "_" + tripItem.tripDate //TODO: need have way to generate tripID
              // check if the trip exist
              if (tripService.isTripExist(tripID)) {
                tripService.updateTripPlan(tripID, tripItem.calls)
              } else {
                // register new trips
                tripItem.calls
                  .filter(p => p.a.isDefined && p.d.isDefined)
                  .foreach(callItem => {
                    val arrivalDTO = PTOrderedCallDTO(
                      tripId = tripID,
                      trip = tripItem.trip,
                      trainType = trainType,
                      time = callItem.a.get,
                      stopRef = callItem.s,
                      value = 0,
                      displayTime = "",
                      tripItem.speedBandName,
                      callType = 0
                    )
                    val departureDTO = PTOrderedCallDTO(
                      tripId = tripID,
                      trip = tripItem.trip,
                      trainType = trainType,
                      time = callItem.d.get,
                      stopRef = callItem.s,
                      value = 0,
                      displayTime = "",
                      tripItem.speedBandName,
                      callType = 1
                    )
                    // add arrival
                    PTStore.addCall(arrivalDTO)
                    // add departure
                    PTStore.addCall(departureDTO)

                    // send trip register to trip controller
                    tripCtrl ! RequestTrackTrip("etg", tripID, tripItem.trip, callItem.a.get, arrivalDTO, departureDTO)

                  })

              }

            })

            response.status
        }
    }
  }
}
