package service

import java.time.Clock
import javax.inject.{Inject, Singleton}

import api.dto.statis.{KPIDto, TotalStatusDTO, VMAndTripCountPerMinDTO, VMDelayAndSkipStopStatisDTO}
import org.joda.time.DateTime
import play.api.Logger
import play.api.inject.ApplicationLifecycle
import service.dataset._

@Singleton
class StatisticService @Inject() (clock: Clock, appLifecycle: ApplicationLifecycle) {

  def findTotalRunStatus(startTime:DateTime, endTime:DateTime):TotalStatusDTO = {
    val currentTripCount = findCurrentRunningTrips
    val totalTripCount = findTotalTrips
    val vmCount = VMStore.findAllVM.size
    val currentDelayTripsCount = findCurrentDelay(startTime, endTime).groupBy(_.trip).keys.size
    val currentSkipStops = findCurrentSkipStops(startTime, endTime).size
    val totalDelayTripsCount = findTotalDelays.groupBy(_.trip).keys.size
    val totalSkipStopCount = findTotalSkipStops.size
    TotalStatusDTO(
      totalTrips = totalTripCount.size,
      currentTrips = currentTripCount,
      totalDelayTrips = totalDelayTripsCount,
      currentDelayTrips = currentDelayTripsCount,
      totalSkipStops = totalSkipStopCount,
      currentSkipStops = currentSkipStops)
  }

  def findTotalDelayAndSkipStopStatis() :List[VMDelayAndSkipStopStatisDTO] = {
    val countDelayBySector = findTotalDelays.filter(_.delay >= 180).flatMap(dto => {
      PTSectorMap.findSectorsByNode(dto.stopRef).map(sectorKey => {
        (sectorKey, 1)
      })
    }).groupBy(_._1).mapValues(_.size)

    val countSkipStopBySector = findTotalDelays.flatMap(dto => {
      PTSectorMap.findSectorsByNode(dto.stopRef).map(sectorKey => {
        (sectorKey, 1)
      })
    }).groupBy(_._1).mapValues(_.size)

    countSkipStopAndDelayBySector(countDelayBySector, countSkipStopBySector)
  }

  def findCurrentDelayAndSkipStopStatis(startTime:DateTime, endTime:DateTime) :List[VMDelayAndSkipStopStatisDTO] = {
    val countDelayBySector = findCurrentDelay(startTime:DateTime, endTime:DateTime).flatMap(dto => {
      PTSectorMap.findSectorsByNode(dto.stopRef).map(sectorKey => {
        (sectorKey, 1)
      })
    }).groupBy(_._1).mapValues(_.size)

    val countSkipStopBySector = findCurrentSkipStops(startTime:DateTime, endTime:DateTime).flatMap(dto => {
      PTSectorMap.findSectorsByNode(dto.stopRef).map(sectorKey => {
        (sectorKey, 1)
      })
    }).groupBy(_._1).mapValues(_.size)

    countSkipStopAndDelayBySector(countDelayBySector, countSkipStopBySector)
  }

  def loadCurrentKPI(startTime:DateTime, endTime:DateTime):KPIDto = {
    val currentTripCount = findCurrentRunningTrips
    val vmCount = VMStore.findAllVM.filter(dto => dto.time.isAfter(startTime) && dto.time.isBefore(endTime)).size
    val currentDelayCount = findCurrentDelay(startTime, endTime).size
    val currentSkipStops = findCurrentSkipStops(startTime, endTime).size
    val currentDelayTripsCount = findCurrentDelayTripsCount(startTime, endTime)
    KPIDto(currentTripCount, vmCount, currentDelayTripsCount, currentSkipStops)
  }

  def loadVMAndTripsCountPerMinute():VMAndTripCountPerMinDTO = {
    val tripPerMin = PTStore.countTripPerMin
    val vmPerMin = VMStore.findVMPerMinuteStatis
    VMAndTripCountPerMinDTO(
      tripCount = tripPerMin.map(pair => List(pair._1, pair._2)).toList.sortBy(_.head),
      vmCount = vmPerMin.map(pair => List(pair._1, pair._2)).toList.sortBy(_.head)
    )
  }

  def findCurrentDelayTripsCount(startTime:DateTime, endTime:DateTime):Int = {
    findCurrentDelay(startTime, endTime).groupBy(_.trip).keys.size
  }

  def findCurrentRunningTrips():Int = {
    VMStore.findCurrentRunningTrips.size
  }

  def findTotalSkipStops:List[VMSkipStopDTO] = {
    VMStore.findAllSkipStops.toList
  }

  def findTotalDelays:List[VMDelayDTO] = {
    VMStore.findAllDelays.toList
  }

  def findTotalTrips:List[String] ={
    VMStore.finalAllTrips
  }

  def findCurrentDelay(startTime:DateTime, endTime:DateTime):List[VMDelayDTO] = {
    VMStore.findAllDelays.toList.filter(dto => dto.time.isAfter(startTime) && dto.time.isBefore(endTime) && dto.delay >= 180).sortBy(_.time.getMillis)
  }

  def findCurrentSkipStops(startTime:DateTime, endTime:DateTime):List[VMSkipStopDTO] = {
    VMStore.findAllSkipStops.toList.filter(dto => dto.time.isAfter(startTime) && dto.time.isBefore(endTime))
  }

  private def countSkipStopAndDelayBySector(countDelayBySector:Map[String, Int], countSkipStopBySector:Map[String, Int]):List[VMDelayAndSkipStopStatisDTO] = {
    PTSectorMap.areaSectorMap.map {
      case (borderId, sectorList) =>
        val counted = sectorList.sortBy(_.seqNr).map(sector => {
          val skipStopCount = countSkipStopBySector.get(sector.key).getOrElse(0)
          val delayCount = countDelayBySector.get(sector.key).getOrElse(0)
          (delayCount, skipStopCount)
        })
        VMDelayAndSkipStopStatisDTO(
          borderId = borderId,
          delayCount = counted.map(_._1).toList,
          skipStopCount = counted.map(_._2).toList
        )
    } toList
  }
}
