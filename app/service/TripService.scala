package service

import java.time.Clock
import javax.inject.{Inject, Named, Singleton}

import akka.actor.ActorRef
import api.dto._
import play.api.Logger
import play.api.inject.ApplicationLifecycle
import service.dataset.{PTOrderedCallDTO, PTSectorMap, PTStore, VMStore}
import simulator.trip.TripController.{RemoveTripPlanCall, RequestTrackTrip}

@Singleton
class TripService @Inject() (@Named("trip-controller")tripCtrl: ActorRef, clock: Clock, appLifecycle: ApplicationLifecycle) {
  def findTripStatus(trip:String):Option[TripStatus] = {
    VMStore.findLatestVMByTrip(trip)
  }

  def isTripExist(tripID:String):Boolean = {
    PTStore.findTripDetails(tripID).isDefined
  }

  /**
    *
    * @param tripId
    * @return
    */
  def findTripDetails(tripId:String):Option[TripDetailDTO] = {
    PTStore.findTripDetails(tripId).map(calls => {
      TripDetailDTO(tripId, calls.head.trip, calls.map(call => {
        TripDetailItem(
          timestamp = call.time,
          movementType = call.callType,
          stopRef = call.stopRef
        )
      }).toSeq.sortBy(_.timestamp.getMillis)
      )
    })
  }

  def findTripPlatformDetails(tripId:String):Option[TripPlatformDetailDTO] = {
    val plannedTrip = PTStore.findTripDetails(tripId)
    val actualList = VMStore.findAllVMByTrip(tripId)
    plannedTrip match {
      case Some(calls) =>
        val trip = calls.head.trip
        val items =calls.filter(call => PTSectorMap.isPlatform(call.stopRef)).groupBy(_.stopRef).map {
          case (stopRef, pair) =>
            val platform = PTSectorMap.loadMicroNodeDetails(stopRef).get.NodeName
            val plannedArrival = pair.find(_.callType == 0).map(_.time)
            val plannedDeparture = pair.find(_.callType == 1).map(_.time)
            val actualArrival = actualList.find(vm => vm.stopRef == stopRef && vm.callType == 0).map(_.time)
            val actualDeparture = actualList.find(vm => vm.stopRef == stopRef && vm.callType == 1).map(_.time)
            TripPlatformDetailItem(stopRef, platform, plannedArrival, plannedDeparture, actualArrival, actualDeparture)
        }
        Some(TripPlatformDetailDTO(tripId, trip, items.toSeq.sortBy(_.plannedArrival.get.getMillis)))
      case None => None
    }
  }

  def delayTripPlanAtStop(tripId:String, stopRef:String, delay:Long)= {
    val plannedTrip = PTStore.findTripDetails(tripId).get.toList.sortBy(_.time.getMillis)
    val needUpdateTrip = plannedTrip.span(_.stopRef != stopRef)._2.groupBy(_.stopRef).map {
      case (nodeId, calls) =>
        val arrivalTime = if (nodeId == stopRef) {
          // don't change the arrival time at start node
          calls.find(_.callType == 0).map(_.time)
        } else {
          calls.find(_.callType == 0).map(_.time.minusSeconds(-1*delay.toInt))
        }
        val departureTime = calls.find(_.callType == 1).map(_.time.minusSeconds(-1*delay.toInt))
        PTCallItem(arrivalTime, departureTime, nodeId)
    }.toList.sortBy(_.a.get.getMillis)
    updateTripPlanAfterStop(tripId, stopRef, needUpdateTrip)

  }

  def skipStopAtStop(tripId:String, stopRef:String) = {
    val plannedTrip = PTStore.findTripDetails(tripId).get.toList.sortBy(_.time.getMillis)
    val plannedArrivalTime = plannedTrip.find(call => call.stopRef == stopRef && call.callType ==0).get
    val plannedDepartureTime = plannedTrip.find(call => call.stopRef == stopRef && call.callType ==1).get
    val diff = (plannedDepartureTime.time.getMillis - plannedArrivalTime.time.getMillis)/1000
    delayTripPlanAtStop(tripId, stopRef, -1*diff)
  }

  /**
    * this function will merge two trip together
    * startTrip will be terminate
    * endTrip will be start from the endTripStopRef
    * @param startTripId
    * @param startTripStopRef
    * @param endTripId
    * @param endTripStopRef
    */
  def mergeTrips(startTripId:String, startTripStopRef:String, endTripId:String, endTripStopRef:String):Unit = {
    val startTrip = PTStore.findTripDetails(startTripId).get.toList.sortBy(_.time.getMillis)
    val endTrip = PTStore.findTripDetails(endTripId).get.toList.sortBy(_.time.getMillis)

    val startTripTrip = startTrip.head.trip
    val startTripTrainType = startTrip.head.trainType
    val startTripSpeedBand = startTrip.head.speedBandName
    val endTripTrip = endTrip.head.trip
    val endTripTrainType = endTrip.head.trainType
    val endTripSpeedBand = endTrip.head.speedBandName


    val (unchangedStartTrip, changedStartTrip) = startTrip.span(_.stopRef != startTripStopRef)
    val (changedEndTrip, unchangedEndTrip) = endTrip.span(_.stopRef != endTripStopRef)
    val changedNode = changedStartTrip.head
    // terminate startTrip
    changedStartTrip.foreach(removeTripCall)
    // modify the last node of startTrip
    addTripCall(startTripId, startTripTrip, startTripTrainType, startTripSpeedBand)(PTCallItem(Some(changedNode.time), Some(unchangedEndTrip.head.time), s = changedNode.stopRef))
    // terminate endTrip
    changedEndTrip.foreach(removeTripCall)
    // create new trips
  }

  def updateTripPlan(tripId:String, newPlan:List[PTCallItem]) = {
    val plannedTrip = PTStore.findTripDetails(tripId).get.toList.sortBy(_.time.getMillis)
    val groupByStop = plannedTrip.groupBy(_.stopRef).map(pair => {
      PTCallItem(pair._2.find(_.callType == 0).map(_.time),
        pair._2.find(_.callType == 1).map(_.time),
        pair._1)
    }).toList.sortBy(_.a.get.getMillis)
    val (same, changed) = (groupByStop zip newPlan).span(pair => {
      !(pair._1.a != pair._2.a || pair._1.d != pair._2.d || pair._1.s != pair._2.s)
    })
    if(!changed.isEmpty) {
      val updateList:List[PTCallItem] = newPlan.span(_.s != changed.head._2.s)._2
      updateTripPlanAfterStop(tripId, changed.head._1.s, updateList)
    }
  }

  /**
    * update trip plan from stopRef
    * @param tripId
    * @param stopRef
    * @param newPlan
    */
  private def updateTripPlanAfterStop(tripId:String, stopRef:String, newPlan:List[PTCallItem]):Unit = {
    val plannedTrip = PTStore.findTripDetails(tripId).get.toList.sortBy(_.time.getMillis)
    val needRemoveTrip = plannedTrip.span(_.stopRef != stopRef)._2
    val trip = plannedTrip.head.trip
    val trainType = plannedTrip.head.trainType
    val speedBandName = plannedTrip.head.speedBandName
    needRemoveTrip.foreach(oldcall => {
      // send trip plan remove msg to actor
      tripCtrl ! RemoveTripPlanCall("etg", tripId, oldcall.trip, oldcall.stopRef)
      // remove from PTStore
      PTStore.removeByTripIDAndStop(tripId, oldcall.stopRef)
    })
    // register those new item
    newPlan.foreach(addTripCall(tripId, trip, trainType, speedBandName))

  }

  private def addTripCall (tripId:String, trip:String, trainType:String, speedBandName:String) = (callItem:PTCallItem) => {
    val arrivalDTO = PTOrderedCallDTO(
      tripId = tripId,
      trip = trip,
      trainType = trainType,
      time = callItem.a.get,
      stopRef = callItem.s,
      value = 0,
      displayTime = "",
      speedBandName = speedBandName,
      callType = 0
    )
    val departureDTO = PTOrderedCallDTO(
      tripId = tripId,
      trip = trip,
      trainType = trainType,
      time = callItem.d.get,
      stopRef = callItem.s,
      value = 0,
      displayTime = "",
      speedBandName = speedBandName,
      callType = 1
    )
    // add arrival
    PTStore.addCall(arrivalDTO)
    // add departure
    PTStore.addCall(departureDTO)

    // send trip register to trip controller
    tripCtrl ! RequestTrackTrip("etg", tripId, trip, callItem.a.get, arrivalDTO, departureDTO)
  }

  private val removeTripCall= (call: PTOrderedCallDTO) => {
    // send trip plan remove msg to actor
    tripCtrl ! RemoveTripPlanCall("etg", call.tripId, call.trip, call.stopRef)
    // remove from PTStore
    PTStore.removeByTripIDAndStop(call.tripId, call.stopRef)
  }
}
