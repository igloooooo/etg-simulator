package service.dataset

import javax.inject.{Inject, Singleton}

import akka.stream.Materializer
import akka.stream.alpakka.csv.scaladsl.{CsvParsing, CsvToMap}
import akka.stream.scaladsl.{ Sink, StreamConverters}
import play.api.Logger
import play.api.inject.ApplicationLifecycle

import scala.concurrent.ExecutionContext

@Singleton
class DataStore @Inject() (appLifecycle: ApplicationLifecycle)(implicit val mat: Materializer, ec: ExecutionContext) {
  Logger.info(s"DataStore: Starting application")
  // load etg master data
  StreamConverters
    .fromInputStream(() => getClass.getResourceAsStream("/Board.csv"))
    .via(CsvParsing.lineScanner())
    .via(CsvToMap.toMap())
    .runWith(Sink.foreach(f => {
      PTSectorMap.boardMap += (f("ID").utf8String.toInt -> f("Name").utf8String)
    }))
    .onComplete(f => {
      StreamConverters
        .fromInputStream(() =>
          getClass.getResourceAsStream("/ImportedAreaSector.csv"))
        .via(CsvParsing.lineScanner())
        .via(CsvToMap.toMap())
        .runWith(Sink.foreach(f => {
          PTSectorMap.addAreaSector(f("BoardID").utf8String,
            f("Name").utf8String,
            f("SeqNr").utf8String.toInt)
        }))
        .onComplete(f => {
          StreamConverters
            .fromInputStream(() =>
              getClass.getResourceAsStream(
                "/ImportedAreaSectorDisplayNode.csv"))
            .via(CsvParsing.lineScanner())
            .via(CsvToMap.toMap())
            .runWith(Sink.foreach(f => {
              PTSectorMap.addAreaSectorDisplayNode(
                f("BoardID").utf8String,
                f("SectorName").utf8String,
                f("NodeID").utf8String,
                f("SeqNr").utf8String.toInt,
                f("DistanceToNextSectorNode").utf8String.toInt,
                0,
                f("ShowInETG").utf8String.toBoolean
              )
            }))
            .onComplete(f => {
              StreamConverters
                .fromInputStream(() =>
                  getClass.getResourceAsStream(
                    "/ImportedAreaSectorDecisionNode.csv"))
                .via(CsvParsing.lineScanner())
                .via(CsvToMap.toMap())
                .runWith(Sink.foreach(f => {
                  PTSectorMap.rawDecisionNodeList += AreaSectorDecisionNode(
                    f("BoardID").utf8String.toInt,
                    f("SectorName").utf8String,
                    f("NodeID").utf8String
                  )
                }))
                .onComplete(f => {
                  StreamConverters
                    .fromInputStream(() =>
                      getClass.getResourceAsStream(
                        "/ImportedTopologyNode.csv"))
                    .via(CsvParsing.lineScanner())
                    .via(CsvToMap.toMap())
                    .runWith(Sink.foreach(f => {
                      val isPlatform = if(f("IsPlatform").utf8String.isEmpty) false else f("IsPlatform").utf8String.toBoolean
                      PTSectorMap.rawNodeMap += ImportedTopologyNode(
                        f("NodeID").utf8String,
                        f("NodeName").utf8String,
                        f("NodeDescription").utf8String,
                        f("NetworkLevel").utf8String,
                        f("ParentNodeID").utf8String,
                        f("KMPost").utf8String.toDouble,
                        isPlatform
                      )
                    }))
                    .onComplete(f => {
                      PTSectorMap.reMap
                      Logger.info("Sector & Area init completed")

                    })
                })
            })
        })
    })

}
