package service.dataset

import play.api.Logger

object ETGDisplayUtil {

  /**
    * this function is for removing deuplicated start point, e.x. 109C
    * For example: in MainB Main North, CE16 only can be end of trip, not start of trip
    *
    */
  def removeDuplicatedStartNode(sectorKey:String, tripCalls:List[PTOrderedCallDTO]):List[PTOrderedCallDTO] = {
    if(sectorKey == "MainNorth") {
      // remove CE16 from the start point and CE18 from the end point
      removeDuplicatedNode("CE16", "CE18", tripCalls)
    } else {
      tripCalls
    }
  }

  def removeEmptyNodeFromHeadAndLast(calls:List[List[Long]]):List[List[Long]] = {
    val checkedHead = if(calls.head.last == -1) calls.tail else calls
    if(checkedHead.size > 1) {
      if(checkedHead.last.last == -1) {
        checkedHead.dropRight(1)
      } else {
        checkedHead
      }
    } else {
      List()
    }
  }

  private def removeDuplicatedNode(removeStartNodeRef:String,
                                   removeEndNodeRef:String,
                                   tripCalls:List[PTOrderedCallDTO]) : List[PTOrderedCallDTO] = {
    val sortedTrips = tripCalls.sortBy(_.time.getMillis)
    if (sortedTrips.head.stopRef == removeStartNodeRef) {
      sortedTrips.filter(_.stopRef != removeStartNodeRef)
    } else if(sortedTrips.last.stopRef == removeEndNodeRef) {
      sortedTrips.filter(_.stopRef != removeEndNodeRef)
    } else {
      sortedTrips
    }
  }

}
