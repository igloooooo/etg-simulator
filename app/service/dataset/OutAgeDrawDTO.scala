package service.dataset

import play.api.libs.json.Json

object OutAgeDrawDTO{
  implicit val residentReads = Json.reads[OutAgeDrawDTO]
  implicit val residentWrite = Json.writes[OutAgeDrawDTO]
}
case class OutAgeDrawDTO (name:String, index:Int, startX:Long, startY:Int, endX:Long, endY:Int, description:String)
