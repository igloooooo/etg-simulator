package service.dataset

import api.dto.OutAgeLinkDTO

import scala.collection.mutable.ListBuffer

object OutAgeStore {
  val outAgeList: ListBuffer[OutAgeLinkDTO] = new ListBuffer[OutAgeLinkDTO]()
  var outAgeSectorMap = Map[String, ListBuffer[OutAgeLinkDTO]]()

  def removeOutAges() = {
    outAgeList.clear()
    outAgeSectorMap = Map[String, ListBuffer[OutAgeLinkDTO]]()
  }
  def addOutAgeLink(dto: OutAgeLinkDTO) = {

    outAgeList += dto

    PTSectorMap.decisionNodeMap.get(dto.fromNode) match {
      case Some(sectorNameList) =>
        sectorNameList.foreach(parseOutAgeNode(_, dto))
      case None =>
    }

  }

  def findOutAgeByBorder(borderId: Int): Seq[OutAgeDrawDTO] = {
    PTSectorMap.areaSectorMap
      .get(borderId)
      .map(f => f.flatMap(area => findOutAgeBySector(area.key, area.seqNr)))
      .get
      .toSeq
  }

  def findOutAgeBySector(sectorName: String,
                         seqNr: Int): Seq[OutAgeDrawDTO] = {
    outAgeList
      .groupBy(_.id)
      .flatMap(
        f =>
          convertOutageToSectorValue(f._1,
            generateDescriptionOfOutage(f._2.head),
            sectorName,
            seqNr,
            f._2))
      .toSeq
  }

  private def generateDescriptionOfOutage(
                                           outAgeLinkDTO: OutAgeLinkDTO): String = {
    s"${outAgeLinkDTO.id} - ${outAgeLinkDTO.comments} - ${outAgeLinkDTO.approvedStartDateTime} - ${outAgeLinkDTO.approvedEndDateTime}"
  }
  private def convertOutageToSectorValue(
                                          id: String,
                                          description: String,
                                          sectorName: String,
                                          seqNr: Int,
                                          list: Seq[OutAgeLinkDTO]): Option[OutAgeDrawDTO] = {
    val validNodeList = list
      .flatMap(dto => {
        val etgNodeList = new ListBuffer[(Int, Long, Long)]()

        PTSectorMap.convertToSectorData(dto.fromNode, sectorName) match {
          case Some(pair) =>
            if (pair._1 == sectorName) {
              etgNodeList += ((pair._2,
                dto.approvedStartDateTime.getMillis,
                dto.approvedEndDateTime.getMillis))
            }
          case None =>
        }
        PTSectorMap.convertToSectorData(dto.toNode, sectorName) match {
          case Some(pair) =>
            if (pair._1 == sectorName) {
              etgNodeList += ((pair._2,
                dto.approvedStartDateTime.getMillis,
                dto.approvedEndDateTime.getMillis))
            }
          case None =>
        }
        etgNodeList.toList
      })
      .distinct
      .sortBy(_._1)
    // we only need start node and end node per id
    validNodeList.toList match {
      case Nil =>
        None
      case head :: Nil =>
        Some(
          OutAgeDrawDTO(id,
            seqNr - 1,
            head._2,
            head._1,
            head._3,
            head._1,
            description))
      case head :: (_ :+ last) =>
        Some(
          OutAgeDrawDTO(id,
            seqNr - 1,
            head._2,
            head._1,
            last._3,
            last._1,
            description))
    }
  }

  private def parseOutAgeNode(sectorName: String, dto: OutAgeLinkDTO) = {
    outAgeSectorMap.get(sectorName) match {
      case Some(list) => list += dto
      case None =>
        val buffer = new ListBuffer[OutAgeLinkDTO]()
        buffer += dto
        outAgeSectorMap += (sectorName -> buffer)
    }
  }
}
