package service.dataset

import play.api.libs.json.Json

object PTDrawDTO{
  implicit val residentReads = Json.reads[PTDrawDTO]
  implicit val residentWrite = Json.writes[PTDrawDTO]
}
/**
  *
  * @param trip trip name
  * @param calls array of [time, value, ...][time, value, ...]
  */
case class PTDrawDTO(tripId: String, trip:String, sectorName:String, index:Int, isPeakHourTrip:Boolean, isChanged:Boolean, calls:List[List[Long]])
