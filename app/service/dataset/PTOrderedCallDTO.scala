package service.dataset

import org.joda.time.DateTime

object PTOrderedCallDTO {}

// for the type 0 - aim arrival, 1 - aim departure
case class PTOrderedCallDTO(tripId: String,
                            trip: String,
                            trainType: String,
                            time: DateTime,
                            stopRef: String,
                            value: Int,
                            displayTime: String,
                            speedBandName:String,
                            callType: Int = 0)
  extends Ordered[PTOrderedCallDTO] {
  override def compare(that: PTOrderedCallDTO) = {
    if (time.isBefore(that.time))
      -1
    else if (time.isAfter(that.time))
      1
    else {
      // if it is the same time, check the vm type, put arrival at front
      if (callType == that.callType) {
        0
      } else if(callType == 0) {
        -1
      } else {
        1
      }
    }
  }
}
