package service.dataset

import api.dto.network.NetworkNode
import api.dto.{MarkLineItem, NodeDetails, SectorItem}
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object PTSectorMap {
  private val logger = LoggerFactory.getLogger(PTSectorMap.getClass)
  var nodeSectorMap = Map[String, String]()
  var boardMap = Map[Int, String]()
  // boardId -> list(AreaSector)
  var areaSectorMap = Map[Int, ListBuffer[AreaSector]]()
  private[this] var rawAreaSectorDisplayNode =
    Map[String, ListBuffer[AreaSectorDisplayNode]]()
  // map: sector -> list of node
  var areaSectorDisplayNodeMap = Map[String, List[AreaSectorDisplayNode]]()
  // map: (sector, nodeid) -> display node
  var areaSectorDisplayNodeValueMap =
    Map[(String, String), AreaSectorDisplayNode]()
  // raw data, don't use it
  val rawNodeMap = ListBuffer[ImportedTopologyNode]()
  // master node map: nodeid-> node details
  var masterNodeMap = Map[String, ImportedTopologyNode]()
  // all node map: node id ->  node details
  var microNodeMap = Map[String, ImportedTopologyNode]()
  // sectorName -> decision node list
  var decisionNodeMap = Map[String, List[String]]()

  val rawDecisionNodeList = ListBuffer[AreaSectorDecisionNode]()

  def addBorder(boardID: String, name: String): Unit = {
    val key = convertSectorNameToKey(name)

    boardMap += (boardID.toInt -> key)
  }
  def addAreaSector(boardID: String, name: String, seqNr: Int) = {
    val key = convertSectorNameToKey(name)
    areaSectorMap.get(boardID.toInt) match {
      case Some(list) => list += AreaSector(boardID.toInt, name, seqNr, key)
      case None =>
        areaSectorMap += (boardID.toInt -> ListBuffer(
          AreaSector(boardID.toInt, name, seqNr, key)))
    }
  }

  def addAreaSectorDisplayNode(boardID: String,
                               sectorName: String,
                               nodeID: String,
                               seqNr: Int,
                               distanceToNextSectorNode: Int,
                               displayValue: Int = 0,
                               isShow: Boolean = true) = {
    val key = convertSectorNameToKey(sectorName)
    rawAreaSectorDisplayNode.get(key) match {
      case Some(buff) =>
        buff += AreaSectorDisplayNode(boardID.toInt,
                                      key,
                                      nodeID,
                                      seqNr,
                                      distanceToNextSectorNode,
                                      displayValue,
                                      isShow)
      case None =>
        rawAreaSectorDisplayNode += (key -> ListBuffer(
          AreaSectorDisplayNode(boardID.toInt,
                                key,
                                nodeID,
                                seqNr,
                                distanceToNextSectorNode,
                                displayValue,
                                isShow)))
    }
  }

  /**
    * based on the decision node map, one stop can be displayed in a few sector, not just one
    * @param stopRef
    * @return list of (sector key, display value)
    */
  def convertToSectorData(stopRef: String): Option[List[(String, Int)]] = {
    decisionNodeMap.get(stopRef) match {
      case Some(sectorNameKeyList) =>
        microNodeMap.get(stopRef) match {
          case Some(node) =>
            // find parent node value
            if (node.ParentNodeID.isEmpty) {
              logger.info("why it is empty? {}", node.NodeID)
              None
            } else {
              val convertedList = sectorNameKeyList
                .map(sectorNameKey => {
                  areaSectorDisplayNodeValueMap.get(
                    (sectorNameKey, node.ParentNodeID)) match {
                    case Some(sectorNode) =>
                      Some((sectorNode.sectorName, sectorNode.displayValue))
                    case None => None
                  }
                })
                .flatten
              Some(convertedList)
            }
          case None => None
        }
      case None => None
    }

  }

  /**
    *
    * @param stopRef
    * @param sectorNameKey
    * @return (sectorName, value)
    */
  def convertToSectorData(stopRef: String,
                          sectorNameKey: String): Option[(String, Int)] = {
    decisionNodeMap.get(stopRef) match {
      case Some(sectorNameKeyList) =>
        microNodeMap.get(stopRef) match {
          case Some(node) =>
            // find parent node value
            if (node.ParentNodeID.isEmpty) {
              logger.info("why it is empty? {}", node.NodeID)
              None
            } else {
              sectorNameKeyList.find(_ == sectorNameKey) match {
                case Some(_) =>
                  areaSectorDisplayNodeValueMap.get(
                    (sectorNameKey, node.ParentNodeID)) match {
                    case Some(sectorNode) =>
                      Some((sectorNode.sectorName, sectorNode.displayValue))
                    case None => None
                  }
                case None => None
              }

            }
          case None => None
        }
      case None => None
    }

  }

  def findMasterNodeByBorder(borderId: Int): Seq[Seq[MarkLineItem]] = {
    areaSectorMap.get(borderId) match {
      case Some(list) =>
        val temp = list
          .sortBy(_.seqNr)
          .map(sector => findMasterNodeBySector(sector.key))
          .toSeq
        temp
      case None => Seq()
    }

  }

  def findSectorListByBorder(borderId:Int): Seq[SectorItem] = {
    areaSectorMap.get(borderId) match {
      case Some(list) =>
        val temp = list
          .sortBy(_.seqNr)
          .map(sector => SectorItem(borderId, sector.seqNr-1, sector.name, sector.key))
          .toSeq
        temp
      case None => Seq()
    }
  }

  def findMasterNodeBySector(sector: String): Seq[MarkLineItem] = {
    areaSectorDisplayNodeMap.get(sector) match {
      case Some(list) =>
        list
          .filter(_.isShow)
          .map(node => {
            val masterNode = masterNodeMap.get(node.nodeID)
            MarkLineItem(node.displayValue,
                         node.seqNr - 1,
                         masterNode.map(_.NodeName).getOrElse(""),
                         masterNode.map(_.KMPost).getOrElse(0.0),
                         masterNode.map(_.NodeID).getOrElse(""))
          })

      case None => Seq()
    }
  }

  def findMarkLineByTrip(trip:String):Seq[MarkLineItem] = {
    PTStore.pt.filterKeys(_ != PTStore.UNKNOWN).flatMap(_._2.get(trip)).flatten
      .map(_.stopRef).toList.distinct
      .map(stopRef => {
        // find the masterNode
        findMasterNodeByNode(stopRef) match {
          case Some(masterNode) =>
            Some(MarkLineItem((masterNode.KMPost*10 + 50).toInt,
              0,
              masterNode.NodeName,
              masterNode.KMPost,
              masterNode.NodeID))
          case None => None
        }
      }).flatten
        .toList.distinct.sortBy(_.yAxis)
      .toSeq
  }


  /**
    * find the master node by stopRef
    * @param node
    * @return
    */
  def findMasterNodeByNode(node:String):Option[ImportedTopologyNode] = {
    microNodeMap.get(node).map(_.ParentNodeID).map(parentNodeId => rawNodeMap.find(_.NodeID == parentNodeId).get)
  }

  def findNodeByMasterNode(masterNode:String):Option[List[ImportedTopologyNode]] = {
    masterNodeMap.get(masterNode).map(master => {
      microNodeMap.filter(_._2.ParentNodeID == master.NodeID).map(_._2).toList
    })
  }

  /**
    * check if the micro node is platform
    *
    * @param node micro node
    * @return
    */
  def isPlatform(node:String):Boolean = {
    microNodeMap.get(node).exists(_.IsPlatform)
  }

  /**
    * load micro node details
    * @param node
    * @return Option[ImportedTopologyNode]
    */
  def loadMicroNodeDetails(node:String):Option[ImportedTopologyNode] = {
    microNodeMap.get(node)
  }

  /**
    * find all nodes with same parent
    * @param node micro node
    * @return
    */
  def findAllNodesWithSameParent(node:String):Option[List[ImportedTopologyNode]] = {
    findMasterNodeByNode(node).map(parent => {
      microNodeMap.filter(_._2.ParentNodeID == parent.NodeID).map(_._2).toList
    })
  }

  /**
    * re-mapping all master data
    */
  def reMap = {
    areaSectorDisplayNodeMap = rawAreaSectorDisplayNode.map(pair => {
      val valuedList: List[AreaSectorDisplayNode] = pair._2
        .sortBy(_.seqNr)
        .scanRight(AreaSectorDisplayNode(1, "", "", 0, 0, 0, false))((op, b) =>
          op.copy(displayValue = op.distanceToNextSectorNode + b.displayValue))
        .toList
      (pair._1 -> valuedList)
    })
    decisionNodeMap = rawDecisionNodeList
      .groupBy(_.nodeID)
      .map(pair => {
        (pair._1 -> pair._2
          .map(node => convertSectorNameToKey(node.sectorName))
          .toList)
      })

    areaSectorDisplayNodeMap
      .flatMap(_._2)
      .foreach(node => {
        areaSectorDisplayNodeValueMap += ((node.sectorName, node.nodeID) -> node)
      })

    rawNodeMap
      .filter(_.NetworkLevel == "Meso")
      .toList
      .foreach(node => {
        masterNodeMap += (node.NodeID -> node)
      })
    rawNodeMap
      .filter(_.NetworkLevel == "Micro")
      .toList
      .foreach(node => {
        microNodeMap += (node.NodeID -> node)
      })

  }

  def convertSectorNameToKey(sectorName: String): String = {
    sectorName.replaceAll(" ", "").replaceAll("-", "")
  }

  def findNodeDetails(stopRef:String):Option[NodeDetails] = {
    rawNodeMap.find(_.NodeID == stopRef).map(node => {
      NodeDetails(
        NodeID = node.NodeID,
        NodeName = node.NodeName,
        NodeDescription = node.NodeDescription,
        NetworkLevel = node.NetworkLevel,
        ParentNodeID = node.ParentNodeID,
        KMPost = node.KMPost
      )
    })
  }

  def findSectorsByNode(stopRef:String):List[String] = {
    if(decisionNodeMap.get(stopRef).isDefined) {
      logger.info("find node: " + stopRef)
    }
    decisionNodeMap.getOrElse(stopRef, List())
  }

  private def findMasterNode(stopRef:String):Option[String] = {
    microNodeMap.get(stopRef) match {
      case Some(node) =>
        if(node.ParentNodeID.isEmpty)
          None
        else
          Some(node.ParentNodeID)
      case None => None
    }
  }
}

case class AreaSector(boardID: Int, name: String, seqNr: Int, key: String)
case class AreaSectorDisplayNode(boardID: Int,
                                 sectorName: String,
                                 nodeID: String,
                                 seqNr: Int,
                                 distanceToNextSectorNode: Int,
                                 displayValue: Int,
                                 isShow: Boolean)
case class AreaSectorDecisionNode(boardID: Int,
                                  sectorName: String,
                                  nodeID: String)
case class ImportedTopologyNode(NodeID: String,
                                NodeName: String,
                                NodeDescription: String,
                                NetworkLevel: String,
                                ParentNodeID: String,
                                KMPost: Double,
                                IsPlatform:Boolean)
