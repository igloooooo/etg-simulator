package service.dataset

import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.slf4j.LoggerFactory
import PTSectorMap.areaSectorMap
import api.dto.PlatformStatus
import play.api.Logger
import simulator.trip.ChangedTripStore
import util.PlaybackDateUtil

object PTStore {
  val UNKNOWN: String = "UNKNOWN"
  private val logger = LoggerFactory.getLogger(PTStore.getClass)
  // pt: sector-> set[PTOrderedCallDTO]
  var pt: scala.collection.mutable.Map[String, TripSectorMap] =
    scala.collection.mutable.HashMap[String, TripSectorMap]()
  pt += (UNKNOWN -> TripSectorMap())

  // tripMap: tripID->set[PTOrderedCallDTO]
  var tripMap:scala.collection.mutable.Map[String, Set[PTOrderedCallDTO]] =
    scala.collection.mutable.HashMap[String, Set[PTOrderedCallDTO]]()

  def removeTimeTable = {
    pt = scala.collection.mutable.HashMap[String, TripSectorMap]()
    pt += (UNKNOWN -> TripSectorMap())
    tripMap = scala.collection.mutable.HashMap[String, Set[PTOrderedCallDTO]]()
  }

  def isTimetableLoad = {
    !tripMap.isEmpty
  }

  def addCall(call: PTOrderedCallDTO) = {
    val tripId = call.tripId
    convertToSectorData(call) match {
      case Some(list) =>
        list.foreach(pair => {
          val sectorName = pair._1
          val value = pair._2
          pt.get(sectorName) match {
            case Some(_) =>
            case None =>
              logger.info("crate new sector {}", sectorName)
              pt += (sectorName -> TripSectorMap())
          }
          val callDTO = PTOrderedCallDTO(
            call.tripId,
            call.trip,
            call.trainType,
            call.time,
            call.stopRef,
            value,
            call.time toString ISODateTimeFormat.dateHourMinuteSecond,
            call.speedBandName,
            call.callType)
          val tripSectorMap = pt.get(sectorName).get

          tripSectorMap.put(tripId, tripSectorMap.get(tripId).map(_ :+ callDTO).getOrElse(Seq(callDTO)))

          // add into trip map
          addIntoTripMap(callDTO)
        })
        // we need add blank node to the same trip but in the others sector
        // because some trip will cross the sector and we can only display part of trip
        pt.filter(pair => !list.exists(_._1 == pair._1)).map(_._2).foreach(tripSectorMap => {
          if(tripSectorMap.isDefinedAt(tripId) && !tripSectorMap(tripId).isEmpty) {
            if(tripSectorMap.get(tripId).get.last.value > 0) {
              // only add one empty node
              val emptyCall = PTOrderedCallDTO(
                call.tripId,
                call.trip,
                call.trainType,
                call.time,
                call.stopRef,
                -1, // will be changed to '-' in the front
                call.time toString ISODateTimeFormat.dateHourMinuteSecond,
                call.speedBandName,
                call.callType)
              tripSectorMap.put(tripId, tripSectorMap.get(tripId).get :+ emptyCall)
            }
          }
        })

      case None =>
        logger.debug("missing stop in PTSectorMap.nodeSectorMap {}",
                     call.stopRef)
        val callDTO = PTOrderedCallDTO(
          call.tripId,
          call.trip,
          call.trainType,
          call.time,
          call.stopRef,
          0,
          call.time toString ISODateTimeFormat.dateHourMinuteSecond,
          call.speedBandName,
          call.callType)
        val unKnownTripSectorMap = pt.get(UNKNOWN).get
        unKnownTripSectorMap.put(tripId, unKnownTripSectorMap.get(tripId).map(_ :+ callDTO).getOrElse(Seq(callDTO)))
        // add into trip map
        addIntoTripMap(callDTO)

    }
  }

  def removeByTripIDAndStop(tripID: String, stopRef:String) = {
    // remove from pt
    pt.keys.foreach(key => {
      val sectorTripMap = pt(key)
      if(sectorTripMap.contains(tripID))
        sectorTripMap.put(tripID, sectorTripMap(tripID).filter(_.stopRef != stopRef))
    })
    // remove form trip map
    tripMap.put(tripID, tripMap(tripID).filter(_.stopRef != stopRef))
  }

  /**
    * find platform status
    * @param stopRef any platform node
    * @return
    */
  def findPlatformDetailsByStop(stopRef:String):Seq[PlatformStatus] = {
    // find all platform
    PTSectorMap.findNodeByMasterNode(stopRef) match {
      case Some(platforms) =>
        parsePlatforms(platforms.filter(_.IsPlatform))
      case None => Seq()
    }
  }

  def convertToSectorData(
      call: PTOrderedCallDTO): Option[List[(String, Int)]] = {
    PTSectorMap.convertToSectorData(call.stopRef)
  }

  def findByBorderAndSector(borderId:Int, sectorIndex:Int, startDate:DateTime, endDate:DateTime) : Seq[PTDrawDTO] = {
    areaSectorMap
      .get(borderId)
      .map(_.filter(_.seqNr == sectorIndex+1).flatMap(item =>
        findBySectorName(item.key, item.seqNr, startDate, endDate)).toSeq)
      .getOrElse(Seq())

  }

  def findByBorderId(borderId: Int, startDate:DateTime, endDate:DateTime): Seq[PTDrawDTO] = {
    areaSectorMap
      .get(borderId)
      .map(_.flatMap(item =>
        findBySectorName(item.key, item.seqNr, startDate, endDate)).toSeq)
      .getOrElse(Seq())
  }

  def findBySectorName(sector: String,
                       seqNr: Int,
                       startTime: DateTime,
                       endTime: DateTime): Seq[PTDrawDTO] = {
    pt.get(sector) match {
      case Some(tripSectorMap) =>
        tripSectorMap.flatMap(_._2)
          .filter(call =>
            call.time.isBefore(endTime) && call.time.isAfter(startTime))
          .groupBy(call => (call.tripId, call.trip))
          .map(pair => {
            val isTripChanged = ChangedTripStore.isTripChanged(pair._1._1)
            PTDrawDTO(
              pair._1._2 + "_" + PlaybackDateUtil.convertToDate(startTime), // todo: this date should been calculate by startDate and currently run time
              pair._1._2,
              sector,
              seqNr - 1,
              false,
              isTripChanged,
              ETGDisplayUtil.removeEmptyNodeFromHeadAndLast(pair._2.toList
                .map(
                  orderedCall =>
                    List(orderedCall.time.getMillis,orderedCall.value))
                .distinct
                .sortBy(_.head))

            )
          })
          .toSeq

      case None => Seq()
    }
  }

  /**
    *
    * @param trip
    * @return list -> [time, value ...]
    */
  def findByTrip(trip: String): Seq[List[Int]] = {
    pt.map(_._2.get(trip)).flatten.flatten.filter(_.value >= 0)
      .toSeq
      .map(
        orderedCall =>
          List(orderedCall.time.getMillis.toInt, orderedCall.value))
      .sortBy(_.head)
  }

  /**
    * find all related trip
    * it will filter all trips via its marklines and start-end time
    * @param tripID
    * @return
    */
  def findRelatedTrip(tripID:String):Seq[PTDrawDTO] ={
    // find trip
    val tripCalls = pt.filterKeys(_ != UNKNOWN).flatMap(_._2.get(tripID)).flatten.filter(_.value > 0)
      .toList.sortBy(_.time.getMillis)
    if (tripCalls.isEmpty) {
      return Seq()
    }
    val startCall = tripCalls.head
    val endCall = tripCalls.last

    val tripMasterNode = tripCalls
      .map(call => PTSectorMap.findMasterNodeByNode(call.stopRef).map(_.NodeID).getOrElse(""))
        .filter(!_.isEmpty)
        .distinct

    val tripStopList = tripCalls.map(_.stopRef)

    // filter all trips by marklines
    pt.filterKeys(_ != UNKNOWN).flatMap(pair => {
      pair._2.toList.map(_._2)
    }).flatten.filter(dto=> {
      dto.tripId == tripID ||
        (dto.time.isAfter(startCall.time) && dto.time.isBefore(endCall.time) && isSameLine(dto.stopRef, tripStopList) && dto.value > 0)
    }).groupBy(call => (call.tripId, call.trip))
      .filter(_._2.size > 2)
      .map(pair => {
        val isTripChanged = ChangedTripStore.isTripChanged(pair._1._1)
        PTDrawDTO(
          pair._1._2 + "_" + PlaybackDateUtil.convertToDate(startCall.time), // todo: this date should been calculate by startDate and currently run time
          pair._1._2,
          "Trip View",
          0,
          false,
          isTripChanged,
          pair._2
            .toList
            .map(
              orderedCall =>
                List(orderedCall.time.getMillis,
                  (PTSectorMap.findMasterNodeByNode(orderedCall.stopRef).get.KMPost*10 + 50).toInt))
            .distinct
            .sortBy(_.head)

        )
      })
      .toSeq
  }

  /**
    *
    * @param tripId
    * @param stopRef
    * @return
    */
  def findTripAtStop(tripId:String, stopRef:String):Iterable[PTOrderedCallDTO] = {
    tripMap.get(tripId) match {
      case Some(list) => list.filter(call => call.tripId == tripId && call.stopRef == stopRef)
      case None => List()
    }
  }

  def findTripAtSector(sector:String, tripId:String):Iterable[PTOrderedCallDTO] = {
    pt(sector).filter(_._1 == tripId).flatMap(_._2)
  }

  /**
    * trip will cross in the ETG, which means those trips have same master node
    * @param stopRef
    * @param tripMasterNodes
    * @return
    */
  def isCrossLine(stopRef:String, tripMasterNodes:List[String]):Boolean = {
    PTSectorMap.findMasterNodeByNode(stopRef) match {
      case Some(masterNode) =>
        tripMasterNodes.find(_ == masterNode.NodeID).isDefined
      case None =>
        false
    }
  }

  /**
    * trip will run on the same line
    * @param stopRef
    * @param tripCalls
    * @return
    */
  def isSameLine(stopRef:String, tripCalls:List[String]):Boolean = tripCalls.contains(stopRef)

  def findTripDetails(tripId:String):Option[Set[PTOrderedCallDTO]] = tripMap.get(tripId)

  def findTripDetails(tripId:String, sector:String):Option[Set[PTOrderedCallDTO]] = {
    pt.get(sector) match {
      case Some(tripSectMap) =>
        tripSectMap.get(tripId) match {
          case Some(dtos) => Some(dtos.toSet)
          case None => None
        }
      case None =>
        None
    }
  }

  def countTripPerMin():Map[Long, Int] = {
    val tripStartAndEndTime = tripMap.map {
      case (tripId, calls) =>
        val sortedCalls = calls.toList.sortBy(_.time.getMillis)
        (tripId, sortedCalls.head, sortedCalls.last)
    }
    val startOfToday = DateTime.now().withTimeAtStartOfDay()
    val intervalList = (0 to 60*24-1 toList).map(index => startOfToday.minusMinutes(-1*index))
    intervalList.map(interval => {
      val size = tripStartAndEndTime.filter(tuple3 => interval.isAfter(tuple3._2.time) && interval.isBefore(tuple3._3.time)).size
      (interval.getMillis -> size)
    }).toMap
  }

  private def addIntoTripMap(dto:PTOrderedCallDTO) = {
    tripMap.get(dto.tripId) match {
      case Some(callDTOSet) =>
        tripMap.put(dto.tripId, callDTOSet + dto)
      case None =>
        tripMap.put(dto.tripId, Set(dto))
    }
  }

  private def parsePlatforms(platforms:List[ImportedTopologyNode]):Seq[PlatformStatus] = {
    val platFormNodes = platforms.map(_.NodeID)
    pt.filterKeys(_ != UNKNOWN).flatMap(pair => {
      pair._2.toList.map(_._2)
    }).flatten
      .filter(dto => platFormNodes.contains(dto.stopRef))
      .groupBy(_.stopRef).flatMap {
      case (stopRef, list) =>
        list.groupBy(_.tripId) map {
          case (tripId, pair) =>
            if(pair.size < 2) {
              None
            } else {
              if (pair.exists(_.callType == 0) && pair.exists(_.callType == 1)) {
                val arrival = pair.find(_.callType == 0).get
                val departure = pair.find(_.callType == 1).get
                val duration:Long = departure.time.getMillis - arrival.time.getMillis
                Some(PlatformStatus(stopRef, platforms.find(_.NodeID == stopRef).map(_.NodeDescription).getOrElse(""), tripId, arrival.trip, arrival.time.getMillis, departure.time.getMillis, duration, false))
              } else {
                None
              }

            }
        }
    }.flatten.filter(_.duration >= 30*1000).toSeq.sortBy(_.startTime)
  }


//  def findTripLink(trip: String): Seq[NetworkLink] = {
//    val stopRefList = findTripStopDetailsByTrip(trip).map(_.stopRef)
//    stopRefList
//      .zip(stopRefList.tail)
//      .flatMap(pair => NetworkLinkMap.convertToLink(pair._1, pair._2))
//  }

}
