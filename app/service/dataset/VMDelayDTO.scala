package service.dataset

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

object VMDelayDTO {
  implicit val residentReads = Json.reads[VMDelayDTO]
  implicit val residentWrite = Json.writes[VMDelayDTO]
}

case class VMDelayDTO(tripId:String, trip:String, stopRef:String, time:DateTime, callType:Int, delay:Int, lat:Float, lon:Float)
