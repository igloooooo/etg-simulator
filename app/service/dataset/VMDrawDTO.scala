package service.dataset

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

object VMDrawDTO {
  implicit val residentReads = Json.reads[VMDrawDTO]
  implicit val residentWrite = Json.writes[VMDrawDTO]
}

/**
  *
  * @param tripId
  * @param trip
  * @param index
  * @param sectorName
  * @param stopRef
  * @param time
  * @param value
  * @param callType 0 - arrival 1 - departure
  * @param delay
  */
case class VMDrawDTO(tripId:String, trip:String, index:Int, sectorName:String, stopRef:String, time:DateTime, value:Int, callType:Int, delay:Int)
