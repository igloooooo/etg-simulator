package service.dataset

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._

object VMSkipStopDTO {
  implicit val residentReads = Json.reads[VMSkipStopDTO]
  implicit val residentWrite = Json.writes[VMSkipStopDTO]
}

case class VMSkipStopDTO(tripId:String, trip:String, stopRef:String, sectorName:String, time:DateTime, lat:Float, lon:Float)
