package service.dataset

import api.dto.network.{VMNetwork3dDTO, VMNetworkDTO, VerticesNode}
import api.dto.{MarkLineItem, PlatformStatus, TrainActualDto, TripStatus}
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.slf4j.LoggerFactory
import play.api.Logger
import service.dataset.PTSectorMap.areaSectorMap
import service.dataset.PTStore.{UNKNOWN, pt}
import service.network.{NetworkLinkMap, NodeGeoMap}
import simulator.trip.ChangedTripStore
import util.PlaybackDateUtil

import scala.collection.mutable.ListBuffer

object VMStore {
  private val logger = LoggerFactory.getLogger(PTStore.getClass)
  // vm: sector-> set[VMSectorMap]
  private var vmETG: scala.collection.mutable.Map[String, VMSectorMap] =
    scala.collection.mutable.HashMap[String, VMSectorMap]()
  vmETG += (UNKNOWN -> VMSectorMap())

  // vmNetwork: trip-> (fromNode, toNode)
  private var vmNetwork = scala.collection.mutable.HashMap[String, (VMDrawDTO, VMDrawDTO)]()

  private var vmSkipStops = ListBuffer[VMSkipStopDTO]()
  private var vmDelays = ListBuffer[VMDelayDTO]()
  // this if for statistic of trip per minute, (Interval -> Count), Interval is milliseconds
  private var tripPerMinute = scala.collection.mutable.HashMap[Long, Int]()
  // this if for statistic of total vm per minute, (Interval -> Count), Interval is milliseconds
  private var vmPerMinute = scala.collection.mutable.HashMap[Long, Int]()

  def reset = {
    vmETG = scala.collection.mutable.HashMap[String, VMSectorMap]()
    vmETG += (UNKNOWN -> VMSectorMap)
    vmDelays = ListBuffer[VMDelayDTO]()
    vmSkipStops = ListBuffer[VMSkipStopDTO]()
    tripPerMinute = scala.collection.mutable.HashMap[Long, Int]()
    vmPerMinute = scala.collection.mutable.HashMap[Long, Int]()
  }

  def addVM(dto:TrainActualDto) = {
    // add into etg
    addIntoETG(dto)
    // add into network
    addIntoNetwork(dto)
  }

  def findByBorderId(borderId: Int): Seq[PTDrawDTO] = {
    val startDate = DateTime.now().withTimeAtStartOfDay()
    val endDate = DateTime.now().minusDays(-1).withTimeAtStartOfDay()

    //    val startDate = DateTime.parse("2018-08-03T00:00:01")
    //    val endDate = DateTime.parse("2018-08-03T23:59:59")
    findByBorderId(borderId, startDate, endDate)
  }

  def findByBorderId(borderId: Int, startTime: DateTime, endTime:DateTime): Seq[PTDrawDTO] = {
    areaSectorMap
      .get(borderId)
      .map(_.flatMap(item =>
        findBySectorName(item.key, item.seqNr, startTime, endTime)).toSeq)
      .getOrElse(Seq())
  }

  def findBySectorName(sector: String,
                       seqNr: Int,
                       startTime: DateTime,
                       endTime: DateTime): Seq[PTDrawDTO] = {
    vmETG.get(sector) match {
      case Some(vMSectorMap) =>
        vMSectorMap.flatMap(_._2)
          .filter(call =>
            call.time.isBefore(endTime) && call.time.isAfter(startTime))
          .groupBy(call => (call.tripId, call.trip))
          .map(pair =>{
            val isTripChanged = ChangedTripStore.isTripChanged(pair._1._1)
            PTDrawDTO(
              pair._1._1,
              pair._1._2,
              sector,
              seqNr - 1,
              false,
              isTripChanged,
              pair._2
                .toList
                .map(
                  orderedCall =>
                    List(orderedCall.time.getMillis,orderedCall.value))
                .distinct
                .sortBy(_.head)
            )
          })
          .toSeq

      case None => Seq()
    }
  }

  def findAllVMByTrip(tripID:String):Seq[VMDrawDTO] = {
    vmETG.filterKeys(_ != UNKNOWN).flatMap(pair => {
      pair._2.toList.map(_._2)
    }).flatten.filter(_.tripId == tripID).toList.sortBy(_.time.getMillis)
  }

  /**
    * this function will return the vm only related to trip, including all other trips
    * @param tripId tripID
    * @param startTime
    * @param endTime
    * @return
    */
  def findByTripAndRelatedTrips(tripId:String, startTime: DateTime, endTime: DateTime):Seq[PTDrawDTO] ={
    // find trip
    val tripCalls = pt.filterKeys(_ != UNKNOWN).flatMap(_._2.get(tripId)).flatten
      .toList.sortBy(_.time.getMillis)
    if (tripCalls.isEmpty) {
      return Seq()
    }
    val startCall = tripCalls.head
    val endCall = tripCalls.last

    val tripMasterNode = tripCalls
      .map(call => PTSectorMap.findMasterNodeByNode(call.stopRef).map(_.NodeID).getOrElse(""))
      .filter(!_.isEmpty)
      .distinct

    val tripStopRefList = tripCalls.map(_.stopRef)

    vmETG.filterKeys(_ != UNKNOWN).flatMap(pair => {
      pair._2.toList.map(_._2)
    }).flatten.filter(call=> {
      (call.time.isBefore(endTime) && call.time.isBefore(endCall.time)
      && call.time.isAfter(startTime) && call.time.isAfter(startCall.time)
      && isSameLine(call.stopRef, tripStopRefList)
        && call.value > 0)
    }).groupBy(call => (call.tripId, call.trip))
      .map(pair => {
        val isTripChanged = ChangedTripStore.isTripChanged(pair._1._1)
        PTDrawDTO(
          pair._1._1,
          pair._1._2,
          "Trip View",
          0,
          false,
          isTripChanged,
          pair._2
            .toList
            .map(
              orderedCall =>
                List(orderedCall.time.getMillis,
                  (PTSectorMap.findMasterNodeByNode(orderedCall.stopRef).get.KMPost*10 + 50).toInt))
            .distinct
            .sortBy(_.head)

        )
      })
      .toSeq
  }

  def loadVMForNetwork:List[VMNetworkDTO] = {
    vmNetwork
      .filter(trip => trip._2._1.stopRef != trip._2._2.stopRef)
      .map(o => {
      // we need put the start node and endNode into the vertices so that it is easy for drawing
      val vertices = NetworkLinkMap.convertToLink(o._2._1.stopRef, o._2._2.stopRef).map(link => {
        List(VerticesNode(link.fromLocX, link.fromLocY, -1)) ::: link.vertices.toList ::: List(VerticesNode(link.toLocX, link.toLocY, -1))
      }).getOrElse(Seq())
      VMNetworkDTO(
        trip = o._1,
        fromNode = o._2._1.stopRef,
        toNode = o._2._2.stopRef,
        vertices = vertices,
        delay = o._2._2.delay // use the last node delay
      )
    }).filter(_.vertices.size > 0).toList
  }

  def loadVMForNetwork3d(): List[VMNetwork3dDTO] = {
    loadVMForNetwork.map(dto => {
      val fromGeoNode = NodeGeoMap.findGeoNodeByName(dto.fromNode)
      val toGeoNode = NodeGeoMap.findGeoNodeByName(dto.toNode)
      if(fromGeoNode.isDefined && toGeoNode.isDefined) {
        Some(VMNetwork3dDTO(
          trip = dto.trip,
          fromNode = dto.fromNode,
          toNode = dto.toNode,
          fromLon = fromGeoNode.get.longitude,
          fromLat = fromGeoNode.get.latitude,
          toLon = toGeoNode.get.longitude,
          toLat = toGeoNode.get.latitude
        ))
      } else {
        None
      }
    }).flatten.toList
  }

  /**
    * this function will return the last VM as trip status for the target trip
    * @param trip
    * @return
    */
  def findLatestVMByTrip(trip:String):Option[TripStatus] = {
    vmNetwork.get(trip).map(pair => {
      TripStatus(
        trip = trip,
        delay = pair._1.delay,
        lastUpdateTime = PlaybackDateUtil.convertToDBFormat(pair._2.time),
        status = "active"
      )
    })
  }

  /**
    * this function will return the platform status by master node
    * @param masterNode
    * @param startDate
    * @param endDate
    * @return
    */
  def findPlatformDetailsByMasterNode(masterNode:String, startDate:DateTime, endDate:DateTime):Seq[PlatformStatus] = {
    PTSectorMap.findNodeByMasterNode(masterNode) match {
      case Some(nodes) =>
        parsePlatforms(nodes.filter(_.IsPlatform), startDate, endDate)
      case None =>
        Seq()
    }
  }
  /**
    * this function will return the platform status by stopRef
    * @param stopRef the micro node, not master node
    * @param startDate
    * @param endDate
    * @return
    */
  def findPlatformDetailsByStop(stopRef:String, startDate:DateTime, endDate:DateTime):Seq[PlatformStatus] = {
    // find all platform
    PTSectorMap.findAllNodesWithSameParent(stopRef) match {
      case Some(platforms) =>
        parsePlatforms(platforms.filter(_.IsPlatform), startDate, endDate)
      case None => Seq()
    }
  }

  def findAllSkipStops = vmSkipStops
  def findAllDelays = vmDelays
  def findAllVM:Iterable[VMDrawDTO] = vmETG.flatMap(_._2.toList.map(_._2)).flatten
  def findCurrentRunningTrips:List[String] = vmNetwork.map(_._1).toList
  def findTripPerMinuteStatis = tripPerMinute
  def findVMPerMinuteStatis = vmPerMinute
  def finalAllTrips:List[String] = vmETG.filter(_._1 != UNKNOWN).flatMap(_._2.keys).toList.distinct

  private def parsePlatforms(platforms:List[ImportedTopologyNode], startDate:DateTime, endDate:DateTime):Seq[PlatformStatus] = {
    val platFormNodes = platforms.map(_.NodeID)
    vmETG.filterKeys(_ != UNKNOWN).flatMap(pair => {
      pair._2.toList.map(_._2)
    }).flatten
      .filter(dto => platFormNodes.contains(dto.stopRef) && dto.time.isAfter(startDate) && dto.time.isBefore(endDate))
      .groupBy(_.stopRef).flatMap {
      case (stopRef, list) =>
        list.groupBy(_.tripId) map {
          case (tripId, pair) =>
            if(pair.size < 2) {
              None
            } else {
              if (pair.exists(_.callType == 0) && pair.exists(_.callType == 1)) {
                val arrival = pair.find(_.callType == 0).get
                val departure = pair.find(_.callType == 1).get
                val duration:Long = departure.time.getMillis - arrival.time.getMillis
                Some(PlatformStatus(stopRef, platforms.find(_.NodeID == stopRef).map(_.NodeDescription).getOrElse(""), tripId, arrival.trip, arrival.time.getMillis, departure.time.getMillis, duration, true))
              } else {
                None
              }

            }
        }
    }.flatten.filter(_.duration >= 30*1000).toSeq.sortBy(_.startTime)
  }


  private def isCrossInETG(targetStop:String, tripMasterNode:List[String]):Boolean = {
    PTSectorMap.findMasterNodeByNode(targetStop) match {
      case Some(masterNode) =>
        tripMasterNode.find(_ == masterNode.NodeID).isDefined
      case None => false
    }
  }

  private def isSameLine(targetStop:String, tripCallStops:List[String]):Boolean = tripCallStops.contains(targetStop)

  private def addIntoETG(dto:TrainActualDto) = {
    val tripId = dto.trip + "_" + PlaybackDateUtil.convertToDate(dto.time)
    PTSectorMap.convertToSectorData(dto.stopRef) match {
      case Some(list) =>
        list.foreach(pair => {
          val sectorName = pair._1
          val value = pair._2
          vmETG.get(sectorName) match {
            case Some(_) =>
            case None =>
              logger.info("crate new sector {}", sectorName)
              vmETG += (sectorName -> VMSectorMap())
          }
          val vmSectorMap = vmETG(sectorName)
          val callDTO = VMDrawDTO(
            tripId = tripId,
            trip = dto.trip,
            index = 0, // has not been used in there,
            sectorName=sectorName,
            stopRef = dto.stopRef,
            time = dto.time,
            value = value,
            callType = dto.actualType,
            delay = calculateDelay(dto).getOrElse(-1)
          )
          vmSectorMap.put(tripId, vmSectorMap.get(tripId).map(_ :+ callDTO).getOrElse(Seq(callDTO)))
          // check if it is skip stop
          checkIfSkipStops(callDTO) match {
            case Some(vmSkipStopDTO) =>
              vmSkipStops.append(vmSkipStopDTO)
            case None =>

          }

        })
        // we need add blank node to the same trip but in the others sector
        // because some trip will cross the sector and we can only display part of trip
        vmETG.filter(pair => !list.exists(_._1 == pair._1)).map(_._2).foreach(vmSectorMap => {
          if(vmSectorMap.isDefinedAt(tripId)) {
            if(vmSectorMap.get(tripId).get.last.value > 0) {
              // only add one empty node
              val emptyCall = VMDrawDTO(
                tripId = tripId,
                trip = dto.trip,
                index = 0, // has not been used in there,
                sectorName=vmSectorMap(tripId).head.sectorName,
                stopRef = dto.stopRef,
                time = dto.time,
                -1, // it will be changed to '-' in the etg
                callType = dto.actualType,
                delay = calculateDelay(dto).getOrElse(-1)
              )
              vmSectorMap.put(tripId, vmSectorMap.get(tripId).get :+ emptyCall)
            }
          }
        })

      case None =>
        // those nodes can only been display in network view
        val vmSectorMap = vmETG(UNKNOWN)
        val callDTO = VMDrawDTO(
          tripId = tripId,
          trip = dto.trip,
          index = 0, // has not been used in there,
          sectorName=UNKNOWN,
          stopRef = dto.stopRef,
          time = dto.time,
          value = 0,
          callType = dto.actualType,
          delay = calculateDelay(dto).getOrElse(-1)
        )
        vmSectorMap.put(tripId, vmSectorMap.get(tripId).map(_ :+ callDTO).getOrElse(Seq(callDTO)))
    }
  }

  /**
    * this function will generate data set for network view
    * if trip finished, the vm data need to be removed
    * @param dto
    */
  private def addIntoNetwork(dto:TrainActualDto) = {
    // remove the finished trips
    removeFinishedTrips
    vmNetwork.get(dto.trip) match {
      case Some(pair) =>
        if (pair._2.stopRef != dto.stopRef) {
          val delay = calculateDelay(dto) match {
            case Some(d) => d
            case None => pair._2.delay
          }
          if(delay > 0) {
            // add into delay list
            val location = NodeGeoMap.findGeoNodeByName(dto.stopRef)
            vmDelays.append(VMDelayDTO(
              tripId = dto.trip + "_" + PlaybackDateUtil.convertToDate(dto.time), //TODO: find a way to generate tripID
              trip = dto.trip,
              time = dto.time,
              stopRef = dto.stopRef,
              callType = dto.actualType,
              delay = delay,
              lon = location.map(_.longitude).getOrElse(200F),
              lat = location.map(_.latitude).getOrElse(200F)
            ))
          }
          val newNode = VMDrawDTO(
            tripId = dto.trip + "_" + PlaybackDateUtil.convertToDate(dto.time),
            trip = dto.trip,
            index = 0, // has not been used in there,
            sectorName=UNKNOWN,
            stopRef = dto.stopRef,
            time = dto.time,
            value = 0,
            callType = dto.actualType,
            delay = delay
          )
          vmNetwork.put(dto.trip, (pair._2, newNode))
        }
      case None =>
        val newNode = VMDrawDTO(
          tripId = dto.trip + "_" + PlaybackDateUtil.convertToDate(dto.time),
          trip = dto.trip,
          index = 0, // has not been used in there,
          sectorName=UNKNOWN,
          stopRef = dto.stopRef,
          time = dto.time,
          value = 0,
          callType = dto.actualType,
          delay = 0
        )
        val set = (newNode, newNode)
        vmNetwork.put(dto.trip, set)
    }
    // update statistic data
    updateTripPerMinuteMap(dto.time, vmNetwork.keys.size)
    updateVMPerMinuteMap(dto.time)
  }

  private def updateTripPerMinuteMap(time:DateTime, tripCount:Int):Unit = {
    val interval = time.withSecondOfMinute(0).getMillis
    tripPerMinute.put(interval, tripCount)
  }

  private def updateVMPerMinuteMap(time:DateTime):Unit = {
    val interval = time.withSecondOfMinute(0).getMillis
    vmPerMinute.put(interval, vmPerMinute.get(interval).getOrElse(0) + 1)
  }

  private def calculateDelay(dto:TrainActualDto):Option[Int] = {
    val tripId = dto.trip + "_" + PlaybackDateUtil.convertToDate(dto.time) // TODO: find a way to get tripID
    PTStore.findTripAtStop(tripId, dto.stopRef)
      .find(_.callType == dto.actualType)
      .map(call => ((dto.time.getMillis - call.time.getMillis)/1000).toInt)
  }

  private def removeFinishedTrips() = {
    vmNetwork = vmNetwork.filter(trip => {
      PTStore.findTripDetails(trip._1) match {
        case Some(calls) =>
          if(calls.last.stopRef == trip._2._2.stopRef) {
            false
          } else {
            true
          }
          true
        case None => true
      }
    })
  }

  private def checkIfSkipStops(vmDraw: VMDrawDTO):Option[VMSkipStopDTO] = {
    val vmPair = vmETG.get(vmDraw.sectorName).flatMap(_.get(vmDraw.tripId)).map(_.filter(_.stopRef == vmDraw.stopRef))
    vmPair match {
      case Some(pair) =>
        if (pair.size > 1) {
          val vmArrival = pair.find(_.callType == 0)
          val vmDeparture = pair.find(_.callType == 1)
          if (vmArrival.isDefined && vmDeparture.isDefined) {
            val actualDwell = vmDeparture.get.time.getMillis - vmArrival.get.time.getMillis
            PTStore.findTripDetails(vmDraw.tripId, vmDraw.sectorName).map(_.filter(_.stopRef == vmDraw.stopRef)) match {
              case Some(stopPair) =>
                if (stopPair.size < 1) {
                  None
                } else {
                  val planedArrival = stopPair.find(_.callType == 0).get.time
                  val planedDeparture = stopPair.find(_.callType == 1).get.time
                  val dwell = planedDeparture.getMillis - planedArrival.getMillis
                  if (actualDwell + 10 * 1000 < dwell && dwell > 30*1000) {
                    val location = NodeGeoMap.findGeoNodeByName(vmDraw.stopRef)
                    Some(VMSkipStopDTO(
                      tripId = vmDraw.tripId,
                      trip = vmDraw.trip,
                      stopRef = vmDraw.stopRef,
                      time = vmDraw.time,
                      sectorName = vmDraw.sectorName,
                      lon = location.map(_.longitude).getOrElse(200F),
                      lat = location.map(_.latitude).getOrElse(200F)
                    ))
                  } else {
                    None
                  }
                }
              case None =>
                None
            }
          } else {
            None
          }
        } else {
          None
        }
      case None => None
    }
  }
}
