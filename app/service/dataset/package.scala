package service

package object dataset {
  // tripID -> calls
  type TripSectorMap = scala.collection.mutable.Map[String, Seq[PTOrderedCallDTO]]
  def TripSectorMap() = scala.collection.mutable.Map[String, Seq[PTOrderedCallDTO]]()

  // tripID-> calls
  type VMSectorMap = scala.collection.mutable.Map[String, Seq[VMDrawDTO]]
  def VMSectorMap() = scala.collection.mutable.Map[String, Seq[VMDrawDTO]]()
}
