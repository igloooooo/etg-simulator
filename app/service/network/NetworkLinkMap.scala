package service.network

import api.dto.network.{NetworkLink, VerticesNode}
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * rawNetworkLinkSet can map to all timetable
  * virtualNetworkLinkSet is for drawing network, there are some node doesn't exist in time table
  *
  */
object NetworkLinkMap {
  private val logger = LoggerFactory.getLogger(NetworkLinkMap.getClass)

  // this is only for display and won't change after init
  var virtualNetworkLinkSet = mutable.Set[NetworkLink]()

  // timetableNetwork is following the timetable and is real node
  val timetableNetworkLinkSet = mutable.Set[NetworkLink]()

  private val verticesLinkMap = mutable.Map[(String, String), Seq[VerticesNode]]()
  def addLink(link: NetworkLink) = {
    virtualNetworkLinkSet += link
  }

  /**
    * mostly, read data from csv file and init map
    * @param fromNodeName
    * @param toNodeName
    * @param isRunningLine
    * @return
    */
  def addLink(fromNodeName: String,
              toNodeName: String,
              isRunningLine: Boolean) = {
    val fromNode =
      NetworkNodeMap.networkNodeSet.find(_.shortName == fromNodeName)
    val toNode = NetworkNodeMap.networkNodeSet.find(_.shortName == toNodeName)

    if (fromNode.isDefined && toNode.isDefined) {
      timetableNetworkLinkSet += NetworkLink(fromNode.get.shortName,
                                             toNode.get.shortName,
                                             fromNode.get.locX,
                                             fromNode.get.locY,
                                             toNode.get.locX,
                                             toNode.get.locY,
                                             isRunningLine)
    } else {
      logger.warn("Can not find node: " + fromNode)
      logger.warn("Can not find node: " + toNode)
    }
  }

  def findVerticesLink(fromNode:String, toNode:String):Seq[VerticesNode] = {
    verticesLinkMap.getOrElse((fromNode, toNode), Seq())
  }

  /**
    * stop or node out-service means all trip on this node need to be re-route
    * @param stopRef
    */
  def outServiceStop(stopRef: String): Unit = {
    val impactedLinks = timetableNetworkLinkSet.filter(link =>
      link.fromNode == stopRef || link.toNode == stopRef)
    // remove those links
    timetableNetworkLinkSet --= impactedLinks
    // add modified links
    timetableNetworkLinkSet ++= impactedLinks.map(_.copy(valid = false))
  }

  def backServiceStop(stopRef: String): Unit = {
    val impactedLinks = timetableNetworkLinkSet.filter(link =>
      link.fromNode == stopRef || link.toNode == stopRef)
    // remove those links
    timetableNetworkLinkSet --= impactedLinks
    // add modified links
    timetableNetworkLinkSet ++= impactedLinks.map(_.copy(valid = true))
  }

  def findNextStop(startStop: String): List[String] = {
    timetableNetworkLinkSet
      .filter(link => link.fromNode == startStop && link.valid)
      .map(_.toNode)
      .toList
  }

  def findAvailableLinkFromNode(startNode:String):List[NetworkLink] = {
    virtualNetworkLinkSet.filter(_.fromNode == startNode).toList
  }

  /**
    * vertices node is only for network display. They are not real node
    * @param fromNodeName
    * @param toNodeName
    * @param LocX
    * @param LocY
    * @param sequence
    * @return
    */
  def addVerticesLink(fromNodeName: String,
                      toNodeName: String,
                      LocX: Int,
                      LocY: Int,
                      sequence: Int) = {
    verticesLinkMap.get((fromNodeName, toNodeName)) match {
      case Some(list) =>
        val verticesSeq =
          (list :+ VerticesNode(LocX, LocY, sequence)).sortBy(_.seq)
        verticesLinkMap += ((fromNodeName, toNodeName) -> verticesSeq)
      case None =>
        verticesLinkMap += ((fromNodeName, toNodeName) -> Seq(
          VerticesNode(LocX, LocY, sequence)))
    }

  }

  /**
    * append vertices node into networkLinkSet
    */
  def massageNetworkLinkSet = {
    virtualNetworkLinkSet = timetableNetworkLinkSet.map(original =>
      verticesLinkMap.get((original.fromNode, original.toNode)) match {
        case Some(list) =>
          original.copy(vertices = list)
        case None =>
          original
    })
  }

  def getAllRunningLine(): Set[NetworkLink] = {
    val links = virtualNetworkLinkSet
        .map(link => {
          // put startNode and endNode into vertices for easy drawing
          link.copy(vertices = List(VerticesNode(link.fromLocX, link.fromLocY, -1)) ::: link.vertices.toList ::: List(VerticesNode(link.toLocX, link.toLocY, -1)))
        })
      .toSet.toList
    val mergeLines = ListBuffer[NetworkLink]()
    var mergeRest = links

    do{
      val startNode = mergeRest.head
      val targetForwardLinks = ListBuffer[NetworkLink](startNode)
      val targetBackwardLinks = ListBuffer[NetworkLink]()
      mergeRest =mergeLineForward(startNode, targetForwardLinks, mergeRest.tail)
      mergeRest = mergeLineBackward(startNode, targetBackwardLinks, mergeRest)

      val targetLink = targetBackwardLinks.toList.reverse ::: targetForwardLinks.toList
      // generate a new link
      val vertices = VerticesNode(targetLink.head.fromLocX, targetLink.head.fromLocY, -1) :: targetLink.flatMap(_.vertices.tail)

      mergeLines.append(NetworkLink(
        fromNode = targetLink.head.fromNode,
        fromLocX = targetLink.head.fromLocX,
        fromLocY = targetLink.head.fromLocY,
        toNode = targetLink.last.toNode,
        toLocX = targetLink.last.toLocX,
        toLocY = targetLink.last.toLocY,
        isRunningLine = true,
        vertices = vertices
      ))
    } while(mergeRest.size > 1)

    if (mergeRest.size == 1) {
      mergeLines.append(mergeRest.head)
    }

    mergeLines.toSet
  }

  def convertToLink(fromNodeName: String,
                    toNodeName: String): Option[NetworkLink] = {
    virtualNetworkLinkSet.find(link =>
      link.fromNode == fromNodeName && link.toNode == toNodeName)
  }

  /**
    * merge links to larger links
    * TODO: should consider BSD
    * @param links
    */
  private def mergeLineForward(startNode:NetworkLink, links: ListBuffer[NetworkLink], rests:List[NetworkLink]): List[NetworkLink] = {
    val splits = rests.span(source => {
      !(source.fromNode == startNode.toNode && startNode.fromNode != source.toNode)
    } )
    if (splits._2.size == 0) {
      // can't find node, the link end
      rests
    } else {
      links.append(splits._2.head)
      mergeLineForward(splits._2.head, links, splits._1 ::: splits._2.tail)
    }
  }

  private def mergeLineBackward(startNode:NetworkLink, links: ListBuffer[NetworkLink], rests:List[NetworkLink]): List[NetworkLink] = {
    val splits = rests.span(source => {
      !(source.toNode == startNode.fromNode && startNode.toNode != source.fromNode)
    })
    if (splits._2.size == 0) {
      // can't find node, the link end
      rests
    } else {
      links.append(splits._2.head)
      return mergeLineBackward(splits._2.head, links, splits._1 ::: splits._2.tail)
    }
  }



}
