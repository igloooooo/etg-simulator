package service.network

import api.dto.network.{NetworkNode, NetworkPlatformLabel}

import scala.collection.mutable

object NetworkNodeMap {
  var networkNodeSet = mutable.Set[NetworkNode]()
  def addNode(link: NetworkNode) = {
    networkNodeSet += link
  }

  def generatePlatformLabels():Set[NetworkPlatformLabel] = {
    networkNodeSet.filter(_.isPlatform).groupBy(_.longName).map{
      case (platformName, nodes) =>
        caculateLabelPosition(nodes.toList)
    }.toSet
  }

  def findNetworkNode(stopRef:String):Option[NetworkNode] = {
    networkNodeSet.find(_.shortName == stopRef)
  }

  private def caculateLabelPosition(nodes:List[NetworkNode]):NetworkPlatformLabel = {
    if(nodes.size == 1) {
      NetworkPlatformLabel(
        shortName = nodes.head.shortName,
        longName = nodes.head.longName,
        locX = nodes.head.locX,
        locY = nodes.head.locY - 3,
        rotate = 0
      )
    } else {
      if(nodes.head.locY == nodes.last.locY) {
        val maxLocX = nodes.map(_.locX).max
        NetworkPlatformLabel(
          shortName = nodes.head.shortName,
          longName = nodes.head.longName,
          locX = maxLocX + 3,
          locY = nodes.head.locY,
          rotate = 90
        )
      } else {
        val minLocY = nodes.map(_.locY).min
        NetworkPlatformLabel(
          shortName = nodes.head.shortName,
          longName = nodes.head.longName,
          locX = nodes.head.locX,
          locY = minLocY - 3,
          rotate = 0
        )
      }
    }
  }
}
