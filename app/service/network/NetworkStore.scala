package service.network

import java.nio.file.Paths
import javax.inject.{Inject, Singleton}

import akka.stream.Materializer
import akka.stream.alpakka.csv.scaladsl.{CsvParsing, CsvToMap}
import akka.stream.scaladsl.{FileIO, Sink}
import api.dto.network.{NetworkNode, NodeGeo}
import play.api.Logger
import play.api.inject.ApplicationLifecycle

import scala.concurrent.ExecutionContext
import scala.util.Try
@Singleton
class NetworkStore @Inject() (appLifecycle: ApplicationLifecycle)(implicit val mat: Materializer, ec: ExecutionContext) {
  /**
    * start to load network data
    */
  FileIO
    .fromPath(
      Paths.get(getClass.getResource("/Node-Rail-Network-SRT.csv").toURI))
    .via(CsvParsing.lineScanner())
    .via(CsvToMap.toMap())
    .runWith(Sink.foreach(f => {
      if (!f("Name").utf8String.isEmpty)
        NetworkNodeMap.addNode(
          NetworkNode(f("Name").utf8String,
            f("LongName").utf8String,
            f("LocX").utf8String.toInt,
            f("LocY").utf8String.toInt,
            f("IsJunction").utf8String.toBoolean,
            f("IsPublicTimingPoint").utf8String.toBoolean))
    }))
    .onComplete(_ => {
      // load node of network
      FileIO
        .fromPath(
          Paths.get(getClass.getResource("/Link-Rail-Network-SRT.csv").toURI))
        .via(CsvParsing.lineScanner())
        .via(CsvToMap.toMap())
        .runWith(Sink.foreach(f => {
          NetworkLinkMap.addLink(f("FromNodeName").utf8String,
            f("ToNodeName").utf8String,
            f("IsRunningLine").utf8String.toBoolean)
        }))
        .onComplete(parseVertices(_))
    })

  private[this] def parseVertices(f: Try[Any]): Unit = {
    //Vertices-Rail-Network-SRT.csv
    FileIO
      .fromPath(
        Paths.get(getClass.getResource("/Vertices-Rail-Network-SRT.csv").toURI))
      .via(CsvParsing.lineScanner())
      .via(CsvToMap.toMap())
      .runWith(Sink.foreach(f => {
        NetworkLinkMap.addVerticesLink(f("FromNodeName").utf8String,
          f("ToNodeName").utf8String,
          f("LocX").utf8String.toInt,
          f("LocY").utf8String.toInt,
          f("Sequence").utf8String.toInt)
      }))
      .onComplete(f => {
        NetworkLinkMap.massageNetworkLinkSet
        Logger.info("Link Vertices init completed.")
        // init speed ban
        parseRunTime
      })
  }

  private[this] def parseRunTime(): Unit = {
    //ImportedSectionalRunTime.csv
    FileIO
      .fromPath(
        Paths.get(getClass.getResource("/ImportedSectionalRunTime.csv").toURI))
      .via(CsvParsing.lineScanner())
      .via(CsvToMap.toMap())
      .runWith(Sink.foreach(f => {
        SectorRunTimeMap.addRuntimeItem(
          f("SpeedBand").utf8String,
          f("StartParentNodeID").utf8String,
          f("EndParentNodeID").utf8String,
          f("PassPassDuration").utf8String,
          f("MinPassPassDuration").utf8String,
          f("StartStopDuration").utf8String
        )
      }))
      .onComplete(f => {
        Logger.info("Speed Ban init completed.")
        parseNodeGeo
      })
  }

  private[this] def parseNodeGeo():Unit = {
    //ImportedSectionalRunTime.csv
    FileIO
      .fromPath(
        Paths.get(getClass.getResource("/NodeGeoInfo.csv").toURI))
      .via(CsvParsing.lineScanner())
      .via(CsvToMap.toMap())
      .runWith(Sink.foreach(f => {
        if(!f("LONGITUDE").utf8String.isEmpty
          && !f("LATITUDE").utf8String.isEmpty
          && !f("NODE_NAME").utf8String.isEmpty) {
          NodeGeoMap.addNode( NodeGeo(
            f("LOCATION_NAME").utf8String,
            f("SYSTEM_NAME").utf8String,
            f("LONGITUDE").utf8String.toFloat,
            f("LATITUDE").utf8String.toFloat,
            f("NODE_NAME").utf8String
          ))
        }

      }))
      .onComplete(f => {
        Logger.info("Node Geo init completed.")
        Logger.info("Network init completed.")
      })
  }
}
