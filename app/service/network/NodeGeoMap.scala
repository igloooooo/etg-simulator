package service.network

import api.dto.network.NodeGeo

import scala.collection.mutable

object NodeGeoMap {
  private var nodeGeoSet = mutable.Set[NodeGeo]()
  def addNode(link: NodeGeo) = {
    nodeGeoSet += link
  }
  def loadGeoNodes():List[NodeGeo] = {
    nodeGeoSet.toList
  }

  def findGeoNodeByName(nodeName:String):Option[NodeGeo] = {
    nodeGeoSet.find(_.nodeName == nodeName)
  }
}
