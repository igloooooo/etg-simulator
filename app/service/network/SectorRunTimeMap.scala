package service.network

object SectorRunTimeMap {
  val sectorRunTimeMap =
    scala.collection.mutable.Map[(String, String, String), SectorRunTimeItem]()

  def addRuntimeItem(speedBand: String,
                     startParentNodeID: String,
                     endParentNodeID: String,
                     passPassDuration: String,
                     minPassPassDuration: String,
                     startStopDuration: String) = {
    sectorRunTimeMap((speedBand, startParentNodeID, endParentNodeID)) =
      new SectorRunTimeItem(
        speedBand,
        startParentNodeID,
        endParentNodeID,
        convertToSeconds(passPassDuration),
        convertToSeconds(minPassPassDuration),
        convertToSeconds(startStopDuration)
      )
  }

  private def convertToSeconds(duration: String): Int = {
    duration.split(":").map(_.toInt).reduceLeft((x, y) => x * 60 + y)
  }

  def minPassTime(speedBand: String,
                  startParentNodeID: String,
                  endParentNodeID: String): Option[SectorRunTimeItem] =
    sectorRunTimeMap.get((speedBand, startParentNodeID, endParentNodeID))
}

case class SectorRunTimeItem(speedBand: String,
                             startParentNodeID: String,
                             endParentNodeID: String,
                             passPassDuration: Int,
                             minPassPassDuration: Int,
                             startStopDuration: Int)
