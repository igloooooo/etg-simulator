package service.network

object SpeedBanMap {
  private val speedBanMap = Map(
    "21" -> "C5",
    "22" -> "4CL",
    "23" -> "6CL",
    "24" -> "8CL",
    "25" -> "8GL",
    "10896" -> "2AL",
    "10897" -> "4AL",
    "11457" -> "Loco Hauled Pass",
    "11458" -> "Indian Pacific",
    "1" -> "High Speed (XPT)",
    "2" -> "Intercity",
    "3" -> "Hunter",
    "11456" -> "Explorer / Endeavour",
    "11459" -> "4th Gen Trains (H,A,M,B)",
    "11460" -> "Tangara",
    "11461" -> "Silver Sets (C,K,S)",
    "26" -> "TM30",
    "11462" -> "AK Cars - Electric Hauled",
    "11463" -> "MTP",
    "11464" -> "Hi-Rail",
    "11465" -> "AK Cars",
    "11466" -> "TM50",
    "11467" -> "TM80"
  )

  def findSpeedBanById(id: String) = speedBanMap.get(id)
}
