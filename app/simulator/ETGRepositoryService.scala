package simulator

import java.sql.Timestamp
import javax.inject.Inject

import akka.actor.ActorSystem
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.{DateTime, Instant}
import org.slf4j.LoggerFactory
import simulator.command.ETGCommand
import simulator.dto.command.{CommandExportItem, CommandItem}
import simulator.repository.{ETGCommandModel, ETGCommandTable}
import simulator.repository.DBConfig._
import slick.jdbc.H2Profile.api._

import scala.concurrent.{ExecutionContext, Future}

class ETGRepositoryService @Inject()(system: ActorSystem)(implicit ec: ExecutionContext){
  val logger = LoggerFactory.getLogger(this.getClass)

  def loadAllCommand():Future[Seq[CommandItem]] = {
    db.run(ETGCommandTable.result)
      .map(f =>
        f.map(command =>
          CommandItem(
            command.id,
            command.commandType,
            new Instant(command.executeTime.getTime) toString ISODateTimeFormat.dateHourMinuteSecond,
            command.description)))
  }

  def insertETGCommand(commandType: String,
                       jsonCommand: String,
                       executeTime: Timestamp) = {
    val qInsert = ETGCommandTable += ETGCommandModel(0,
                                                     commandType,
                                                     jsonCommand,
                                                     executeTime)
    db.run(qInsert)
  }

  def loadCommandsByIds(ids: Seq[Long]): Future[Seq[ETGCommand]] = {
    val query = ETGCommandTable.filter(_.id.inSet(ids))
    db.run(query.result).map(f => f.flatMap(_.convertToCommand))
  }

  def exportCommandsByIds(ids: Seq[Long]): Future[Seq[CommandExportItem]] = {
    val query = ETGCommandTable.filter(_.id.inSet(ids))
    db.run(query.result)
      .map(
        f =>
          f.map(
            c =>
              new CommandExportItem(c.commandType,
                                    c.jsonCommand,
                                    c.executeTime.getTime)))
  }

  def exportAllCommands: Future[Seq[CommandExportItem]] = {
    val query = ETGCommandTable
    db.run(query.result)
      .map(
        f =>
          f.map(
            c =>
              new CommandExportItem(c.commandType,
                                    c.jsonCommand,
                                    c.executeTime.getTime)))
  }
}
