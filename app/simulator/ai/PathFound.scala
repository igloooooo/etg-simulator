package simulator.ai

import org.slf4j.LoggerFactory
import service.dataset.{ImportedTopologyNode, PTSectorMap}
import service.network.NetworkLinkMap

import scala.collection.mutable.ListBuffer

object PathFound {
  val logger = LoggerFactory.getLogger(PathFound.getClass)

  def foundPath(start: ImportedTopologyNode,
                stop: ImportedTopologyNode): Seq[ImportedTopologyNode] = {

    var openList = new ListBuffer[CheckNode]()
    var closeList = new ListBuffer[CheckNode]()

    var startCheckNode = CheckNode(null, start, generateCostValue(node = start))
    var currentNode = startCheckNode
    var isFound = false

    // add start point to openList
    openList += startCheckNode
    while (!openList.isEmpty && !isFound) {
      // the head one is the min cost node
      currentNode = openList.head
      // remove the head
      openList = openList.tail
//      logger.info("checking {}", currentNode)
      if (currentNode.node == stop) {
        isFound = true
      } else {
        // add into close list
        closeList += currentNode
        // check all possible nodes of current node
        // put current node as parent of those
        val possibleNodeList =
          generateAvailableNode(currentNode, openList, closeList).map(node =>
            CheckNode(currentNode.node, node, generateCostValue(node)))
        // add them into open list
        openList ++= possibleNodeList
        openList = openList.sortBy(_.h)
      }
    }
    if (isFound)
      // generate the path
      (generatePath(closeList, currentNode).reverse :+ stop).toSeq
    else {
      logger.error("can not find the path.")
      Seq()
    }

  }

  private def foundMinCostNode(openList: ListBuffer[CheckNode]): CheckNode = {
    return openList.sortBy(node => Math.abs(node.node.KMPost)).head
  }

  /**
    * f(n) = g(n) + h(n)
    * g(n) is the 0
    * h(n) is the KMPost
    * @param node
    * @return
    */
  private def generateCostValue(node: ImportedTopologyNode): Double = {
    node.KMPost
  }

  private def removeNodeFromOpenList(node: ImportedTopologyNode,
                                     openList: ListBuffer[ImportedTopologyNode])
    : ListBuffer[ImportedTopologyNode] = {
    openList.filter(_ == node)
  }

  private def generateAvailableNode(
      node: CheckNode,
      openList: ListBuffer[CheckNode],
      closeList: ListBuffer[CheckNode]): List[ImportedTopologyNode] = {
    NetworkLinkMap
      .findNextStop(node.node.NodeID)
      .filter(availableNode => {
        // check if it has already been in open or close list
        !openList.exists(_.node.NodeID == availableNode) && !closeList.exists(
          _.node.NodeID == availableNode)
      })
      .map(PTSectorMap.microNodeMap.get(_).get)
  }

  private def generatePath(closeList: ListBuffer[CheckNode],
                           startNode: CheckNode): List[ImportedTopologyNode] = {
    closeList.find(_.node == startNode.parent) match {
      case Some(parent) =>
        if (parent.parent == null)
          List(parent.node)
        else
          generatePath(closeList, parent).::(parent.node)
      case None =>
        List()
    }

  }
}

case class CheckNode(parent: ImportedTopologyNode,
                     node: ImportedTopologyNode,
                     h: Double)
