package simulator.command

import java.sql.Timestamp
import javax.inject.{Inject, Named}

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import org.joda.time.DateTime
import play.api.libs.json.Json
import playback.channel.Protocol
import service.TripService
import simulator.ETGRepositoryService
import simulator.command.CommandController.ReplayCommands
import simulator.repository.ETGCommandType

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object CommandController {

  case class ReplayCommands(ids: Seq[Long]) extends Protocol
}

/**
  * CommandController will handle all commands, such as delay trip or block the node
  */
class CommandController @Inject()(@Named("node-controller")nodeController: ActorRef,
                                  @Named("trip-controller")tripController: ActorRef,
                                  @Named("trip-monitor")tripMonitor: ActorRef,
                                  tripService: TripService,
                                  etgRepositoryService: ETGRepositoryService)(implicit ec: ExecutionContext)
    extends Actor
    with ActorLogging {
  override def receive = {
    case _ @ReplayCommands(ids) =>
      replayCommandByIds(ids)

    case event: NodeBlocked =>
      // save command into db
      etgRepositoryService.insertETGCommand(
        ETGCommandType.BlockNode.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      nodeController forward event

    case event: NodeRelease =>
      etgRepositoryService.insertETGCommand(
        ETGCommandType.ReleaseNode.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      nodeController forward event

    case event: NodeReleaseAll =>
      etgRepositoryService.insertETGCommand(
        ETGCommandType.ReleaseAllNode.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      nodeController forward event

    case event: TerminateTrip =>
      etgRepositoryService.insertETGCommand(
        ETGCommandType.TerminateTrip.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      tripController forward event

    case event: DelayTrip =>
      etgRepositoryService.insertETGCommand(
        ETGCommandType.DelayTrip.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      tripController forward event

    case event: SkipStopOnTrip =>
      etgRepositoryService.insertETGCommand(
        ETGCommandType.SkipStopOnTrip.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      tripService.skipStopAtStop(event.tripId, event.stopRef)

    case event: DelayTripPlan =>
      etgRepositoryService.insertETGCommand(
        ETGCommandType.DelayTripPlan.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      tripService.delayTripPlanAtStop(event.tripId, event.startStopRef, event.seconds)

    case event: ResetTrip =>
      etgRepositoryService.insertETGCommand(
        ETGCommandType.ResetTrip.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      tripController forward event

    case event: UpdateTripPlan =>
      etgRepositoryService.insertETGCommand(
        ETGCommandType.UpdateTripPlan.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      tripService.updateTripPlan(event.tripId, event.calls.filter(!_.s.isEmpty).sortBy(_.a.get.getMillis))

    case event: MergeTripPlan =>
      etgRepositoryService.insertETGCommand(
        ETGCommandType.MergeTripPlan.toString,
        Json.stringify(Json.toJson(event)),
        new Timestamp(DateTime.now().getMillis))
      tripService.mergeTrips(event.startTripId, event.startTripStopRef, event.endTripId, event.endTripStopRef)

    case event: Reboot =>
      // reset all node firstly
      nodeController forward ResetNode

    // reset all nodes finished
    case event: ResetNodeFinish =>
      // start to reset all trips
      tripController forward Reboot
  }

  private def replayCommandByIds(ids: Seq[Long]) = {
    etgRepositoryService.loadCommandsByIds(ids).onComplete {
      case Success(commands) =>
        commands.foreach(receive(_))
      case Failure(e) =>
        log.warning("can NOT load commands by ids: {}", ids)
    }
  }

}
