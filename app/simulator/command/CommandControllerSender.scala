package simulator.command

import akka.actor.{ActorContext, ActorRef}

trait CommandControllerSender {
  def commandControllerSender(msg: Any)(implicit context: ActorContext,
                                        sender: ActorRef) = {
    context
      .actorSelection("akka://application/user/command-controller") ! msg
  }
}
