package simulator.command

import java.nio.file.Paths

import akka.stream.IOResult
import akka.stream.scaladsl.{FileIO, Source}
import play.api.libs.json.Json
import simulator.dto.command.CommandExportItem

import scala.concurrent.Future

object CommandParse {
  def parseCommandFile(
      path: String): Source[Seq[CommandExportItem], Future[IOResult]] = {
    FileIO
      .fromPath(Paths.get(path))
      .map(_.utf8String)
      .map(rawJson => Json.fromJson[Seq[CommandExportItem]](Json.parse(rawJson)).get)
  }
}
