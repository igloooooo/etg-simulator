package simulator.command

import akka.stream.{Attributes, Inlet, SinkShape}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler}
import simulator.dto.command.CommandExportItem
import simulator.repository.CommandRepositoryService

class CommandParseSink extends GraphStage[SinkShape[Seq[CommandExportItem]]] {

  val in: Inlet[Seq[CommandExportItem]] = Inlet("CommandParseSink")
  override def shape = SinkShape(in)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {

      // This requests one element at the Sink startup.
      override def preStart(): Unit = pull(in)

      setHandler(
        in,
        new InHandler {
          override def onPush(): Unit = {
            val commands: Seq[CommandExportItem] = grab(in)
            CommandRepositoryService.insertETGCommands(commands)
            pull(in)
          }
        }
      )
    }

}
