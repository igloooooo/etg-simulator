package simulator.command

import api.dto.{PTCallItem, TimeTableResponse}
import play.api.libs.json.Json
import playback.channel.Protocol
import service.dataset.PTOrderedCallDTO

sealed trait ETGCommand extends Protocol {
  def description = ""
}

object NodeBlocked {
  implicit val actionReads = Json.reads[NodeBlocked]
  implicit val actionWrite = Json.writes[NodeBlocked]
}

object NodeRelease {
  implicit val actionReads = Json.reads[NodeRelease]
  implicit val actionWrite = Json.writes[NodeRelease]
}
object NodeReleaseAll {
  implicit val actionReads = Json.reads[NodeReleaseAll]
  implicit val actionWrite = Json.writes[NodeReleaseAll]
}
object DelayTrip {
  implicit val actionReads = Json.reads[DelayTrip]
  implicit val actionWrite = Json.writes[DelayTrip]
}
object TerminateTrip {
  implicit val actionReads = Json.reads[TerminateTrip]
  implicit val actionWrite = Json.writes[TerminateTrip]
}
object ResetTrip {
  implicit val actionReads = Json.reads[ResetTrip]
  implicit val actionWrite = Json.writes[ResetTrip]
}
object ResetTripAll {
  implicit val actionReads = Json.reads[ResetTripAll]
  implicit val actionWrite = Json.writes[ResetTripAll]
}
object SkipStopOnTrip {
  implicit val actionReads = Json.reads[SkipStopOnTrip]
  implicit val actionWrite = Json.writes[SkipStopOnTrip]
}
object DelayTripPlan {
  implicit val actionReads = Json.reads[DelayTripPlan]
  implicit val actionWrite = Json.writes[DelayTripPlan]
}
object UpdateTripPlan {
  implicit val actionReads = Json.reads[UpdateTripPlan]
  implicit val actionWrite = Json.writes[UpdateTripPlan]
}
object MergeTripPlan {
  implicit val actionReads = Json.reads[MergeTripPlan]
  implicit val actionWrite = Json.writes[MergeTripPlan]
}

case class NodeBlocked(group: String, stopRef: String) extends ETGCommand {
  override def description: String = s"Node $stopRef has been blocked."

}
case class NodeRelease(group: String, stopRef: String) extends ETGCommand {
  override def description: String = s"Node $stopRef has been released."
}

case class NodeReleaseAll(group: String) extends ETGCommand {
  override def description: String = s"All blocked nodes has been released."
}
case class DelayTrip(group: String,
                     tripId: String,
                     seconds: Long,
                     startRef: String,
                     stopRef: String)
    extends ETGCommand {
  override def description: String = s"Trip $tripId has been delay $seconds ."
}

case class DelayTripPlan(group: String,
                     tripId: String,
                     seconds: Long,
                     startStopRef: String)
  extends ETGCommand {
  override def description: String = s"Trip Plan $tripId has been delay $seconds at $startStopRef."
}

case class SkipStopOnTrip(group: String,
                          tripId: String,
                          stopRef: String)
  extends ETGCommand {
  override def description: String = s"Stop $stopRef has been skip on trip $tripId ."
}
case class TerminateTrip(group: String, tripId: String) extends ETGCommand {
  override def description: String = s"Trip $tripId has been terminated."
}
case class ResetTrip(group: String, tripId: String) extends ETGCommand {
  override def description: String = s"Trip $tripId has been reset."
}
case class ResetTripAll(group: String) extends ETGCommand {
  override def description: String = s"All Trips have been reset."
}
case class UpdateTripPlan(group: String, tripId: String, stopRef:String, calls:List[PTCallItem]) extends ETGCommand {
  override def description: String = s"Trip $tripId has been update."
}
case class MergeTripPlan(group: String, startTripId: String, startTripStopRef:String, endTripId: String, endTripStopRef:String) extends ETGCommand {
  override def description: String = s"Trip $startTripId and $endTripId will be merged at $startTripId ."
}

/**
  * reset all nodes
  */
case class ResetNode() extends ETGCommand
case class ResetNodeFinish(stopRef: String) extends ETGCommand
case class Reboot() extends ETGCommand
