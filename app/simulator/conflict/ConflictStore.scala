package simulator.conflict

import simulator.dto.TripNodeConflictEvent

import scala.collection.mutable

object ConflictStore {
  var conflictList = mutable.Map[String, Option[TripNodeConflictEvent]]()

  def addOrUpdate(event: TripNodeConflictEvent): Boolean = {
    conflictList.get(event.stopRef) match {
      case Some(item) =>
        if (event.conflictedList isEmpty) {
          // remove the conflict
          conflictList -= event.stopRef
          true
        } else {
          // update conflict
          item match {
            case Some(ce) =>
              if (ce.conflictedList == event.conflictedList) {
                false
              } else {
                conflictList -= event.stopRef
                conflictList += (event.stopRef -> Some(event))
                true
              }
            case None =>
              conflictList += (event.stopRef -> Some(event))
              true
          }
        }
      case None =>
        if (event.conflictedList isEmpty) {
          // no conflict
          false
        } else {
          // new conflict
          conflictList += (event.stopRef -> Some(event))
          true
        }

    }
  }

  def findAllConflicts():List[TripNodeConflictEvent] = {
    conflictList.toList.flatMap(_._2)
  }

  def reset = {
    conflictList = mutable.Map[String, Option[TripNodeConflictEvent]]()
  }
}
