package simulator.dto

import api.dto.network.NetworkLink
import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._



object TripNodeConflictItem{
  implicit val actionReads = Json.reads[TripNodeConflictItem]
  implicit val actionWrite = Json.writes[TripNodeConflictItem]
}

case class TripNodeConflictItem(triggeredTrip: String, impactedTrip: String)

object TripNodeConflictDrawItem{
  implicit val actionReads = Json.reads[TripNodeConflictDrawItem]
  implicit val actionWrite = Json.writes[TripNodeConflictDrawItem]
}

case class TripNodeConflictDrawItem(stopRef:String, locX:Int, locY:Int, conflictedList: Set[TripNodeConflictItem])

object TripNodeConflictEvent{
  implicit val actionReads = Json.reads[TripNodeConflictEvent]
  implicit val actionWrite = Json.writes[TripNodeConflictEvent]
}

case class TripNodeConflictEvent(stopRef: String, conflictedList: Set[TripNodeConflictItem])

object NodeUpdateEvent {
  implicit val actionReads = Json.reads[NodeUpdateEvent]
  implicit val actionWrite = Json.writes[NodeUpdateEvent]
}

case class NodeUpdateEvent(stopRef: String, isBlocked:Boolean)

object BlockNodeDrawItem{
  implicit val actionReads = Json.reads[BlockNodeDrawItem]
  implicit val actionWrite = Json.writes[BlockNodeDrawItem]
}
case class BlockNodeDrawItem(stopRef: String, locX:Int, locY:Int)

object TripMovementEvent {
  implicit val actionReads = Json.reads[TripMovementEvent]
  implicit val actionWrite = Json.writes[TripMovementEvent]
}

case class TripMovementEvent(tripId:String, trip: String, updateTime: DateTime, stopRef: String, eventType: String, delay: Int = 0, sector:String="", node:String="", value:Int=0) {

}

object NetworkUpdateEvent {
  implicit val actionReads = Json.reads[NetworkUpdateEvent]
  implicit val actionWrite = Json.writes[NetworkUpdateEvent]
}

case class NetworkUpdateEvent(trip: String, link:NetworkLink)

object ETGServerEvent {
  implicit val actionReads = Json.reads[ETGServerEvent]
  implicit val actionWrite = Json.writes[ETGServerEvent]
  val ActualArrivalMovement = "ActualArrivalMovement"
  val ActualDepartureMovement = "ActualDepartureMovement"
  val TripCreate = "TripCreate"
  val TripInit = "TripCreate"
  val TripTerminated = "TripTerminated"
  val TripActive = "TripActive"
  val TripHolding = "TripHolding"
  val TripNodeConflict = "TripNodeConflict"
  val NodeUpdate = "NodeUpdate"
  val TripNetworkUpdate = "TripNetworkUpdate"
}

case class ETGServerEvent(eventType: String, tripMovementEvent: TripMovementEvent=null, nodeConflict:TripNodeConflictEvent= null, nodeUpdateEvent: NodeUpdateEvent=null, networkUpdateEvent: NetworkUpdateEvent=null)
