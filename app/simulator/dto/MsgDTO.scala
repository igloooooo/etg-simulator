package simulator.dto

trait MsgDTO {}

case class VMDto(responseTime: String) extends MsgDTO
case class PTDto(responseTime: String) extends MsgDTO

case class SriMsg(payload: String, queue: String)
