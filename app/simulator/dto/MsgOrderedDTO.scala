package simulator.dto

import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import service.dataset.PTOrderedCallDTO
import simulator.dto.SriMsg
import simulator.dto.vm.VMResponse

import scala.concurrent.duration.Duration
object MsgOrderedDTO {}

case class MsgOrderedDTO(val msgType: VMResponse.Value,
                         val time: DateTime,
                         val trip: String,
                         val stopRef: String,
                         val delay: Option[Duration] = None)
    extends Ordered[MsgOrderedDTO] {
  override def compare(that: MsgOrderedDTO) = {
    if (time.isBefore(that.time))
      -1
    else if (time.isAfter(that.time))
      1
    else {
      // if it is the same time, check the vm type, put arrival at front
      if (msgType == that.msgType) {
        0
      } else if(msgType == VMResponse.Arrival) {
        -1
      } else {
        1
      }
    }
  }

  def isReady() = {
    val targetTime = delay match {
      case Some(d) =>
        time.minusSeconds(-d.toSeconds.toInt)
      case None => time
    }
    targetTime.isBefore(DateTime.now())
  }

  def getTriggeredTime = {
    delay match {
      case Some(d) =>
        time.minusSeconds(-d.toSeconds.toInt)
      case None => time
    }
  }

}
