package simulator.dto.command

import play.api.libs.json.Json

object CommandExportResponse {
  implicit val actionReads = Json.reads[CommandExportResponse]
  implicit val actionWrite = Json.writes[CommandExportResponse]
}

case class CommandExportResponse(json:String)

object CommandExportItem {
  implicit val actionReads = Json.reads[CommandExportItem]
  implicit val actionWrite = Json.writes[CommandExportItem]
}

case class CommandExportItem(commandType: String,
                             jsonCommand: String,
                             executeTime: Long)
