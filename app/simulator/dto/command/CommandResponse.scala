package simulator.dto.command

import play.api.libs.json.Json

object CommandItem {
  implicit val actionReads = Json.reads[CommandItem]
  implicit val actionWrite = Json.writes[CommandItem]
}

case class CommandItem(id:Long, commandType:String, time:String, description:String)

object CommandResponse {
  implicit val actionReads = Json.reads[CommandResponse]
  implicit val actionWrite = Json.writes[CommandResponse]
}

case class CommandResponse(commands:Seq[CommandItem])
