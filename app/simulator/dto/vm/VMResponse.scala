package simulator.dto.vm

import org.joda.time.format.{DateTimeFormatter, ISODateTimeFormat}
import service.dataset.PTOrderedCallDTO

trait VMResponse {
  def toXML: String = ""

}

object VMResponse extends Enumeration {

  implicit def toXML(vm: VMResponse): String = vm.toXML

  val Arrival, Departure = Value
  def apply(dateTripCallDTO: PTOrderedCallDTO,
            responseType: VMResponse.Value): VMResponse = {

    responseType match {
      case Arrival =>
        val aimDateTime = dateTripCallDTO.time
        val aimedArrivalTimeStr = aimDateTime toString ISODateTimeFormat.dateHourMinuteSecond
        val responseTime = aimDateTime minusSeconds 3 toString ISODateTimeFormat.dateHourMinuteSecond
        val startDate = aimDateTime toString ISODateTimeFormat.date
        VMArrivalResponse(
          dateTripCallDTO.trip,
          dateTripCallDTO.trip,
          "21685",
          dateTripCallDTO.stopRef,
          aimedArrivalTimeStr,
          responseTime,
          startDate
        )
      case Departure =>
        val aimDateTime = dateTripCallDTO.time
        val aimedArrivalTimeStr = aimDateTime toString ISODateTimeFormat.dateHourMinuteSecond
        val responseTime = aimDateTime minusSeconds 3 toString ISODateTimeFormat.dateHourMinuteSecond
        val startDate = aimDateTime toString ISODateTimeFormat.date
        VMDepartureResponse(
          dateTripCallDTO.trip,
          dateTripCallDTO.trip,
          "21685",
          dateTripCallDTO.stopRef,
          aimedArrivalTimeStr,
          responseTime,
          startDate
        )
    }
  }

}

final case class VMDepartureResponse(datedVehicleJourneyRef: String,
                                     courseOfJourneyRef: String,
                                     vehicleRef: String,
                                     stopPointRef: String,
                                     aimedDepartureTime: String,
                                     responseTime: String,
                                     tripStartDate: String)
    extends VMResponse {
  def uuid = java.util.UUID.randomUUID.toString
  override def toXML: String =
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<ns0:Siri xmlns:ns0=\"http://www.siri.org.uk/siri\" version=\"2.0\">\n" +
      "    <ns0:ServiceDelivery>\n" +
      s"        <ns0:ResponseTimestamp>$responseTime</ns0:ResponseTimestamp>\n" +
      "        <ns0:ResponseMessageIdentifier>79244bd9-9f48-45b8-857f-69665377048e</ns0:ResponseMessageIdentifier>\n" +
      "        <ns0:VehicleMonitoringDelivery version=\"1.0\">\n" +
      s"            <ns0:ResponseTimestamp>$responseTime</ns0:ResponseTimestamp>\n" +
      "            <ns0:VehicleActivity>\n" +
      s"                <ns0:RecordedAtTime>$responseTime</ns0:RecordedAtTime>\n" +
      s"                <ns0:ItemIdentifier>$uuid</ns0:ItemIdentifier>\n" +
      s"                <ns0:ValidUntilTime>$responseTime</ns0:ValidUntilTime>\n" +
      "                <ns0:MonitoredVehicleJourney>\n" +
      "                    <ns0:FramedVehicleJourneyRef xmlns:ns0=\"http://www.siri.org.uk/siri\">\n" +
      "                        <ns0:DataFrameRef>----</ns0:DataFrameRef>\n" +
      s"                        <ns0:DatedVehicleJourneyRef>$datedVehicleJourneyRef</ns0:DatedVehicleJourneyRef>\n" +
      "                    </ns0:FramedVehicleJourneyRef>\n" +
      s"                   <ns0:CourseOfJourneyRef>$datedVehicleJourneyRef</ns0:CourseOfJourneyRef>" +
      "                    <ns0:VehicleRef>" + vehicleRef + "</ns0:VehicleRef>\n" +
      "                    <ns0:MonitoredCall>\n" +
      "                        <ns0:StopPointRef>" + stopPointRef + "</ns0:StopPointRef>\n" +
      s"                        <ns0:ActualDepartureTime>$aimedDepartureTime</ns0:ActualDepartureTime>\n" +
      "                    </ns0:MonitoredCall>\n" +
      "                </ns0:MonitoredVehicleJourney>\n" +
      "                <ns0:Extensions>\n" +
      "                    <ns1:VehicleActivityExtension xmlns:ns1=\"http://www.quintiq.com/ServicePlanner/TrainLocationAdvice\">\n" +
      s"                        <TripStartDate>$tripStartDate</TripStartDate>\n" +
      "                        <StartOfJourneyRef>FSB2</StartOfJourneyRef>\n" +
      "                        <TriggeringLocation>ST403M Loc</TriggeringLocation>\n" +
      "                        <Facility>\n" +
      "                            <FacilityCode>Lidcombe</FacilityCode>\n" +
      "                        </Facility>\n" +
      "                    </ns1:VehicleActivityExtension>\n" +
      "                </ns0:Extensions>\n" +
      "            </ns0:VehicleActivity>\n" +
      "            <ns0:Extensions>\n" +
      "                <ns1:VehicleMonitoringDeliveryExtension\n" +
      "                        xmlns:ns1=\"http://www.quintiq.com/ServicePlanner/TrainLocationAdvice\">\n" +
      "                    <Type>Update</Type>\n" +
      "                    <Requestors/>\n" +
      "                </ns1:VehicleMonitoringDeliveryExtension>\n" +
      "            </ns0:Extensions>\n" +
      "        </ns0:VehicleMonitoringDelivery>\n" +
      "    </ns0:ServiceDelivery>\n" +
      "</ns0:Siri>"
}

final case class VMArrivalResponse(datedVehicleJourneyRef: String,
                                   CourseOfJourneyRef: String,
                                   vehicleRef: String,
                                   stopPointRef: String,
                                   aimedArrivalTime: String,
                                   responseTime: String,
                                   tripStartDate: String)
    extends VMResponse {
  def uuid = java.util.UUID.randomUUID.toString
  override def toXML: String =
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<ns0:Siri xmlns:ns0=\"http://www.siri.org.uk/siri\" version=\"2.0\">\n" +
      "    <ns0:ServiceDelivery>\n" +
      s"        <ns0:ResponseTimestamp>$responseTime</ns0:ResponseTimestamp>\n" +
      "        <ns0:ResponseMessageIdentifier>79244bd9-9f48-45b8-857f-69665377048e</ns0:ResponseMessageIdentifier>\n" +
      "        <ns0:VehicleMonitoringDelivery version=\"1.0\">\n" +
      s"            <ns0:ResponseTimestamp>$responseTime</ns0:ResponseTimestamp>\n" +
      "            <ns0:VehicleActivity>\n" +
      s"                <ns0:RecordedAtTime>$responseTime</ns0:RecordedAtTime>\n" +
      s"                <ns0:ItemIdentifier>$uuid</ns0:ItemIdentifier>\n" +
      s"                <ns0:ValidUntilTime>$responseTime</ns0:ValidUntilTime>\n" +
      "                <ns0:MonitoredVehicleJourney>\n" +
      "                    <ns0:FramedVehicleJourneyRef>\n" +
      "                        <ns0:DataFrameRef>----</ns0:DataFrameRef>\n" +
      s"                        <ns0:DatedVehicleJourneyRef>$datedVehicleJourneyRef</ns0:DatedVehicleJourneyRef>\n" +
      "                    </ns0:FramedVehicleJourneyRef>\n" +
      s"                   <ns0:CourseOfJourneyRef>$datedVehicleJourneyRef</ns0:CourseOfJourneyRef>" +
      "                    <ns0:VehicleRef>" + vehicleRef + "</ns0:VehicleRef>\n" +
      "                    <ns0:MonitoredCall>\n" +
      "                        <ns0:StopPointRef>" + stopPointRef + "</ns0:StopPointRef>\n" +
      s"                        <ns0:ExpectedArrivalTime>$aimedArrivalTime</ns0:ExpectedArrivalTime>\n" +
      "                    </ns0:MonitoredCall>\n" +
      "                </ns0:MonitoredVehicleJourney>\n" +
      "                <ns0:Extensions>\n" +
      "                    <ns1:VehicleActivityExtension xmlns:ns1=\"http://www.quintiq.com/ServicePlanner/TrainLocationAdvice\">\n" +
      s"                        <TripStartDate>$tripStartDate</TripStartDate>\n" +
      "                        <StartOfJourneyRef>FSB2</StartOfJourneyRef>\n" +
      "                        <TriggeringLocation>ST403M Loc</TriggeringLocation>\n" +
      "                        <Facility>\n" +
      "                            <FacilityCode>Lidcombe</FacilityCode>\n" +
      "                        </Facility>\n" +
      "                    </ns1:VehicleActivityExtension>\n" +
      "                </ns0:Extensions>\n" +
      "            </ns0:VehicleActivity>\n" +
      "            <ns0:Extensions>\n" +
      "                <ns1:VehicleMonitoringDeliveryExtension\n" +
      "                        xmlns:ns1=\"http://www.quintiq.com/ServicePlanner/TrainLocationAdvice\">\n" +
      "                    <Type>Update</Type>\n" +
      "                    <Requestors/>\n" +
      "                </ns1:VehicleMonitoringDeliveryExtension>\n" +
      "            </ns0:Extensions>\n" +
      "        </ns0:VehicleMonitoringDelivery>\n" +
      "    </ns0:ServiceDelivery>\n" +
      "</ns0:Siri>"
}
