package simulator.repository

import java.sql.Timestamp
import java.time.LocalDateTime

import simulator.dto.command.CommandExportItem
import slick.jdbc.H2Profile.api._
import DBConfig.db

object CommandRepositoryService {
  def insertETGCommands(commands: Seq[CommandExportItem]) = {
    val qInsert = ETGCommandTable ++= commands.map(item => {
      val timestamp: Timestamp = Timestamp.valueOf(LocalDateTime.now())
      timestamp.setTime(item.executeTime)

      ETGCommandModel(0, item.commandType, item.jsonCommand, timestamp)
    })

    db.run(qInsert)
  }
}
