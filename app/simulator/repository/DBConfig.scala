package simulator.repository

import slick.driver.H2Driver.api.Database

object DBConfig {
  implicit val db = Database.forConfig("h2mem")
}
