package simulator.repository

import java.sql.Timestamp

import play.api.libs.json.Json
import slick.jdbc.H2Profile.api._
import simulator.command._

case class ETGCommandModel(id: Long,
                           commandType: String,
                           jsonCommand: String,
                           executeTime: Timestamp) {
  def description = {
    convertToCommand.map(_.description).getOrElse("")
  }
  def convertToCommand: Option[ETGCommand] = {
    ETGCommandType.withNameOpt(commandType) match {
      case Some(etgCommandType) =>
        etgCommandType match {
          case ETGCommandType.DelayTrip =>
            Some(Json.fromJson[DelayTrip](Json.parse(jsonCommand)).get)
          case ETGCommandType.SkipStopOnTrip =>
            Some(Json.fromJson[SkipStopOnTrip](Json.parse(jsonCommand)).get)
          case ETGCommandType.UpdateTripPlan =>
            Some(Json.fromJson[UpdateTripPlan](Json.parse(jsonCommand)).get)
          case ETGCommandType.MergeTripPlan =>
            Some(Json.fromJson[MergeTripPlan](Json.parse(jsonCommand)).get)
          case ETGCommandType.TerminateTrip =>
            Some(Json.fromJson[TerminateTrip](Json.parse(jsonCommand)).get)
          case ETGCommandType.BlockNode =>
            Some(Json.fromJson[NodeBlocked](Json.parse(jsonCommand)).get)
          case ETGCommandType.ReleaseAllNode =>
            Some(Json.fromJson[NodeReleaseAll](Json.parse(jsonCommand)).get)
          case ETGCommandType.ReleaseNode =>
            Some(Json.fromJson[NodeRelease](Json.parse(jsonCommand)).get)
          case _ =>
            None
        }

      case None => None
    }
  }
}
object ETGCommandType extends Enumeration {
  val BlockNode = Value("BlockNode")
  val ReleaseNode = Value("ReleaseNode")
  val ReleaseAllNode = Value("ReleaseAllNode")
  val DelayTrip = Value("DelayTrip")
  val SkipStopOnTrip = Value("SkipStopOnTrip")
  val ResetTrip = Value("ResetTrip")
  val UpdateTripPlan = Value("UpdateTripPlan")
  val MergeTripPlan = Value("MergeTripPlan")
  val DelayTripPlan = Value("DelayTripPlan")
  val Reboot = Value("Reboot")
  val TerminateTrip = Value("TerminateTrip")

  def withNameOpt(s: String): Option[Value] = values.find(_.toString == s)
}
object ETGCommandTable extends TableQuery(new ETGCommandTable(_)) {}
class ETGCommandTable(tag: Tag)
    extends Table[ETGCommandModel](tag, "ETGCommand") {
  def id = column[Long]("ID", O.AutoInc, O.PrimaryKey)
  def commandType = column[String]("COMMANDTYPE")
  def jsonCommand = column[String]("JSONCOMMAND")
  def executeTime = column[Timestamp]("EXECUTETIME")
  def * =
    (id, commandType, jsonCommand, executeTime) <> (ETGCommandModel.tupled, ETGCommandModel.unapply)
}
