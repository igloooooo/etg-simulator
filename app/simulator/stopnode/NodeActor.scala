package simulator.stopnode

import akka.actor.{Actor, ActorLogging, Props}
import org.joda.time.DateTime
import playback.channel.Protocol
import simulator.command.{NodeBlocked, NodeRelease, ResetNode, ResetNodeFinish}
import simulator.dto.NodeUpdateEvent
import simulator.stopnode.NodeActor._

import scala.collection.SortedSet

object NodeActor {
  def props(group: String, nodeId: String): Props =
    Props(new NodeActor(group, nodeId))

  case class RegisterTimeTable(group: String,
                               nodeId: String,
                               trip: String,
                               arrivalTime: DateTime,
                               departureTime: DateTime)
      extends Protocol

  case class UpdateTimeTable(group: String,
                             nodeId: String,
                             trip: String,
                             arrivalTime: DateTime,
                             departureTime: DateTime)
      extends Protocol

  case class RemoveTripPlanOnNode(group: String,
                                  nodeId: String,
                                  trip: String)
      extends Protocol

  case class ConflictAlert(stopRef: String,
                           conflictedList: List[NodeConflictedItem])

  case class CheckNodeAvailableRequest(group: String, stopRef: String)
      extends Protocol
  case class CheckNodeAvailableResponse(group: String,
                                        stopRef: String,
                                        isBlocked: Boolean)
      extends Protocol
  case class TrainArrival(trip: String,
                          group: String,
                          stopRef: String,
                          time: DateTime)
  case class TrainDeparture(trip: String,
                            group: String,
                            stopRef: String,
                            time: DateTime)

}

class NodeActor(group: String, nodeId: String)
    extends Actor
    with ActorLogging
    with NodeControllerSender {
  var callOrdered: SortedSet[NodeTimeTableCallDTO] =
    scala.collection.SortedSet()
  var isBlocked: Boolean = false

  override def receive = {
    case reg @ RegisterTimeTable(`group`, `nodeId`, _, _, _) =>
      callOrdered = callOrdered + new NodeTimeTableCallDTO(reg.arrivalTime,
                                                           reg.departureTime,
                                                           reg.trip)
    case updatedCall @ UpdateTimeTable(`group`, `nodeId`, _, _, _) =>
      updateTimetableByTrip(
        new NodeTimeTableCallDTO(updatedCall.arrivalTime,
                                 updatedCall.departureTime,
                                 updatedCall.trip))
      nodeControllerSender(new ConflictAlert(nodeId, checkConflict()))

    case _ @ RemoveTripPlanOnNode(`group`, `nodeId`, trip) =>
      removeTimetableCallByTrip(trip)
      nodeControllerSender(new ConflictAlert(nodeId, checkConflict()))

    case departureEvent @ TrainDeparture(_, `group`, `nodeId`, _) =>
      // release currently node
      if (isBlocked) {
        isBlocked = false
        nodeControllerSender(new NodeUpdateEvent(nodeId, isBlocked))
      }

    case _ @CheckNodeAvailableRequest(`group`, `nodeId`) =>
      sender() ! new CheckNodeAvailableResponse(group, nodeId, isBlocked)
      log.debug("recieve checking for {}, result is {}", nodeId, isBlocked)

    case _ @NodeBlocked(`group`, `nodeId`) =>
      if (!isBlocked) {
        isBlocked = true
        log.info("status of node {} changed to {}", nodeId, isBlocked)
        nodeControllerSender(new NodeUpdateEvent(nodeId, isBlocked))
      }

    case _ @NodeRelease(`group`, `nodeId`) =>
      if (isBlocked) {
        isBlocked = false
        log.info("status of node {} changed to {}", nodeId, isBlocked)
        nodeControllerSender(new NodeUpdateEvent(nodeId, isBlocked))
      }

    case _ @ResetNode() =>
      isBlocked = false
      callOrdered = scala.collection.SortedSet()
      sender() ! ResetNodeFinish(nodeId)
  }

  def checkConflict() = {
    check(callOrdered.toList)
      .collect {
        case Some(pair) =>
          pair
      }
      .map(pair => {
        new NodeConflictedItem(pair._1.trip,
                               pair._2.trip,
                               nodeId,
                               pair._1.arrivalTime)
      })

  }

  def check(seq: List[NodeTimeTableCallDTO])
    : List[Option[(NodeTimeTableCallDTO, NodeTimeTableCallDTO)]] = {
    seq match {
      case Nil         => List()
      case head :: Nil => List()
      case head :: others =>
        others.map(f => {
          if (head.departureTime.isAfter(f.arrivalTime))
            Some((head, f))
          else
            None
        }) ++ check(others)
    }
  }
  def updateTimetableByTrip(updated: NodeTimeTableCallDTO): Unit = {
    callOrdered.find(_.trip == updated.trip) match {
      case Some(origin) =>
        callOrdered -= origin
        callOrdered = callOrdered + updated
      case None =>
        log.warning(
          "Update an trip which does NOT exist in the node: " + updated)
    }
  }

  def removeTimetableCallByTrip(trip:String):Unit = {
    callOrdered.find(_.trip == trip) match {
      case Some(origin) =>
        callOrdered -= origin
      case None =>
//        log.warning(s"Update an trip which does NOT exist in the node: $trip, $nodeId")
    }
  }
}
