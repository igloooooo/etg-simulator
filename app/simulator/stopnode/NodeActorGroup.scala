package simulator.stopnode

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import simulator.command._
import simulator.stopnode.NodeActor.{RegisterTimeTable, RemoveTripPlanOnNode, UpdateTimeTable}

object NodeActorGroup {
  def props(groupId: String): Props = Props(new NodeActorGroup(groupId))
}

class NodeActorGroup(groupId: String) extends Actor with ActorLogging {
  var nodeToActor = Map.empty[String, ActorRef]
  var actorToNode = Map.empty[ActorRef, String]
  var nodeResetMap = Seq[String]()

  override def receive = {
    case register @ RegisterTimeTable(`groupId`, _, _, _, _) =>
      nodeToActor.get(register.nodeId) match {
        case Some(actorRef) =>
          actorRef forward register
        case None =>
          log.debug("create node for " + register.nodeId)
          val nodeActor = context.actorOf(
            NodeActor.props(groupId, register.nodeId),
            s"node-${register.nodeId}")
          context.watch(nodeActor)
          actorToNode += nodeActor -> register.nodeId
          nodeToActor += register.nodeId -> nodeActor
          nodeActor forward register
      }

    case updateTimeTable @ UpdateTimeTable(`groupId`, nodeId, _, _, _) =>
      nodeToActor.get(nodeId) match {
        case Some(actorRef) =>
          actorRef forward updateTimeTable
        case None =>
          log.warning(
            "Try to update node which does not exist: " + updateTimeTable)
      }

    case event @ RemoveTripPlanOnNode(`groupId`, nodeId, _) =>
      nodeToActor.get(nodeId) match {
        case Some(actorRef) =>
          actorRef forward event
        case None =>
          log.warning(
            "Try to update node which does not exist: " + event)
      }

    case event @ NodeBlocked(`groupId`, nodeId) =>
      nodeToActor.get(nodeId) match {
        case Some(actorRef) =>
          actorRef forward event
        case None =>
          log.warning("Try to block node which does not exist: " + event)
      }

    case event @ NodeRelease(`groupId`, nodeId) =>
      nodeToActor.get(nodeId) match {
        case Some(actorRef) =>
          actorRef forward event
        case None =>
          log.warning("Try to release node which does not exist: " + event)
      }

    case event @ NodeReleaseAll(`groupId`) =>
      nodeToActor.foreach(_._2 forward event)

    case event @ ResetNode() =>
      nodeResetMap = Seq[String]()
      nodeToActor.foreach(_._2 forward event)

    case _ @ResetNodeFinish(stopRef) =>
      nodeResetMap = nodeResetMap :+ stopRef
      if (nodeResetMap.size == nodeToActor.size) {
        context.parent ! ResetNodeFinish(groupId)
      }
  }
}
