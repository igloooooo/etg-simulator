package simulator.stopnode

import org.joda.time.DateTime

object NodeConflictedItem {}

case class NodeConflictedItem(triggerTrip: String,
                              impactedTrip: String,
                              stopRef: String,
                              startTime: DateTime)
