package simulator.stopnode

import javax.inject.{Inject, Named}

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import org.joda.time.format.ISODateTimeFormat
import simulator.command._
import simulator.conflict.ConflictStore
import simulator.dto.{ETGServerEvent, NodeUpdateEvent, TripNodeConflictEvent, TripNodeConflictItem}
import simulator.stopnode.NodeActor.{ConflictAlert, RegisterTimeTable, RemoveTripPlanOnNode, UpdateTimeTable}
import simulator.trip.TripMonitorSender

import scala.concurrent.ExecutionContext

object NodeController {

}

class NodeController @Inject()(system: ActorSystem, @Named("trip-monitor")tripMonitor: ActorRef)(implicit ec: ExecutionContext)
    extends Actor
    with ActorLogging
    with CommandControllerSender {
  var groupIdToActor = Map.empty[String, ActorRef]
  var actorToGroupId = Map.empty[ActorRef, String]
  var resetNodeGroup = Seq[String]()

  override def receive = {
    case register @ RegisterTimeTable(groupId, _, _, _, _) =>
      groupIdToActor.get(groupId) match {
        case Some(actorRef) =>
          actorRef forward register
        case None =>
          log.debug("Creating device group actor for {}", groupId)
          val groupActor =
            context.actorOf(NodeActorGroup.props(groupId), "group-" + groupId)
          context.watch(groupActor)
          groupActor forward register
          groupIdToActor += groupId -> groupActor
          actorToGroupId += groupActor -> groupId
      }
    case updateTimeTable @ UpdateTimeTable(groupId, _, _, _, _) =>
      groupIdToActor.get(groupId) match {
        case Some(actorRef) =>
          actorRef forward updateTimeTable
        case None =>
          log.warning(
            "Try to update node which does not exist: " + updateTimeTable)
      }
    case event @ RemoveTripPlanOnNode(groupId, _, _) =>
      groupIdToActor.get(groupId) match {
        case Some(actorRef) =>
          actorRef forward event
        case None =>
          log.warning(
            "Try to update node which does not exist: " + event)
      }


    case event @ NodeBlocked(groupId, _) =>
      groupIdToActor.get(groupId) match {
        case Some(actorRef) =>
          actorRef forward event
        case None =>
          log.warning("Try to block node which does not exist: " + event)
      }

    case event @ NodeRelease(groupId, _) =>
      groupIdToActor.get(groupId) match {
        case Some(actorRef) =>
          actorRef forward event
        case None =>
          log.warning("Try to release node which does not exist: " + event)
      }

    case event @ NodeReleaseAll(groupId) =>
      groupIdToActor.get(groupId) match {
        case Some(actorRef) =>
          actorRef forward event
        case None =>
          log.warning("Try to release node which does not exist: " + event)
      }

    case event @ ResetNode() =>
      resetNodeGroup = Seq()
      groupIdToActor.foreach(_._2 forward event)

    case event @ NodeUpdateEvent(_, _) =>
      tripMonitor ! new ETGServerEvent(ETGServerEvent.NodeUpdate, nodeUpdateEvent = event)

    case conflict @ ConflictAlert(_, _) =>
      handleNodeConflict(conflict)

    case _ @ResetNodeFinish(groupId) =>
      resetNodeGroup = resetNodeGroup :+ groupId
      if (resetNodeGroup.size == groupIdToActor.size) {
        commandControllerSender(ResetNodeFinish(""))
      }
  }

  private def handleNodeConflict(conflict: ConflictAlert) = {
    if (conflict.conflictedList isEmpty) {
      // it could be the case which conflicts has been removed
      val event =
        new ETGServerEvent(
          ETGServerEvent.TripNodeConflict,
          nodeConflict = TripNodeConflictEvent(
            conflict.stopRef,
            Set[TripNodeConflictItem]()
          )
        )
      if (ConflictStore addOrUpdate event.nodeConflict) {
        tripMonitor ! event
        log.error("there is conflict for " + conflict)
      }

    } else {
      val event =
        new ETGServerEvent(
          ETGServerEvent.TripNodeConflict,
          nodeConflict = TripNodeConflictEvent(
            conflict.stopRef,
            conflict.conflictedList
              .map(item =>
                new TripNodeConflictItem(item.triggerTrip, item.impactedTrip))
              .toSet
          )
        )
      if (ConflictStore addOrUpdate event.nodeConflict) {
        tripMonitor ! event
        log.error("there is conflict for " + conflict)
      }
    }
  }
}
