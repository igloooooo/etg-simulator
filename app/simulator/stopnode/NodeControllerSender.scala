package simulator.stopnode

import akka.actor.{ActorContext, ActorRef}

trait NodeControllerSender {
  def nodeControllerSender(msg: Any)(implicit context: ActorContext,
                                     sender: ActorRef) = {
    context
      .actorSelection("akka://application/user/node-controller") ! msg
  }

  def nodeSender(stopRef: String, msg: Any)(implicit context: ActorContext,
                                            sender: ActorRef) = {
    val url =
      s"akka://application/user/node-controller/group-etg/node-${stopRef}"
    context
      .actorSelection(url) ! msg
  }
}
