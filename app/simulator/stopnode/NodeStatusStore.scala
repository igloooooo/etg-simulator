package simulator.stopnode

import org.slf4j.LoggerFactory
import simulator.dto.NodeUpdateEvent

object NodeStatusStore {
  private[this] val logger = LoggerFactory.getLogger(NodeStatusStore.getClass)
  private val nodeStatusSet
  : scala.collection.mutable.Set[NodeUpdateEvent] =
    scala.collection.mutable.Set[NodeUpdateEvent]()

  def addNodeStatus(update:NodeUpdateEvent): Unit = {
    nodeStatusSet.update(update, true)
  }

  def loadAllNodes = nodeStatusSet.filter(_.isBlocked)
}
