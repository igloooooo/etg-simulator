package simulator.stopnode

import org.joda.time.DateTime

class NodeTimeTableCallDTO(val arrivalTime: DateTime,
                           val departureTime: DateTime,
                           val trip: String)
    extends Ordered[NodeTimeTableCallDTO] {

  override def compare(that: NodeTimeTableCallDTO) = {
    if (arrivalTime.isBefore(that.arrivalTime))
      -1
    else if (arrivalTime.isAfter(that.arrivalTime))
      1
    else
      0
  }

}
