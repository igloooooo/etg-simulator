package simulator.trip

import org.slf4j.LoggerFactory
import simulator.dto.MsgOrderedDTO

import scala.collection.SortedSet

/**
  * this class saving all changed trips
  */
object ChangedTripStore {

  private[this] val logger = LoggerFactory.getLogger(ChangedTripStore.getClass)
  var changedTrips
    : scala.collection.mutable.Map[String, SortedSet[MsgOrderedDTO]] =
    scala.collection.mutable.HashMap[String, SortedSet[MsgOrderedDTO]]()

  def addChangedTrip(trip: String, calls: SortedSet[MsgOrderedDTO]) = {
    changedTrips += (trip -> calls)
  }

  def findByTrip(trip: String): Option[Seq[MsgOrderedDTO]] = {
    changedTrips.find(_._1 == trip).map(_._2.toSeq)
  }

  def isTripChanged(tripId:String):Boolean = {
    changedTrips.contains(tripId)
  }

  def resetAll = {
    changedTrips = scala.collection.mutable.HashMap[String, SortedSet[MsgOrderedDTO]]()
  }

  /**
    * this will filter movement by sector and trip
    * @param trip
    * @param sector
    * @return
    */
//  def findByTripSector(trip: String,
//                       sector: String): Option[Seq[ActualMovement]] = {
//    changedTrips
//      .find(_._1 == trip)
//      .map(
//        _._2
//          .flatMap(TripMonitorActor.convertToDisplayData(_))
//          .filter(_.sector == sector)
//          .map(ActualMovement(_))
//          .toSeq
//          .sortBy(_.time))
//  }
}
