package simulator.trip

import akka.actor.{Actor, ActorLogging, FSM, Props}
import org.joda.time.DateTime
import play.api.Logger
import playback.channel.{PollMsg, Protocol}
import service.dataset.PTOrderedCallDTO
import simulator.command._
import simulator.dto.{ETGServerEvent, MsgOrderedDTO, TripMovementEvent}
import simulator.dto.vm.VMResponse
import simulator.stopnode.NodeActor._
import simulator.stopnode.NodeControllerSender
import simulator.trip.TripController.{RemoveTripPlanCall, RequestTrackTrip}

import scala.collection.SortedSet
import scala.concurrent.duration._

object TripActor extends Enumeration {
  def props(group: String, tripId:String, trip: String, tripDate:DateTime): Props =
    Props(new TripActor(group, tripId, trip, tripDate))

  case class changePlatform(group: String,
                            trip: String,
                            originalStopRef: String,
                            targetStopRef: String)
      extends Protocol
  case class TripMovement(msg: MsgOrderedDTO) extends Protocol

}

class TripActor(group: String, tripId:String, trip: String, tripDate:DateTime)
    extends FSM[TripActorState, TripActorStateData]
    with TripMonitorSender
    with NodeControllerSender
    with TripControllerSender {
  import TripActor._

  startWith(Init, new TripActorStateData(scala.collection.SortedSet(), List()))

  when(Init) {
    case Event(requestTrackTrip @ RequestTrackTrip(`group`, `tripId`, _, _, _, _),
               actorStateData) => {
      val updatedData =
        handleTimeTableRegister(requestTrackTrip.arrivalDto, requestTrackTrip.departureDto, actorStateData)
      goto(Creating) using updatedData
    }
  }

  when(Creating) {
    case Event(requestTrackTrip @ RequestTrackTrip(`group`, `tripId`, _, _, _, _),
               actorStateData) => {
      val updatedData =
        handleTimeTableRegister(requestTrackTrip.arrivalDto, requestTrackTrip.departureDto, actorStateData)
      stay() using updatedData
    }

    case Event(PollMsg(startTimeDiff, endTimeDiff, targetTime, baseLineTime), actorStateData) => {
      handlePollMsg(startTimeDiff, endTimeDiff, targetTime, baseLineTime, actorStateData)
    }
  }

  when(Active) {
    case Event(requestTrackTrip @ RequestTrackTrip(`group`, `tripId`, _, _, _, _),
               actorStateData) => {
      val updatedData =
        handleTimeTableRegister(requestTrackTrip.arrivalDto, requestTrackTrip.departureDto, actorStateData)
      stay() using updatedData
    }
    case Event(delay @ DelayTrip(`group`, `tripId`, _, _, _), actorStateData) => {
      val duration = Duration(delay.seconds, SECONDS)
      val adjustedMsg =
        delayTrip(actorStateData.msgOrdered, duration, "", "")
      val changedDelay = actorStateData.currentTripDelay + duration
      stay() using actorStateData.copy(msgOrdered = adjustedMsg,
                                       currentTripDelay = changedDelay)
    }

    case Event(_ @ResetTrip(`group`, `tripId`), stateData) => {
      reset(stateData)
      stay()
    }

    case Event(PollMsg(startTimeDiff, endTimeDiff, targetTime, baseLineTime), actorStateData) => {
      handlePollMsg(startTimeDiff, endTimeDiff, targetTime, baseLineTime, actorStateData)
    }
    case Event(SkipStopOnTrip(groupId, _, stopRef), actorStateData) =>
      stay() using actorStateData.copy(msgOrdered = skipStopForStop(stopRef, actorStateData.msgOrdered))
  }

  when(Holding) {
    case Event(PollMsg(startTimeDiff, endTimeDiff, targetTime, baseLineTime), actorStateData) => {
      handleHoldingPollMsg(startTimeDiff, endTimeDiff, targetTime, baseLineTime, actorStateData)
    }
    case Event(delay @ DelayTrip(`group`, `tripId`, _, _, _), actorStateData) => {
      val duration = Duration(delay.seconds, SECONDS)
      val adjustedMsg =
        delayTrip(actorStateData.msgOrdered, duration, "", "")
      val changedDelay = actorStateData.currentTripDelay + duration
      stay() using actorStateData.copy(msgOrdered = adjustedMsg,
        currentTripDelay = changedDelay)
    }

    case Event(SkipStopOnTrip(groupId, _, stopRef), actorStateData) =>
      if(actorStateData.msgOrdered.head.stopRef == stopRef) {
        log.warning(s"Can NOT execute skip stop on blocked Node: $stopRef")
        stay()
      } else {
        stay() using actorStateData.copy(msgOrdered = skipStopForStop(stopRef, actorStateData.msgOrdered))
      }

  }

  /**
    * when trip is in this state, it is waiting for the response of checking next node request
    */
  when(Pending) {
    case Event(_ @CheckNodeAvailableResponse(_, stopRef, isBlocked),
               actorStateData) => {
      if (isBlocked) {
        // hold message
        val adjustedData = holdingTrip(actorStateData)
        goto(Holding) using actorStateData.copy(msgOrdered =
                                                  adjustedData.msgOrdered,
                                                currentTripDelay =
                                                  adjustedData.currentTripDelay)
      } else {
        // find the next message, it will be departure message and send it out
        val targetMsg = actorStateData.msgOrdered.head
        sendMsg(targetMsg)
        // remove it from the queue
        goto(Active) using actorStateData.copy(
          msgOrdered = actorStateData.msgOrdered.tail)
      }
    }
  }

  when(Terminate) {
    case Event(PollMsg(_,_,_,_), actorStateData) => {
      stopActor()
      stop()
    }
  }

  whenUnhandled {
    case Event(_ @TerminateTrip(`group`, `tripId`), _) => {
      goto(Terminate)
    }
    case Event(_ @RemoveTripPlanCall(`group`, `tripId`, _, stopRef), actorStateData) => {
      val updatedMsg = handleRemoveTripPlanCall(stopRef, actorStateData.msgOrdered)
      stay() using actorStateData.copy(msgOrdered = updatedMsg)
    }
    case Event(_,_) =>
      Logger.warn("can't handle message: " + trip + "["+stateName+"]")
      stay()
  }

  initialize()

  /////////////////////////////////////////////////////////////
  //  functions for actor
  /////////////////////////////////////////////////////////////

  private[this] def stopActor(): Unit = {
    context.stop(self)
  }

  private[this] def isEmpty(msgOrdered: SortedSet[MsgOrderedDTO]): Boolean = {
    msgOrdered.isEmpty
  }

  /**
    * after this poll, only the holding departure msg will be popup because every step has more than 10 seconds gap
    * @param actorStateData
    * @return
    */
  private[this] def handleHoldingPollMsg(startTimeDiff: Int, endTimeDiff:Int, targetTime:DateTime, baseLineTime:DateTime, actorStateData: TripActorStateData) = {
    val (others, readyMsg) = pollMsg(actorStateData.msgOrdered, startTimeDiff, endTimeDiff, targetTime, baseLineTime)
    if (!readyMsg.isEmpty) {
      // first one must be next stop
      val nextStop = others.head.stopRef
      // push all msg back
      val waitingSet: SortedSet[MsgOrderedDTO] = others ++ readyMsg
      askIsAvailableOfNextStop(nextStop)
      goto(Pending) using actorStateData.copy(msgOrdered = waitingSet)
    } else {
      stay()
    }
  }

  private[this] def handlePollMsg(startTimeDiff: Int, endTimeDiff:Int, targetTime:DateTime, baseLineTime:DateTime, actorStateData: TripActorStateData) = {

    val (restMsg, readyMsg) = pollMsg(actorStateData.msgOrdered, startTimeDiff, endTimeDiff, targetTime, baseLineTime)
    if (!readyMsg.isEmpty) {
      readyMsg.filter(_.msgType == VMResponse.Arrival).foreach(sendMsg)
      readyMsg.find(_.msgType == VMResponse.Departure) match {
        case Some(dto) =>
          if (!restMsg.isEmpty) {
            // put this those message back to the queue and waiting for the checking result
            val nextStop = restMsg.head.stopRef
            val waitingSet: SortedSet[MsgOrderedDTO] = restMsg ++ readyMsg
              .filter(_.msgType == VMResponse.Departure)
            askIsAvailableOfNextStop(nextStop)
            // will change status to pending
            goto(Pending) using actorStateData.copy(msgOrdered = waitingSet)
          } else {
            // last stop, just finished it
            sendMsg(dto)
            goto(Terminate)
          }

        case None =>
          goto(Active) using actorStateData.copy(msgOrdered = restMsg)
      }
    } else {
      if (isEmpty(actorStateData.msgOrdered) && stateName != Init) {
        goto(Terminate)
      } else {
        stay()
      }
    }
  }

  private def pollMsg(target: SortedSet[MsgOrderedDTO], startTimeDiff: Int, endTimeDiff:Int, targetTime:DateTime, baseLineTime:DateTime): (SortedSet[MsgOrderedDTO], Seq[MsgOrderedDTO]) = {
    val queryBaseLineTime = tripDate.withMillisOfDay(baseLineTime.getMillisOfDay)
    val startTime = queryBaseLineTime.minusSeconds(-1*startTimeDiff)
    val endTime = queryBaseLineTime.minusSeconds(-1*endTimeDiff)

    val readyMsg = target.filter(dto => isReadMsg(dto, startTime, endTime)).toSeq.sortBy(_.time.getMillis)
    val restMsg = target.filter(dto => isRestMsg(dto, startTime, endTime)).toSeq.sortBy(_.time.getMillis)
    (SortedSet(restMsg.toList: _*), readyMsg)
  }

  private def isReadMsg(dto:MsgOrderedDTO, startTime:DateTime, endTime:DateTime) = {
    val msgTime = dto.time.minusSeconds(-1*dto.delay.map(_.toSeconds.toInt).getOrElse(0))
    (msgTime.isAfter(startTime) || msgTime.isEqual(startTime)) && msgTime.isBefore(endTime)
  }

  private def isRestMsg(dto:MsgOrderedDTO, startTime:DateTime, endTime:DateTime) = {
    val msgTime = dto.time.minusSeconds(-1*dto.delay.map(_.toSeconds.toInt).getOrElse(0))
    msgTime.isEqual(endTime) || msgTime.isAfter(endTime)
  }

  /**
    * for this type of delay, we assume train has been block in the stop, which means arrival time doesn't change
    * We will delay the departure time and all the other times
    * this will simulate train has been block on the stop
    * @param delay
    * @param startRef
    * @param stopRef
    */
  private[this] def delayTrip(target: SortedSet[MsgOrderedDTO],
                              delay: Duration,
                              startRef: String,
                              stopRef: String): SortedSet[MsgOrderedDTO] = {
    val (unchanged, changed) =
      if (startRef.isEmpty) {
        (SortedSet[MsgOrderedDTO](), target)
      }
      else {
        target.span(_.stopRef == startRef)
      }

    val adjustedMsg =
      if (changed.head.msgType == VMResponse.Arrival) {
        adjustSavedMsg(changed.tail, delay) + changed.head
      } else {
        adjustSavedMsg(changed, delay)
      }
    val newMsgSet = unchanged ++ adjustedMsg
    sendTimetableUpdate(newMsgSet)
    log.info(s"Trip ${trip} has been delay for ${delay}")
    newMsgSet

  }

  private[this] def adjustSavedMsg(
      target: SortedSet[MsgOrderedDTO],
      delay: Duration): SortedSet[MsgOrderedDTO] = {
    val head = target.head
    target.map(msg => {
      if (head.msgType != VMResponse.Arrival) {
        msg.copy(delay = Some(delay))
      } else {
        if (msg != head) {
          msg.copy(delay = Some(delay))
        } else {
          msg
        }
      }
    })
  }

  private[this] def skipStopForStop(stopRef:String, target: SortedSet[MsgOrderedDTO]): SortedSet[MsgOrderedDTO] = {
    // find stop
    val stopPair = target.filter(_.stopRef == stopRef)
    if(stopPair.size != 2) {
      log.warning(s"Can NOT skip stop for $stopPair")
      target
    }  else {
      val arrival = stopPair.find(_.msgType == VMResponse.Arrival).get
      val departure = stopPair.find(_.msgType == VMResponse.Departure).get
      val diff = departure.time.getMillis - arrival.time.getMillis

      target.map(dto => {
        if (dto.stopRef == stopRef) {
          if(dto.msgType == VMResponse.Departure) {
            dto.copy(time = arrival.time)
          } else {
            dto
          }
        } else if(dto.time.isBefore(arrival.time)) {
          dto
        } else {
          dto.copy(time = dto.time.minusMillis(diff.toInt))
        }
      })
    }
  }

  private[this] def sendTimetableUpdate(
      target: SortedSet[MsgOrderedDTO]): Unit = {
    target
      .groupBy(_.stopRef)
      .map(pair => {
        pair._2.find(p => p.msgType == VMResponse.Arrival) match {
          case Some(arrivalMsg) =>
            val departureMsg = pair._2
              .find(p => p.msgType == VMResponse.Departure)
              .getOrElse(arrivalMsg)
            nodeControllerSender(
              UpdateTimeTable(
                "etg",
                arrivalMsg.stopRef,
                arrivalMsg.trip,
                arrivalMsg.time.minusSeconds(
                  -arrivalMsg.delay.getOrElse(Duration.Zero).toSeconds.toInt),
                departureMsg.time.minusSeconds(
                  -departureMsg.delay.getOrElse(Duration.Zero).toSeconds.toInt)
              ))
          case None =>
        }
      })

    // send message to trip controller
    sendMovementUpdate(tripId, target)
  }

  /**
    *   holding the departure message for next poll
    *   holding for next 10 seconds
    */
  private[this] def holdingTrip(
      tripActorStateData: TripActorStateData): TripActorStateData = {
    log.info("holding message for 10 second: {}", trip)
    val changedTripDelay = tripActorStateData.currentTripDelay + 10.second
    val adjustedMsg =
      delayTrip(tripActorStateData.msgOrdered, changedTripDelay, "", "")
    // send msg to node and block that node
    val currentStopRef = adjustedMsg.head.stopRef
    nodeSender(currentStopRef, NodeBlocked(group, currentStopRef))

    val tripMovementUpdateEvent = new TripMovementEvent(
      tripId,
      trip,
      DateTime.now(),
      currentStopRef,
      ETGServerEvent.TripHolding,
      changedTripDelay.toSeconds.toInt)
    tripMonitorSender(
      new ETGServerEvent(ETGServerEvent.TripActive, tripMovementUpdateEvent))

    tripActorStateData.copy(msgOrdered = adjustedMsg,
                            currentTripDelay = changedTripDelay)
  }

  /**
    * ask if the next stop is available before send departure message
    */
  private[this] def askIsAvailableOfNextStop(stopRef: String) = {
    nodeSender(stopRef, CheckNodeAvailableRequest(group, stopRef))
  }

  private[this] def sendDepartureEventToNode(stopRef: String) = {
    nodeSender(stopRef, TrainDeparture(trip, "etg", stopRef, DateTime.now()))
  }

  /**
    * send message to JMS and monitor
    * @return
    */
  private[this] def sendMsg = (dto: MsgOrderedDTO) => {
    log.debug("send vm for {} [{}- {} - {}]",
              trip,
              dto.time,
              dto.msgType,
              dto.stopRef)
    if(dto.msgType == VMResponse.Arrival) {
      tripMonitorSender(new ETGServerEvent(ETGServerEvent.ActualArrivalMovement, TripMovementEvent(
        tripId = tripId,
        trip = trip,
        updateTime = dto.time.minusSeconds(-1*dto.delay.map(_.toSeconds.toInt).getOrElse(0)),
        stopRef = dto.stopRef,
        eventType = ETGServerEvent.ActualArrivalMovement
      )))
    } else {
      tripMonitorSender(new ETGServerEvent(ETGServerEvent.ActualDepartureMovement, TripMovementEvent(
        tripId = tripId,
        trip = trip,
        updateTime = dto.time.minusSeconds(-1*dto.delay.map(_.toSeconds.toInt).getOrElse(0)),
        stopRef = dto.stopRef,
        eventType = ETGServerEvent.ActualDepartureMovement
      )))
    }

    // update trip status to active
    val tripMovementUpdateEvent = new TripMovementEvent(
      tripId,
      dto.trip,
      dto.time.minusSeconds(-1*dto.delay.map(_.toSeconds.toInt).getOrElse(0)),
      dto.stopRef,
      ETGServerEvent.TripActive,
      dto.delay.map(_.toSeconds.toInt).getOrElse(0)
    )
    tripMonitorSender(
      new ETGServerEvent(ETGServerEvent.TripActive, tripMovementUpdateEvent))

  }

  /////////////////////////////////////////////////////////////
  //  reset the trip &
  //  regenerate the calls from timetable
  /////////////////////////////////////////////////////////////
  private[this] def reset(previous: TripActorStateData): TripActorStateData = {

    val resetMsgSet = previous.timeTable
      .map(pair => (generateMsgByCallDTO(pair._1), generateMsgByCallDTO(pair._2)))
      .foldLeft(scala.collection.SortedSet[MsgOrderedDTO]())((s, p) => {
        s + p._1 + p._2
      })
    previous.copy(msgOrdered = resetMsgSet,
                  previous.timeTable,
                  currentTripDelay = Duration.Zero)

  }

  private[this] def generateMsgByCallDTO(
      dto: PTOrderedCallDTO): MsgOrderedDTO = {
    val result:MsgOrderedDTO =
      if (dto.callType == 0) {
        // arrive
          new MsgOrderedDTO(VMResponse.Arrival,
          dto.time,
          dto.trip,
          dto.stopRef)
      } else {
        // deparutre
          new MsgOrderedDTO(VMResponse.Departure,
            dto.time,
            dto.trip,
            dto.stopRef)
      }

      result
  }

  private[this] def handleTimeTableRegister(
      arrivalDto: PTOrderedCallDTO, departureDto: PTOrderedCallDTO,
      tripActorStateData: TripActorStateData): TripActorStateData = {
    val updatedTimeTable = tripActorStateData.timeTable :+ (arrivalDto, departureDto)
    val arrivalMsg = generateMsgByCallDTO(arrivalDto)
    val departureMsg = generateMsgByCallDTO(departureDto)
    // send Node update
    nodeControllerSender(
      new RegisterTimeTable("etg",
        arrivalMsg.stopRef,
        trip,
        arrivalMsg.time,
        departureMsg.time))
    val msgSet = tripActorStateData.msgOrdered + arrivalMsg + departureMsg
    tripActorStateData.copy(msgOrdered = msgSet,
      timeTable = updatedTimeTable)

  }

  /**
    * this func only remove the current runtime plan, not original plan
    * @param stopRef
    */
  private[this] def handleRemoveTripPlanCall(stopRef:String, msgOrdered: SortedSet[MsgOrderedDTO]):SortedSet[MsgOrderedDTO] = {
    nodeControllerSender(RemoveTripPlanOnNode("etg", stopRef, trip))
    val changedMsg = msgOrdered.filter(_.stopRef != stopRef)
    // send message to trip controller
    sendMovementUpdate(tripId, changedMsg)
    changedMsg
  }
}
