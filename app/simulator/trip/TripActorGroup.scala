package simulator.trip

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import org.joda.time.DateTime
import play.api.Logger
import playback.channel.PollMsg
import simulator.command._
import simulator.dto.{ETGServerEvent, TripMovementEvent}
import simulator.trip.TripController.{RemoveTripPlanCall, RequestTrackTrip}

object TripActorGroup {
  def props(groupId: String): Props = Props(new TripActorGroup(groupId))
}

class TripActorGroup(groupId: String)
    extends Actor
    with ActorLogging
    with TripMonitorSender {
  // tripId -> actor
  var tripToActor = Map.empty[String, ActorRef]
  // actor -> tripId
  var actorToTrip = Map.empty[ActorRef, String]
  var tripMonitorMap: Map[String, TripActor.Value] =
    Map.empty[String, TripActor.Value]

  override def receive = {
    //request for delay trip
    case delay @ DelayTrip(`groupId`, _, _, _, _) =>
      tripToActor.get(delay.tripId) match {
        case Some(tripActor) ⇒
          tripActor forward delay
        case None ⇒
          log.debug("Ignore delay request for {}", delay)
      }

    //remove the trip plan call
    case event @ RemoveTripPlanCall(`groupId`, _, _, _) =>
      tripToActor.get(event.tripId) match {
        case Some(tripActor) ⇒
          tripActor forward event
        case None ⇒
          log.debug("Ignore remove trip plan call request for {}", event)
      }

    // request for skip stop for trip
    case skipStopOnTrip @ SkipStopOnTrip(groupId, _, _) =>
      tripToActor.get(skipStopOnTrip.tripId) match {
        case Some(tripActor) ⇒
          tripActor forward skipStopOnTrip
        case None ⇒
          log.debug("Ignore skip Stop request for {}", skipStopOnTrip)
      }

    case reset @ ResetTrip(`groupId`, _) =>
      tripToActor.get(reset.tripId) match {
        case Some(tripActor) ⇒
          tripActor forward reset
        case None ⇒
          log.debug("Ignore reset request for {}", reset)
      }

      // reset all trips
    case reset @ ResetTripAll(`groupId`) =>
      tripToActor.foreach {
        case (tripId:String, tripActor:ActorRef) =>
          tripActor ! ResetTrip(groupId, tripId)
      }

    case terminate @ TerminateTrip(`groupId`, _) =>
      tripToActor.get(terminate.tripId) match {
        case Some(tripActor) ⇒
          tripActor forward terminate
        case None ⇒
          log.debug("Ignore terminated request for {}", terminate)
      }

    case req @ RequestTrackTrip(`groupId`, _, _, _, _, _) =>
      tripToActor.get(req.tripId) match {
        case Some(tripActor) ⇒
          tripActor forward req
          tripMonitorSender(new ETGServerEvent(
            ETGServerEvent.TripCreate,
            new TripMovementEvent(req.departureDto.tripId, req.trip, DateTime.now(), "", ETGServerEvent.TripCreate)))

        case None ⇒
          log.debug("Creating trip actor for {}", req.trip)
          val tripActor = context.actorOf(TripActor.props(groupId, req.tripId, req.trip, req.tripDate),
                                          s"trip-${req.tripId}")
          context.watch(tripActor)

          // send monitor message
          tripMonitorSender(
            new ETGServerEvent(
              ETGServerEvent.TripInit,
              new TripMovementEvent(req.departureDto.tripId, req.trip, DateTime.now(), "", ETGServerEvent.TripInit)))

          actorToTrip += tripActor -> req.tripId
          tripToActor += req.tripId -> tripActor
          tripActor forward req
      }

    case pollMsg @ PollMsg(_,_,_,_) =>
      actorToTrip.foreach(pair => pair._1 forward pollMsg)

    case Terminated(tripActor) ⇒
      val trip = actorToTrip(tripActor)
      log.info("Trip actor for {} has been terminated", trip)
      actorToTrip -= tripActor
      tripToActor -= trip
      // send monitor message
      tripMonitorSender(
        new ETGServerEvent(
          ETGServerEvent.TripTerminated,
          new TripMovementEvent(trip, trip, DateTime.now(), "", ETGServerEvent.TripTerminated)))

  }

}
