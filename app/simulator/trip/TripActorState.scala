package simulator.trip

import service.dataset.PTOrderedCallDTO
import simulator.dto.MsgOrderedDTO

import scala.collection.SortedSet
import scala.concurrent.duration.Duration

sealed trait TripActorState {}

case object Init extends TripActorState
case object Creating extends TripActorState
case object Pending extends TripActorState
case object Active extends TripActorState
case object Holding extends TripActorState
case object Terminate extends TripActorState

case class TripActorStateData(msgOrdered: SortedSet[MsgOrderedDTO],
                              timeTable: List[(PTOrderedCallDTO, PTOrderedCallDTO)],
                              currentTripDelay: Duration = Duration.Zero)
