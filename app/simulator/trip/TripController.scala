package simulator.trip

import javax.inject.Inject

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, Terminated}
import org.joda.time.DateTime
import playback.channel.{PollMsg, Protocol}
import service.dataset.PTOrderedCallDTO
import simulator.command._
import simulator.dto.{ETGServerEvent, MsgOrderedDTO}
import simulator.trip.TripController.{RemoveTripPlanCall, RequestTrackTrip, TripMovementUpdated}

import scala.collection.SortedSet
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

final object StopTripCmd
final case class StopTripCmd()

final object DelayTripCmd
final case class DelayTripCmd(delay: Int)

object TripController {

  case class RequestTrackTrip(groupId: String,
                              tripId:String,
                              trip: String,
                              tripDate:DateTime,
                              arrivalDto: PTOrderedCallDTO,
                              departureDto: PTOrderedCallDTO)
      extends Protocol
  case class RequestTripList(groupId: String, status: TripActor.Value)
      extends Protocol
  case class ReplyTripList(requestId: String, keySet: Set[String])
      extends Protocol
  case class RemoveTripPlanCall(groupId: String,
                                tripId:String,
                                trip: String,
                                stopRef:String) extends Protocol

  case class TripMovementUpdated(tripId: String, calls: SortedSet[MsgOrderedDTO])
      extends Protocol

}

class TripController @Inject()(system: ActorSystem)(implicit ec: ExecutionContext)
    extends Actor
    with ActorLogging {
  var groupIdToActor = Map.empty[String, ActorRef]
  var actorToGroupId = Map.empty[ActorRef, String]

  override def receive = {
    // poll msg
    case pollMsg @PollMsg(_,_,_,_) =>
      if (!actorToGroupId.keys.isEmpty) {
        actorToGroupId.keys.foreach(_ ! pollMsg)
      }

    // request for reset trip
    case reset @ ResetTrip(groupId, _) =>
      groupIdToActor.get(groupId) match {
        case Some(ref) ⇒
          ref forward reset
        case None ⇒
          log.debug("Ignore reset request for {}", reset)
      }

    // request for reset all trips
    case reset @ ResetTripAll(groupId) =>
      groupIdToActor.get(groupId) match {
        case Some(ref) ⇒
          ref forward reset
        case None ⇒
          log.debug("Ignore reset request for {}", reset)
      }

    // remove trip plan call
    case reset @ RemoveTripPlanCall(groupId, _, _, _) =>
      groupIdToActor.get(groupId) match {
        case Some(ref) ⇒
          ref forward reset
        case None ⇒
          log.debug("Ignore remove trip plan call request for {}", reset)
      }

    // request for delay trip
    case delay @ DelayTrip(groupId, _, _, _, _) =>
      groupIdToActor.get(groupId) match {
        case Some(ref) ⇒
          ref forward delay
        case None ⇒
          log.debug("Ignore delay request for {}", delay)
      }

    // request for skip stop for trip
    case skipStopOnTrip @ SkipStopOnTrip(groupId, _, _) =>
      groupIdToActor.get(groupId) match {
        case Some(ref) ⇒
          ref forward skipStopOnTrip
        case None ⇒
          log.debug("Ignore skip stop request for {}", skipStopOnTrip)
      }

    // request for terminated trip
    case terminate @ TerminateTrip(groupId, _) =>
      groupIdToActor.get(groupId) match {
        case Some(ref) ⇒
          ref forward terminate
        case None ⇒
          log.debug("Ignore terminate request for {}", terminate)
      }

    // request for create or add trip
    case trackMsg @ RequestTrackTrip(groupId,_, _, _, _, _) ⇒
      groupIdToActor.get(groupId) match {
        case Some(ref) ⇒
          ref forward trackMsg
        case None ⇒
          log.debug("Creating device group actor for {}", groupId)
          val groupActor =
            context.actorOf(TripActorGroup.props(groupId), "group-" + groupId)
          context.watch(groupActor)
          groupActor forward trackMsg
          groupIdToActor += groupId -> groupActor
          actorToGroupId += groupActor -> groupId
      }

    case TripMovementUpdated(tripId, calls) =>
      ChangedTripStore.addChangedTrip(tripId, calls)

    // terminate group
    case Terminated(groupActor) ⇒
      val groupId = actorToGroupId(groupActor)
      log.info("Device group actor for {} has been terminated", groupId)
      actorToGroupId -= groupActor
      groupIdToActor -= groupId

  }

}
