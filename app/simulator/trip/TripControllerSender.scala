package simulator.trip

import akka.actor.{ActorContext, ActorRef}
import simulator.dto.MsgOrderedDTO
import simulator.trip.TripController.TripMovementUpdated

import scala.collection.SortedSet

trait TripControllerSender {
  def tripControllerSender(msg: Any)(implicit context: ActorContext,
                                     sender: ActorRef) = {
    context
      .actorSelection("akka://application/user/trip-controller") ! msg
  }

  def sendMovementUpdate(tripId: String, calls: SortedSet[MsgOrderedDTO])(
      implicit context: ActorContext,
      sender: ActorRef) = {
    context
      .actorSelection("akka://application/user/trip-controller") ! new TripMovementUpdated(
      tripId,
      calls)
  }
}
