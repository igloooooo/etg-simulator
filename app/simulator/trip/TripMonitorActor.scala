package simulator.trip

import javax.inject.Inject

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, Terminated}
import api.dto.TrainActualDto
import org.joda.time.DateTime
import play.api.Logger
import service.dataset.{PTStore, VMStore}
import simulator.ETGRepositoryService
import simulator.dto.{ETGServerEvent, NodeUpdateEvent, TripNodeConflictEvent}
import simulator.stopnode.NodeStatusStore

import scala.collection.mutable
import scala.concurrent.ExecutionContext

/**
  * This actor will manage the communication between core and client
  * tripmonitor -> JMS
  * tripmonitor -> VMStore
  */
object TripMonitorActor {
  case class RegistreUser(actorRef: ActorRef)
}

class TripMonitorActor @Inject()(system: ActorSystem, etgRepositoryService: ETGRepositoryService)(implicit ec: ExecutionContext) extends Actor with ActorLogging {
  import TripMonitorActor._

  // tripID-> trip status
  var tripsMap = mutable.Map[String, TripStatus]()
  // stopRef => nodeUpdateEvent
  var nodeConflictMap = mutable.Map[String, TripNodeConflictEvent]()

  override def receive = {

    // terminate trip
    case event @ ETGServerEvent(ETGServerEvent.TripTerminated,
                                movementEvent,
                                _,
                                _,
                                _) =>
      tripsMap.put(movementEvent.tripId,
        tripsMap.get(movementEvent.tripId)
          .map(_.copy(status = movementEvent.eventType))
          .getOrElse(TripStatus(
            tripID = movementEvent.tripId,
            trip = movementEvent.trip,
            delay = 0,
            status = movementEvent.eventType,
            DateTime.now())))

    // update trip status
    case event @ ETGServerEvent(ETGServerEvent.TripCreate |
                                ETGServerEvent.TripInit,
                                move,
                                _,
                                _,
                                _) =>
      tripsMap.put(move.tripId,
        tripsMap.get(move.tripId)
          .map(_.copy(status = move.eventType))
          .getOrElse(TripStatus(
            tripID = move.tripId,
            trip = move.trip,
            delay = 0,
            status = move.eventType,
            DateTime.now())))

    case event @ ETGServerEvent(ETGServerEvent.ActualArrivalMovement |
                                ETGServerEvent.ActualDepartureMovement,
                                move,
                                _,
                                _,
                                _) =>
      val actualType = if(event.eventType == ETGServerEvent.ActualArrivalMovement) 0 else 1
      // put it into VMStore
      VMStore.addVM(TrainActualDto(
        trip = move.trip,
        time=move.updateTime,
        actualType=actualType,
        stopRef=move.stopRef))
      // TODO: trigger JMS to ETG

    // handle node conflict event
    case event @ ETGServerEvent(ETGServerEvent.TripNodeConflict,
                                _,
                                nodeConflict,
                                _,
                                _) =>
      nodeConflictMap.put(nodeConflict.stopRef, nodeConflict)

    // handle node update event
    case event @ ETGServerEvent(ETGServerEvent.NodeUpdate,
                                _,
                                _,
                                nodeUpdateEvent,
                                _) =>
      NodeStatusStore.addNodeStatus(nodeUpdateEvent)

    case event =>
//      Logger.warn(event.toString)

  }

}

case class TripStatus(tripID:String, trip:String, delay:Int, status:String, lastUpdate:DateTime)
