package simulator.trip

import akka.actor.{ActorContext, ActorRef}

trait TripMonitorSender {
  def tripMonitorSender(msg: Any)(implicit context: ActorContext,
                                  sender: ActorRef) = {
    context
      .actorSelection("akka://application/user/trip-monitor") ! msg
  }
}
