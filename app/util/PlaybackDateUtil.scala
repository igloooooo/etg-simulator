package util

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

object PlaybackDateUtil {
  private val dbFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  private val dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")
  def convertToDBFormat(time:DateTime):String ={
    dbFormatter.print(time)
  }

  def convertToDate(time:DateTime):String ={
    dateFormatter.print(time)
  }
}
