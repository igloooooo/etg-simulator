package wsclient

import javax.inject.Inject

import play.api.Configuration
import play.api.libs.ws.WSClient

class MiddlewareWSClient @Inject() (ws: WSClient, config: Configuration){

  def client(url:String) = {
    val host = config.get[String]("etg.api.host")
    val port = config.get[String]("etg.api.port")
    ws.url(s"http://$host:$port/" + url)
  }
}
