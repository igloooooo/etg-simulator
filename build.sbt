import com.typesafe.sbt.packager.MappingsHelper._
mappings in Universal ++= directory(baseDirectory.value / "public")

name := "play-simulator-new"

version := "1.1"

scalaVersion := "2.12.2"

// depends
libraryDependencies += ws
libraryDependencies ++= Seq(
  "com.typesafe.play" % "play-json-joda_2.12" % "2.6.0",
  "com.lightbend.akka" %% "akka-stream-alpakka-jms" % "0.18",
  "javax.jms" % "javax.jms-api" % "2.0.1",
  "org.apache.activemq" % "activemq-all" % "5.14.4",
  "com.typesafe.slick" %% "slick" % "3.2.0",
  "com.h2database" % "h2" % "1.4.187",
  "com.lightbend.akka" %% "akka-stream-alpakka-csv" % "0.18")

lazy val `play-simulator-new` = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(guice, filters, jdbc , cacheApi ,ws , specs2 % Test)

// Play framework hooks for development
PlayKeys.playRunHooks += WebpackServer(file("./front"))

unmanagedResourceDirectories in Test +=  baseDirectory ( _ /"target/web/public/test" ).value

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

// Production front-end build
lazy val cleanFrontEndBuild = taskKey[Unit]("Remove the old front-end build")

cleanFrontEndBuild := {
  val d = file("public/bundle")
  if (d.exists()) {
    d.listFiles.foreach(f => {
      if(f.isFile) f.delete
    })
  }
}

lazy val frontEndBuild = taskKey[Unit]("Execute the npm build command to build the front-end")

frontEndBuild := {
  println(Process("npm install", file("front")).!!)
  println(Process("npm run build", file("front")).!!)
}

frontEndBuild := (frontEndBuild dependsOn cleanFrontEndBuild).value

dist := (dist dependsOn frontEndBuild).value
