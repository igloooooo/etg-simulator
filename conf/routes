# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# Home page
GET     /                                   controllers.FrontController.index()

# API
POST    /api/v1/vm/dataset                  api.VMConfig.loadDataSet
POST    /api/v1/pt/dataset                  api.PTConfig.loadDataSet
POST    /api/v1/far/dataset                 api.FAConfig.loadFARDataSet
POST    /api/v1/fap/dataset                 api.FAConfig.loadFAPDataSet
POST    /api/v1/no/dataset                  api.NOConfig.loadDataSet
POST    /api/v1/im/dataset                  api.IMConfig.loadDataSet

POST    /api/v1/fa/request                  api.FAConfig.faRequestHandler
POST    /api/v1/no/request                  api.NOConfig.noRequestHandler
POST    /api/v1/pt/request                  api.PTConfig.ptRequestHandler
GET     /api/v1/pt/dataset                  api.PTConfig.requestAllDataSet

GET     /api/v1/trip/:tripId/status         api.TripController.tripStatus(tripId:String)
GET     /api/v1/trip/:tripId                api.TripController.tripDetails(tripId:String)
GET     /api/v1/trip/:tripId/platform       api.TripController.tripPlatformDetails(tripId:String)

# play visual
GET     /api/v1/etg/pt/border/:id           api.PTVisualController.timeTableHandler(id:Int, startDate:Option[String], endDate:Option[String])
GET     /api/v1/etg/pt/sector               api.PTVisualController.timeTableBySectorHandler(borderId:Int, sectorIndex:Int, startDate:Option[String], endDate:Option[String])
GET     /api/v1/etg/pt/trips                api.PTVisualController.timeTableBySectorWithTripsHandler(borderId:Int, sectorIndex:Int, startDate:Option[String], endDate:Option[String], tripIdList:List[String])
GET     /api/v1/etg/markline/:id            api.PTVisualController.markLineHandler(id:Int)
GET     /api/v1/etg/trip/call               api.PTVisualController.convertToPTCallItem(borderId:Int, sectorId:Int, tripId:String, value:Int)
GET     /api/v1/etg/markline/trip/:trip     api.PTVisualController.generateMarkLineByTrip(trip:String)
GET     /api/v1/etg/relatedTrips/:trip      api.PTVisualController.generateRelatedTrips(trip:String)

GET     /api/v1/etg/vm/board/:id            api.VMVisualController.incrementVMByBoardId(id:Int, startTime:Option[String], endTime:Option[String])
GET     /api/v1/etg/board/:id/sector        api.BorderVisualController.sectorList(id:Int)
GET     /api/v1/etg/vm/trip/:trip           api.VMVisualController.incrementVMByTrip(trip:String, startTime:Option[String], endTime:Option[String])
GET     /api/v1/etg/no/:id                  api.NOVisualController.networkOutageHandler(id:Int)

GET     /api/v1/geo/node                    api.NetworkVisualController.loadGeoNodes

GET     /api/v1/network/node                api.NetworkVisualController.nodeHandler
GET     /api/v1/network/blocknodes          api.NetworkVisualController.loadBlockNodes
GET     /api/v1/network/platform            api.NetworkVisualController.platformLabels
GET     /api/v1/network/node/:stopRef       api.NetworkVisualController.nodeDetails(stopRef:String)
GET     /api/v1/network/link                api.NetworkVisualController.linkHandler
GET     /api/v1/network/vm                  api.VMVisualController.loadVMForNetworkHandler
GET     /api/v1/network/path/found          api.NetworkVisualController.findPathHandler(fromNodeId:String, toNodeId:String)
GET     /api/v1/network/path/reRoute        api.NetworkVisualController.changePathOnNodeHandler(oldStartNodeId:String, nextNodeId:String, endNodeId:String)
GET     /api/v1/network/path/trip/:tripId   api.NetworkVisualController.findTripPathHandler(tripId:String)
GET     /api/v1/network/path/reRoute/trip   api.NetworkVisualController.changeTripPathOnNodeHandler(tripId:String, oldStartNodeId:String, nextNodeId:String, endNodeId:String)
GET     /api/v1/geo/vm                      api.VMVisualController.loadVMForNetwork3dHandler
GET     /api/v1/platform/:masterNode        api.PlatformVisualController.loadPlatFormStatus(masterNode:String, startTime:Option[String], endTime:Option[String])
GET     /api/v1/platform/lane/:masterNode   api.PlatformVisualController.loadPlatFormLane(masterNode:String)

# statistic
GET     /api/v1/statis/curDelay             api.StatisticController.currentDelay(startTime:Option[String], endTime:Option[String])
GET     /api/v1/statis/curSkipStop          api.StatisticController.currentSkipStop(startTime:Option[String], endTime:Option[String])
GET     /api/v1/statis/curKPI               api.StatisticController.currentKPI(startTime:Option[String], endTime:Option[String])
GET     /api/v1/statis/curDelayAndSkip      api.StatisticController.currentDelayAndSkip(startTime:Option[String], endTime:Option[String])
GET     /api/v1/statis/totalDelayAndSkip    api.StatisticController.totalDelayAndSkip()
GET     /api/v1/statis/totalRunStatus       api.StatisticController.totalRunStatus(startTime:Option[String], endTime:Option[String])
GET     /api/v1/statis/perMinRunStatus      api.StatisticController.loadVMAndTripsCountPerMinute()

# simulator command control
POST    /api/v1/simulator/cmd/skipStop      controllers.SimulatorCommandController.skipStopHandler
POST    /api/v1/simulator/cmd/delayMovement controllers.SimulatorCommandController.delayMovementHandler
POST    /api/v1/simulator/cmd/delayTrip     controllers.SimulatorCommandController.delayTripPlanHandler
POST    /api/v1/simulator/cmd/terminateTrip controllers.SimulatorCommandController.terminateTripHandler
POST    /api/v1/simulator/cmd/updateTrip    controllers.SimulatorCommandController.updateTripHandler
POST    /api/v1/simulator/cmd/mergeTrip     controllers.SimulatorCommandController.mergeTripHandler
POST    /api/v1/simulator/cmd/blockNode     controllers.SimulatorCommandController.blockNodeHandler
POST    /api/v1/simulator/cmd/releaseNode   controllers.SimulatorCommandController.releaseNodeHandler
POST    /api/v1/simulator/cmd/releaseAllNodes   controllers.SimulatorCommandController.releaseAllNodesHandler

# simulator conflict
GET     /api/v1/simulator/conflict          api.ConflictController.allConflictHandler

# play control
POST    /play                           controllers.PlaybackController.actionHandler




# channel config

# Websocket
GET     /ws                         controllers.WSController.ws

# Map static resources from the /public folder to the /assets URL path
GET     /assets/*file               controllers.Assets.at(path="/public", file)

    