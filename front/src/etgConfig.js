import {networkOutageRender} from './networkOutageConfig.js'
import {getYesterdaysDate, getTomorrowdaysDate, generateXaxisData, convertToETGDisplayData} from "./utils.js";
import * as COLOR from './colorConst.js'

const axios = require('axios');
function generateEtgConfig(sectorName, displayName) {
    var chartOption = {
        tooltip: {
                formatter: function (params, ticket, callback) {
                    var trip = getTripFromTripID(params.seriesName);
                    axios.get('/api/v1/trip/' + trip + '/status').then((response) => {
                        callback(ticket, tripHtmlContent(response.data));
                    });
                    return 'Loading';
                },
        },
        legend: {
            show:false
        },
        toolbox: {
            left: 'center',
            feature: {
                dataZoom: {
                    yAxisIndex: 'none',
                    title : {
                        zoom : 'Zoom in',
                        back : 'Zoom out'
                    }
                }
            }
        },
        series: []
    };
    return chartOption;
}

function generateETGMarkLineConfig(config, sectorMarkLines, isYZoom, xZoomStart, xZoomEnd) {
    let chartOption = config;
    var markLineData = [];
    var markLineMaxValue = [];
    var markLineMinValue = [];
    var totalHeight = 0;
    if(sectorMarkLines !== undefined) {
        for(var i=0; i< sectorMarkLines.length; i++) {
            var maxValueForSector = 0;
            var minValueForSector = 0;
            var markLineForSector = [];
            var topGap = 0;
            var bottomGap = 0;
            for(var j=0; j<sectorMarkLines[i].length; j++) {
                var MarkLine = sectorMarkLines[i][j];

                if(MarkLine.yAxis > maxValueForSector) {
                    maxValueForSector = MarkLine.yAxis;
                }
                if(MarkLine.yAxis < minValueForSector) {
                    minValueForSector = MarkLine.yAxis;
                }

                markLineForSector.push({
                    yAxis:MarkLine.yAxis,
                    label:{
                        show: true,
                        position: 'start',
                        formatter: MarkLine.name + '(' + MarkLine.KMPost +')'}});
            }
            if(i == 0) {
                topGap = 20;
            } else {
                topGap = 0;
            }
            if(i == sectorMarkLines.length - 1) {
                bottomGap = 20;
            } else {
                bottomGap = 0;
            }
            /**
             * 40 is the gap of sector
             * 50 is min value
             * top gap only for first sector
             * bottom gap only for last sector
             *
             */
            totalHeight = totalHeight + (maxValueForSector-50) + topGap + bottomGap + 40;
            markLineMaxValue.push(maxValueForSector);
            markLineMinValue.push(minValueForSector);
            markLineData.push(markLineForSector);
        }
    }
    // remove the last gap
    totalHeight = totalHeight - 40;
    // generate multi grid and yAxis
    // generate markline data
    var markLineSeriesData = [];
    var yAxisData = [];
    var xAxisData = [];
    var gridData = [];
    var axisSeq = [];
    var top = 50;
    var rate = 700/totalHeight;
    for(var k =0; k< markLineMaxValue.length; k++) {
        var height = (markLineMaxValue[k]-50)*rate;
        axisSeq.push(k);
        if(k == 0) {
            height += 20*rate;
        }
        if(k == markLineMaxValue.length - 1) {
            gridData.push({
                left: 120,
                right: isYZoom ? 40 : 20,
                top: top,
                bottom: 60
            });
        } else {
            gridData.push({
                left: 120,
                right: isYZoom ? 40 : 20,
                top: top,
                height: height
            });
        }

        yAxisData.push({
            type: 'value',
            show: true,
            splitLine: {
                show: false
            },
            axisTick:{
                show: false
            },
            axisLabel:{
                show: false
            },
            gridIndex: k,
            min: k==markLineMaxValue.length - 1 ? 30 : 50,
            max: k==0 ? markLineMaxValue[k] + 20 : markLineMaxValue[k]
        });
        xAxisData.push({
            type: 'time',
            show: false,
            gridIndex: k,
            axisTick:{
                show: false
            },
            axisLabel:{
                show: true
            },
            splitLine: {
                show: false
            },
            min:getYesterdaysDate(),
            max:getTomorrowdaysDate()
        });
        top = top + height + 40*rate;
        markLineSeriesData = markLineSeriesData.concat(generateSeriesInit(k, markLineData[k],markLineMaxValue.length-1));
    }

    chartOption.yAxis = yAxisData;
    chartOption.xAxis = xAxisData;
    chartOption.grid = gridData;
    chartOption.series = markLineSeriesData;
    chartOption.dataZoom = [
        {
            type: 'slider',
            xAxisIndex: axisSeq,
            filterMode: 'none',
            start: xZoomStart === undefined ? 40: xZoomStart,
            end: xZoomEnd === undefined ? 55: xZoomEnd
        },
        {
            type: 'inside',
            xAxisIndex: axisSeq,
            filterMode: 'none',
            start: xZoomStart === undefined ? 40: xZoomStart,
            end: xZoomEnd === undefined ? 55: xZoomEnd
        }
    ];
    if(isYZoom) {
        chartOption.dataZoom.push({
            type: 'slider',
            yAxisIndex: axisSeq,
            filterMode: 'none',
            start: 25,
            end: 65
        });
        chartOption.dataZoom.push({
            type: 'inside',
            yAxisIndex: axisSeq,
            filterMode: 'none',
            start: 25,
            end: 65
        });
    }

    return chartOption;
}


// this is for trip pt
function generatePTSeries(index, tripId, trip, data, targetTip, isChanged) {
    var opacity = 0.4;
    if (trip === targetTip) {
        opacity = 1;
    }
    let tripData = convertToETGDisplayData(data);
    return {
        name: tripId + '-pt-' + index,
        type: 'line',
        showSymbol: false,
        hoverAnimation: false,
        yAxisIndex: index,
        xAxisIndex: index,
        lineStyle:{
            color: isChanged ? COLOR.etg_trip_pt_changed :COLOR.etg_trip_pt_normal,
            width: 1,
            type: 'dotted',
            opacity: opacity
        },
        zlevel:index,
        animation:false,
        sampling:'average',
        silent: true,
        data: tripData
    }
}

// this is for train actual
function generateTripSeries(index, tripId, trip, data, targetTrip) {
    var lineWidth = 1;
    if (trip === targetTrip) {
        lineWidth = 2;
    }
    return {
        name: tripId + '-' + index,
        type: 'line',
        // showSymbol: false,
        symbolSize: 1,
        hoverAnimation: false,
        yAxisIndex: index,
        xAxisIndex: index,
        lineStyle:{
            color: '#000000',
            width: lineWidth
        },
        itemStyle:{
            normal:{
                color:'#000000',
            }
        },
        emphasis: {
            itemStyle: {
                borderWidth: 1,
                borderColor: '#ef8504'
            }
        },
        markPoint: {
            symbolSize: 1,
            label:{
                show: true,
                formatter: function (param) {
                    return trip;
                },
                color:'#000000',
                rotate: -45,
                fontSize:8
            },
            data: [
                {
                    type: 'min',
                    name: 'Min',
                    valueDim: 'x'
                }
            ]
        },
        zlevel:100,
        animation:false,
        sampling:'average',
        silent: false,
        data: []
    }
}

function generateSeriesInit(index, markLineData, totalIndex) {
    return [{
        name: 'marklinelevel-' + index,
        type: 'line',
        data: [],
        yAxisIndex: index,
        xAxisIndex: index,
        markLine: {
            silent: true,
            symbol: ['',''],
            lineStyle:{
                color: COLOR.etg_markline,
                type: 'solid',
                width:1,
                opacity: 0.5
            },
            zlevel: 1,
            animation:false,
            data: markLineData
        }
    },{
        name: 'outagelevel-' + index,
        type: 'custom',
        renderItem: networkOutageRender,
        yAxisIndex: index,
        xAxisIndex: index,
        encode: {
            x: [0, 2],
            y: [1, 3],
            tooltip: [0, 1, 2, 3, 4, 5],
            itemName: 4
        },
        animation:false,
        tooltip: {
            position: 'inside',
            formatter: function(param) {
                console.log(param.data.value[5]);
                return param.data.value[5];
            }
        },
        data: [],
        zlevel: 2
    },{
        name: 'ten-' + index,
        type: 'line',
        silent: true,
        yAxisIndex: index,
        xAxisIndex: index,
        markLine : {
            silent: true,
            symbol: ['',''],
            label: {
                show:false
            },
            lineStyle:{
                color: COLOR.etg_markline,
                type: 'dashed',
                width:1,
                opacity: 0.5
            },
            zlevel: 1,
            animation:false,
            data : generateXaxisData(10, index, totalIndex, false)
        }
    },{
        name: 'sixty-' + index,
        type: 'line',
        silent: true,
        yAxisIndex: index,
        xAxisIndex: index,
        markLine : {
            silent: true,
            symbol: ['',''],
            lineStyle:{
                color: COLOR.etg_markline,
                type: 'solid',
                width:1
            },
            zlevel: 1,
            animation:false,
            data : generateXaxisData(60, index, totalIndex, true)
        }
    },{
        name: 'thirty-'+ index,
        type: 'line',
        yAxisIndex: index,
        xAxisIndex: index,
        silent: true,
        markLine : {
            silent: true,
            symbol: ['',''],
            lineStyle:{
                color: COLOR.etg_markline,
                type: 'dashed',
                width:2,
                opacity: 0.5
            },
            zlevel: 1,
            animation:false,
            data : generateXaxisData(30, index, totalIndex, true)
        }
    }
    ]
}

function markLineLabel(index, totalIndex) {
    if(index === 0) {
        return {
            show: true,
            position: 'end',
            formatter: function(params) {
                var date = new Date(params.value);
                return date.getHours() + ':' + date.getMinutes();
            }
        }
    } else if (index === totalIndex) {
        return {
            show: true,
            position: 'start',
            formatter: function(params) {
                var date = new Date(params.value);
                return date.getHours() + ':' + date.getMinutes();
            }
        }
    } else {
        return {
            show: false
        }
    }
}

// function tripPTConfig(name, index, data) {
//     return {
//         name: name+'-pt-'+index,
//         type: 'line',
//         showSymbol: false,
//         hoverAnimation: false,
//         yAxisIndex: index,
//         xAxisIndex: index,
//         lineStyle:{
//             color: '#4283f4',
//             type: 'dot',
//             width:1,
//             opacity: 0.8
//         },
//         zlevel:1,
//         animation:false,
//         sampling:'average',
//         silent: true,
//         data: data
//     }
// }

function tripHtmlContent(data) {
    return '<div>' +
        '<h4>'+data.trip+'</h4>' +
        '<table>' +
        '    <tbody>' +
        '       <tr>' +
        '           <td class="text-warning">Last Update:</td>' +
        '           <td>' + data.lastUpdateTime +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">Delay:</td>' +
        '           <td>' + data.delay +' Sec</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">Status:</td>' +
        '           <td>' + data.status +'</td>' +
        '       </tr>' +
        '    </tbody>' +
        '</table>' +
        '</div>';
}

function getTripFromTripID(tripID) {
    return tripID.substring(0,tripID.indexOf("_"))
}

function getTripIDFromTripSeriesName(tripSeriesName) {
    console.log(tripSeriesName);
    return tripSeriesName.substring(0,tripSeriesName.indexOf("_") + 11)
}

function getPTFromTripSeriesName(tripSeriesName) {
    return tripSeriesName.substring(0,tripSeriesName.lastIndexOf("-")) + "-pt" + tripSeriesName.substring(tripSeriesName.lastIndexOf("-"))
}

function highLightTrip(etgChart, tripSeriesName) {

    etgChart.setOption({
        series: {
            name: tripSeriesName,
            lineStyle:{
                width:2
            },
        }
    });
}

function highLightPT(etgChart, tripSeriesName) {
    let ptSeriesName = getPTFromTripSeriesName(tripSeriesName);
    etgChart.setOption({
        series: {
            name: ptSeriesName,
            lineStyle:{
                width:2,
                color: '#ff0000',
                opacity: 1
            },
        }
    });
}

function removeHighLightTrip(etgChart, tripSeriesName) {
    etgChart.setOption({
        series: {
            name: tripSeriesName,
            lineStyle:{
                width:1,
                opacity: 1
            },
        }
    });
}

function removeHighLightPT(etgChart, tripSeriesName) {
    let ptSeriesName = getPTFromTripSeriesName(tripSeriesName);
    etgChart.setOption({
        series: {
            name: ptSeriesName,
            lineStyle:{
                width:1,
                opacity: 0.4,
                color: '#1B30EB'
            },
        }
    });
}

export {generateEtgConfig, generateETGMarkLineConfig, generatePTSeries, generateTripSeries, getTripFromTripID, highLightTrip, removeHighLightTrip, removeHighLightPT, highLightPT, getTripIDFromTripSeriesName}