import ECharts from 'vue-echarts'
import {groupByProperty} from './utils.js'
import * as COLOR from './colorConst.js'

let ptData = []; // this is original pt data
let controlPoints = {}; // this is saving the currently control points
let isDragging = false;
let currentDragId = null;

function initDragDrop(etgChart, data, selectedSector, endDrag, endDropOn) {
    controlPoints = {};
    ptData = data;
    let groupedByIndex = groupByProperty(ptData, 'index');
    let graphics = [];
    for (let index in groupedByIndex) {
        if (groupedByIndex.hasOwnProperty(index) && index == selectedSector) {
            groupedByIndex[index].filter(t => t.calls.length > 2)
                .forEach(trip => {
                    let ptSeriesIndex = findPTIndex(etgChart, trip.tripId +'-' + 0);

                    // generate drapdrop control node
                    graphics = graphics.concat(initTripDragDrop(etgChart,0, trip.tripId, trip.trip, trip.calls, ptSeriesIndex, endDrag, endDropOn));
                });
        }
    }
    etgChart.setOption({
        graphic:graphics
    })
}

function findPTIndex(etgChart, seriesName){
    let index = etgChart.getOption().series.findIndex(function(e){
        return e.name === seriesName;
    });
    return index;
}

function initTripDragDrop(etgChart, index, tripId, trip, data, ptSeriesIndex, endDrag, endDropOn) {
    let seriesName = tripId + '-pt-' + index;
    if(controlPoints[seriesName] === undefined) {
        controlPoints[seriesName] = [];
    }
    let graphic = data.map(function (item, dataIndex) {
        // remove the pass through node
        let pairNodeIndex = data.findIndex((e, index) => e[1] === item[1] && index !== dataIndex);
        if(pairNodeIndex<0 ) {
            controlPoints[seriesName].push(null);
            return null;
        } else if(pairNodeIndex>dataIndex) {
            // this node is arrival node
            let id = seriesName+"_"+dataIndex;
            controlPoints[seriesName].push({
                id: id,
                tripId:tripId,
                item: item
            });
            return {
                type: 'circle',
                id:id,
                position: etgChart.convertToPixel({gridIndex:index}, item),
                shape: {
                    cx: 0,
                    cy: 0,
                    r: 4
                },
                style: {
                    fill: '#0000ff',
                    stoke:'#0000ff',
                    lineWidth:1
                },
                info:{
                    trip: trip,
                    tripId:tripId,
                    dataIndex:dataIndex,
                    seriesName: seriesName
                },
                invisible: true,
                draggable: false,
                ondrop:echarts.util.curry(onDrop, etgChart, index, data, seriesName, id, dataIndex, endDropOn),
                onmouseover: echarts.util.curry(showTooltip, etgChart, ptSeriesIndex, seriesName, dataIndex, id),
                onmouseout: echarts.util.curry(hideTooltip, etgChart, seriesName, dataIndex, id),
                z: 200
            };
        } else{
            // this node is departure node
            let id = seriesName+"_"+dataIndex;
            let minItem = data[pairNodeIndex];
            controlPoints[seriesName].push({
                id: id,
                tripId:tripId,
                item: item
            });
            return {
                type: 'ring',
                id:id,
                position: etgChart.convertToPixel({gridIndex:index}, item),
                shape: {
                    cx: 0,
                    cy: 0,
                    r: 4,
                    r0:1
                },
                style: {
                    fill: '#ff0000',
                    stoke:'#ff0000',
                    lineWidth:1
                },
                info:{
                    trip: trip,
                    tripId:tripId,
                    dataIndex:dataIndex,
                    seriesName: seriesName
                },
                invisible: true,
                draggable: true,
                ondrag: echarts.util.curry(onPointDragging, etgChart,index, data, seriesName, id, dataIndex, minItem),
                ondragstart:echarts.util.curry(onPointDragStart, etgChart, index, data, seriesName, id, dataIndex),
                ondragend:echarts.util.curry(onPointDragEnd, etgChart, index, data, seriesName, id, dataIndex, endDrag),
                z: 200
            };
        }

    }).filter(n=>n);
    return graphic;
}

function updateControlPointPosition(etgChart, index) {
    let positions = [];
    Object.keys(controlPoints).forEach((key) => {
        controlPoints[key].filter(n=>n).forEach(point => {
            positions.push({
                id: point.id,
                position: etgChart.convertToPixel({gridIndex:index}, point.item)
            });
        })
    });
    etgChart.setOption({
        graphic: positions
    });
}

function onDrop(etgChart, index, data, seriesName, id, dataIndex, callback, event) {
    console.log("drop on " + id);
    let droppedNode = controlPoints[seriesName][dataIndex];
    let dragStartValue = droppedNode.item[1];
    let dropTimeValue = droppedNode.item[0];
    let dropTrip = droppedNode.tripId;
    let dragInfo = parseDragId(currentDragId);
    let draggedNode = controlPoints[dragInfo.seriesName][dragInfo.dataIndex];
    let dragStartTimeValue = draggedNode.item[0];
    let dragTrip = draggedNode.tripId;
    callback(dragStartValue, dragStartTimeValue, dragTrip, dropTimeValue, dropTrip);
    isDragging = false;
    etgChart.setOption({
        graphic:{
            id:id,
            invisible: true,
            style: {
                fill: '#ff0000',
                stoke:'#ff0000',
                lineWidth:1
            }
        }
    })
}

function onPointDragStart(etgChart, index, data, seriesName, id, dataIndex, callback, dx, dy){
    console.log("drag start on: " + id);
    currentDragId = id;
    isDragging = true;
}

function onPointDragEnd(etgChart, index, data, seriesName, id, dataIndex, callback, dx, dy) {
    if(id === currentDragId && isDragging == true) {
        // update control points
        data.forEach((item,i) => {
            if (controlPoints[seriesName][i] !== null) {
                controlPoints[seriesName][i].item = item;
            }
        });

        etgChart.setOption({
            graphic: controlPoints[seriesName].filter(n=>n).map(point => {
                return {
                    id: point.id,
                    position: etgChart.convertToPixel({gridIndex: index}, point.item)
                }
            })
        });
        let item = controlPoints[seriesName][dataIndex];
        callback(item.tripId, item.item[0], item.item[1]);
        isDragging = false;
        etgChart.setOption({
            graphic:{
                id:id,
                invisible: true,
                style: {
                    fill: '#ff0000',
                    stoke:'#ff0000',
                    lineWidth:1
                }
            }
        })
    }

}

function showTooltip(etgChart, ptSeriesIndex, seriesName, dataIndex, id) {
    etgChart.dispatchAction({
        type: 'showTip',
        seriesIndex: ptSeriesIndex,
        dataIndex: dataIndex
    });

    if(isDragging && currentDragId !== id) {
        console.log("mouse id: " + id);
        etgChart.setOption({
            graphic:{
                id:id,
                invisible: false,
                style: {
                    fill: '#ff0000',
                    stoke:'#00ff00',
                    lineWidth:1
                },
            },
            series:[{
                name:seriesName,
                lineStyle:{
                    opacity: 1,
                    color: COLOR.etg_trip_pt_changed
                },
            }]
        })
    }
}

function hideTooltip(etgChart, seriesName, dataIndex, id) {
    etgChart.dispatchAction({
        type: 'hideTip'
    });
    if(isDragging) {
        etgChart.setOption({
            graphic:{
                id:id,
                invisible: true
            },
            series:[{
                name:seriesName,
                lineStyle:{
                    opacity: 0.4,
                    color: COLOR.etg_trip_pt_normal
                },
            }]
        })
    }
}

function onPointDragging(etgChart,index, data, seriesName, id, dataIndex, minItem, dx, dy) {
    if(id === currentDragId) {
        let originalNode = data[dataIndex];
        let newPosition = etgChart.convertFromPixel({gridIndex:index}, this.position);
        let diff = newPosition[0]-originalNode[0];
        if (originalNode[0] + diff > minItem[0]) {
            for(let i = dataIndex; i< data.length; i++) {
                data[i] = [data[i][0]+diff, data[i][1]]
            }
            // Update data
            etgChart.setOption({
                series: [{
                    name: seriesName,
                    lineStyle:{
                        opacity: 1
                    },
                    data: data
                }]
            });
        }
    }
}

function parseDragId(id){
    let splitIndex = id.lastIndexOf('_');
    return {
        seriesName: id.substr(0, splitIndex),
        dataIndex: parseInt(id.substr(splitIndex+1))
    }
}

export {initDragDrop, updateControlPointPosition}