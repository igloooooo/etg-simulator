require('echarts');
require('echarts-gl');

import Vue from 'vue';
import VueRouter from 'vue-router'
import App from "./components/App.vue"
import Setting from './components/page/Setting.vue';
import dashboard from './components/page/dashboard.vue';
import PlayControl from './components/page/PlayControl.vue';
import TrainVisual from './components/page/TrainVisual.vue';
import TrainBorderVisual from './components/page/TrianBorderView.vue';
import TripView from './components/page/TripView.vue';
import NetworkView from './components/page/NetworkView.vue';
import Network3DView from './components/page/Network3DView.vue';
import PathPlanView from './components/page/PathPlanView.vue';
import ECharts from 'vue-echarts'
import VueNativeSock from 'vue-native-websocket'
import * as uiv from 'uiv'
import store from "./store"
import ncharts from 'echarts'
import AUSMap from './map/aus-map.json'
// import AUSFullMap from './map/aus-full-map.json'

Vue.prototype.$ncharts = ncharts;
echarts = ncharts;
Vue.use(VueRouter);
Vue.use(require('vue-moment'));
Vue.component('chart', ECharts);
Vue.use(uiv, {prefix: 'uiv'});
const wsHost = "ws://" + ETG.ws.host + ":" + ETG.ws.port + "/ws";
Vue.use(VueNativeSock, wsHost, {
    format: 'json',
    reconnection: true, // (Boolean) whether to reconnect automatically (false)
    reconnectionAttempts: 5, // (Number) number of reconnection attempts before giving up (Infinity),
    reconnectionDelay: 3000, // (Number) how long to initially wait before attempting a new (1000)
});

ECharts.registerMap('network', {"features" :[{
    "type": "Feature",
    "geometry": {
        "type": "Polygon",
        "coordinates": [
            [ [300.0, 100.0], [1100.0, 100.0], [1100.0, 700.0], [300.2, 700.0]]
        ]
    },
    "properties": {
        "name": "Network"
    }
}]});
ECharts.registerMap('AUS', AUSMap);

const moment = require('moment');

const router = new VueRouter({
    routes: [{
        path: '/',
        name: 'dashboard',
        component: dashboard
    },{
        path: '/playcontrol',
        name: 'playcontrol',
        component: PlayControl
    },{
        path: '/setting',
        name: 'setting',
        component: Setting
    },{
        path: '/etg/:borderId',
        name: 'etg',
        component: TrainVisual
    },{
        path: '/tripView/:tripId',
        name: 'tripView',
        component: TripView
    },{
        path: '/etgBorderView/:trip',
        name: 'etgBorderView',
        component: TrainBorderVisual
    },{
        path: '/network',
        name: 'network',
        component: NetworkView
    },{
        path: '/pathPlan/:tripId',
        name: 'pathPlan',
        component: PathPlanView
    },{
        path: '/network3d',
        name: 'network3d',
        component: Network3DView
    }],
    linkExactActiveClass: "router-link-exact-active active"
// .. others
});


const app = new Vue({
    router,
    render: createEle => createEle(App),
    data: store,
    mounted () {
        this.messageHandler()
    },
    methods: {
        messageHandler: function (val) {
            this.$options.sockets.onmessage = (data) => {
                let json = JSON.parse(data.data);
                // console.log(json);
                if (json.eventType == 'PlaybackStatusEvent') {
                    this.state.playTime = moment(json.playTime);
                    this.state.startTime = moment(json.startTime);
                    this.state.endTime = moment(json.endTime);
                    this.state.speed = json.speed;
                    this.state.playStatus = json.playStatus;
                    if (this.state.playStatus === 'Play' || this.state.playStatus === 'Pause') {
                        this.state.isPlay = true;
                    }

                } else if (json.eventType == 'ChannelConfigStatus') {
                    if (this.state.channelConfig[json.name] === undefined) {
                        // console.log(this.state);
                    } else {
                        this.state.channelConfig[json.name].dataSetName = json.dataSetName;
                        this.state.channelConfig[json.name].isLoad = json.isLoad;
                        this.state.channelConfig[json.name].totalCount = json.totalCount;
                        this.state.channelConfig[json.name].sentCount = json.sentCount;
                    }

                }

            }
        }
    }
}).$mount('#app');


import '../sass/style.scss';