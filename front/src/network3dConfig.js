export function network3dInitConfig() {
    return {
        mapbox3D: {
            center: [151.202855, -33.8688],
                zoom: 11,
                pitch: 60,
                bearing: -10,
                style: 'mapbox://styles/mapbox/dark-v9',
                postEffect: {
                enable: true,
                    screenSpaceAmbientOcclusion: {
                    enable: true,
                        intensity: 1,
                        radius: 3,
                        quality: 'high'
                }
            },
            light: {
                main: {
                    intensity: 1,
                        shadow: true,
                        shadowQuality: 'high'
                },
                ambient: {
                    intensity: 0.
                },
                ambientCubemap: {
                    texture: 'asset/canyon.hdr',
                        exposure: 2,
                        diffuseIntensity: 0.5
                }
            }
        },
        series: [{
            name:'stopRef',
            type: 'scatter3D',
            coordinateSystem: 'mapbox3D',
            symbol: 'pin',
            symbolSize: 5,
            opacity: 1,
            label:{
                show:false
            },
            emphasis:{label:{show:false}},
            data: []
        },{
            type: 'lines3D',
            name: 'actualMovement',
            coordinateSystem: 'mapbox3D',
            zlevel: 100,
            effect: {
                show: true,
                constantSpeed: 2,
                trailWidth: 2,
                trailLength: 0.4,
                trailOpacity: 1,
                spotIntensity: 10
            },
            label:{
                show:true,
                formatter: 'trip'
            },
            blendMode: 'lighter',

            lineStyle: {
                width: 0.1,
                color: 'rgb(200, 40, 0)',
                opacity: 0.
            },

            data: []
        }]
    }
}

export function generateVMNetwork3d(data) {
    let vms = data.map(item => {
        return {
            coords: [ [item.fromLon, item.fromLat, 2], [item.toLon, item.toLat, 2] ],
            // 数据值
            value: item.delay,
            // 数据名
            name: item.trip
        }
    });

    return vms
}

export function generateStopRefLayer(data) {
    let stopList = data.map(item => {
        return {
            "type": "Feature",
            "properties": {
                "description": item.nodeName,
                "title":item.nodeName,
                "icon": "marker"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [item.longitude, item.latitude]
            }
        }
    });
    return {
        "id": "stopRef",
        "type": "symbol",
        "source": {
        "type": "geojson",
            "data": {
            "type": "FeatureCollection",
                "features": stopList
            }
        },
        "layout": {
            "icon-image": "{icon}-15",
            "icon-allow-overlap": false,
            "text-field": "{title}",
            "text-size": 10,
            "visibility": "none"
        },
        paint: {
            "text-color": "#46bee9"
        }
    }
}