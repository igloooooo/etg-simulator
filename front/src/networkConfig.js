const network_backgroup = '#404a59';
const network_node_color = '#46bee9';
const network_platform_label_color = '#ff00ff';
const network_trip_warning = '#ef7809';
const network_trip_normal = '#06c930';
const network_trip_label_normal = '#fcfc0c';
const network_trip_alert = '#ff0000';
const network_trip_faster = '#ff00ff';
const network_trip_highlight = '#eef442';
const axios = require('axios');

function getNetworkChartConfig(tripNodeTipCallBack) {
    let networkOption = {
        backgroundColor: '#404a59',
        title: {
            text: 'Train Network Map',
            left: 'center',
            textStyle: {
                color: '#fff'
            }
        },
        tooltip : {
            trigger: 'item'
        },
        geo: {
            map: 'network',
            label: {
                emphasis: {
                    show: false
                }
            },
            zoom: 4,
            roam: true,
            itemStyle: {
                normal: {
                    areaColor: network_backgroup,
                    borderColor: network_backgroup
                },
                emphasis: {
                    areaColor: network_backgroup,
                    show: false
                },
                opacity: 0
            },
            silent: true
        },
        series : [
            {
                name: 'actualMovement',
                type: 'lines',
                polyline: false,
                coordinateSystem: 'geo',
                effect: {
                    show: true,
                    constantSpeed: 3,
                    showSymbol: false,
                    symbol: 'arrow',
                    symbolSize: 8,
                    trailLength: 0
                },
                animation: false,
                zlevel: 1,
                tooltip: {
                    position: 'top',
                    formatter: function (params, ticket, callback) {
                        return tripMovementHtmlContent(params.data);
                    },
                },
                data: []
            },
            {
                name: 'platformLabel',
                type: 'scatter',
                coordinateSystem: 'geo',
                label: {
                    color: network_platform_label_color
                },
                itemStyle: {
                    normal: {
                        color: network_platform_label_color
                    }
                },
                animation: false,
                symbolSize: 1,
                data: []
            },
            {
                name: 'node',
                type: 'scatter',
                coordinateSystem: 'geo',
                label: {
                    formatter: '{b}',
                    position: 'top',
                    show: false,
                    opacity: 0.5,
                    fontSize: 8
                },
                itemStyle: {
                    normal: {
                        color: network_node_color
                    }
                },
                animation: false,
                symbolSize: 5,
                symbol: 'rect',
                tooltip: {
                    position: 'top',
                    formatter: function (params, ticket, callback) {
                        axios.get('/api/v1/network/node/' + params.data.name)
                            .then((response) => {
                                if(response.data !== undefined) {
                                    callback(ticket, nodeHtmlContent(response.data));
                                }
                            });
                        return 'Loading...';
                    },
                },
                data: []
            },
            {
                name: 'conflictNode',
                type: 'effectScatter',
                coordinateSystem: 'geo',
                label: {
                    show: false
                },
                symbolSize: 20,
                showEffectOn: 'render',
                rippleEffect: {
                    brushType: 'stroke'
                },
                itemStyle: {
                    normal: {
                        color: 'red',
                        shadowBlur: 10,
                        shadowColor: '#333'
                    }
                },
                zlevel: 4,
                data: []
            },
            {
                name: 'blockedNode',
                type: 'effectScatter',
                coordinateSystem: 'geo',
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: false
                    },
                    emphasis: {
                        show: true
                    }
                },
                symbolSize: 10,
                showEffectOn: 'render',
                rippleEffect: {
                    brushType: 'stroke'
                },
                itemStyle: {
                    normal: {
                        color: network_trip_warning,
                        shadowBlur: 10,
                        shadowColor: '#FFFFFF'
                    }
                },
                zlevel: 4,
                data: []
            },
            {
                name: 'junctionNode',
                type: 'scatter',
                coordinateSystem: 'geo',
                label: {
                    formatter: '{b}',
                    position: 'bottom',
                    show: false,
                    color: 'black'
                },
                itemStyle: {
                    normal: {
                        color: 'black'
                    }
                },
                animation: false,
                symbol: 'none',
                data: []
            },
            {
                name: 'link',
                type: 'lines',
                coordinateSystem: 'geo',
                polyline: true,
                lineStyle: {
                    normal: {
                        color:'#ffffff',
                        width: 1,
                        opacity: 0.6
                    }
                },
                silent: true,
                data: []
            },
            {
                name: 'highlightlink',
                type: 'lines',
                polyline: true,
                coordinateSystem: 'geo',
                animation: false,
                lineStyle: {
                    color:'#eef442',
                    width: 2,
                    type: 'dashed',
                    opacity: 1
                },
                symbol: 'arrow',
                symbolSize: 10,
                tooltip: {
                    position: 'top',
                    formatter: function (params, ticket, callback) {
                        console.log(params);
                        if(tripNodeTipCallBack !== undefined) {
                            let item = tripNodeTipCallBack();
                            if(item !== null)
                                return tripNodeHtmlContent(item);
                            else
                                return ""
                        } else
                            return "...";
                    },
                },
                animation: false,
                z: 10,
                data: []
            }
        ]
    };
    return networkOption;
}

function convertToNetworkLink(links) {
    var linkList = [];
    var linkLen = links.length;
    var count = 0;
    for(var i=0; i<linkLen; i++) {
        var vertices = links[i].vertices;
        count += vertices.length;
        if(vertices !== undefined && vertices.length > 0) {
            var segment = vertices.map(item => {
                return [item.LocX, item.LocY];
            });
            linkList.push({
                coords: segment
            });

        } else {

        }

    }
    console.log('total count: ' + count);
    return linkList;
}

function generateConflictNodes(json) {
    return json.map(node => {
        return {
            name: node.stopRef,
            value: [node.locX, node.locY],
            tooltip:{
                formatter:conflictNodeHtmlContent(node)
            }
        };
    })
}

function generateBlockNodes(json) {
    return json.map(node => {
        return {
            name: node.stopRef,
            value: [node.locX, node.locY],
            tooltip:{
                formatter:function (e) {
                    return "Node " + node.stopRef + " has been blocked."
                }
            }
        };
    })
}

function generateVMNetwork(json) {
    var trip = json.trip;
    var linkList = [];
    var vertices = json.vertices;
    var color = network_trip_normal;
    if(Math.abs(json.delay)<=5) {
        color = network_trip_normal;
    } else if(json.delay < -5) {
        color = network_trip_faster;
    } else if(json.delay > 180) {
        color = network_trip_warning;
    } else {
        color = network_trip_alert;
    }
    if(vertices !== undefined && vertices.length > 0) {
        if (vertices.length > 2) {
            for(let j = 0; j< vertices.length -2; j ++) {
                linkList.push({
                    fromName: json.fromNode + j,
                    toName: json.toNode + j,
                    coords: [[vertices[j].LocX, vertices[j].LocY], [vertices[j+1].LocX, vertices[j+1].LocY]],
                    label: {
                        show: false
                    },
                    lineStyle: {
                        width: 2,
                        color: network_trip_normal
                    },
                    trip: trip,
                    delay:json.delay
                });
            }
            let lastIndex = vertices.length - 1;
            linkList.push({
                fromName: json.fromNode + lastIndex,
                toName: json.toNode + lastIndex,
                coords: [[vertices[lastIndex-1].LocX, vertices[lastIndex-1].LocY], [vertices[lastIndex-1].LocX, vertices[lastIndex-1].LocY]],
                label: {
                    formatter: generateTripLabel(trip, json.delay),
                    position: 'middle',
                    show: true,
                    rich: {
                        a: {
                            color: network_trip_alert,
                        },
                        n: {
                            color: network_trip_label_normal,
                        }
                    }
                },
                lineStyle: {
                    width: 2,
                    color: network_trip_normal
                },
                trip: trip,
                delay:json.delay
            });
        } else {
            for(let j = 0; j< vertices.length -1; j ++) {
                linkList.push({
                    fromName: json.fromNode + j,
                    toName: json.toNode + j,
                    coords: [[vertices[j].LocX, vertices[j].LocY], [vertices[j+1].LocX, vertices[j+1].LocY]],
                    label: {
                        formatter: generateTripLabel(trip, json.delay),
                        position: 'middle',
                        show: true,
                        rich: {
                            a: {
                                color: network_trip_alert,
                            },
                            n: {
                                color: network_trip_label_normal    ,
                            }
                        }
                    },
                    lineStyle: {
                        width: 2,
                        color: network_trip_normal
                    },
                    trip: trip,
                    delay:json.delay
                });
            }
        }
    }

    return linkList;
}

function generateTripLabel(trip, delay) {
    if(Math.abs(delay)<=180) {
        return '{n|'+trip+'}';
    } else if(delay > 180 && delay <=360) {
        // first char
        return '{a|'+trip.substring(0,1)+'}'+'{n|'+trip.substring(1)+'}';
    } else if(delay > 360 && delay <=540){
        return '{a|'+trip.substring(0,2)+'}'+'{n|'+trip.substring(2)+'}';
    } else if(delay > 540){
        return '{a|'+trip+'}';
    } else {
        return '{n|'+trip+'}';
    }
}
function nodeHtmlContent(data) {
    return '<div>' +
        '<h4>'+data.NodeID+'</h4>' +
        '<p>'+data.NodeDescription+'</p>' +
        '<table>' +
        '    <tbody>' +
        '       <tr>' +
        '           <td class="text-warning">NetworkLevel:</td>' +
        '           <td>' + data.NetworkLevel +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">ParentNodeID:</td>' +
        '           <td>' + data.ParentNodeID +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">KMPost:</td>' +
        '           <td>' + data.KMPost +'</td>' +
        '       </tr>' +
        '    </tbody>' +
        '</table>' +
        '</div>';
}

function tripMovementHtmlContent(data) {
    return '<div>' +
        '<h4>'+data.trip+'</h4>' +
        '<table>' +
        '    <tbody>' +
        '       <tr>' +
        '           <td class="text-warning">From:</td>' +
        '           <td>' + data.fromName +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">To:</td>' +
        '           <td>' + data.toName +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">Delay:</td>' +
        '           <td>' + data.delay +' Sec</td>' +
        '       </tr>' +
        '    </tbody>' +
        '</table>' +
        '</div>';
}

function tripNodeHtmlContent(data) {
    return '<div>' +
        '<h4>'+data.trip+'</h4>' +
        '<table>' +
        '    <tbody>' +
        '       <tr>' +
        '           <td class="text-warning">StopRef:</td>' +
        '           <td>' + data.stopRef +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">Arrival:</td>' +
        '           <td>' + data.arrivalToNode +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">Departure:</td>' +
        '           <td>' + data.departureFromNode +'</td>' +
        '       </tr>' +
        '    </tbody>' +
        '</table>' +
        '</div>';
}

function conflictNodeHtmlContent(data) {
    return '<div>' +
        '<h4>'+data.stopRef+'</h4>' +
        '<table>' +
        '   <thead>' +
        '       <tr>' +
        '           <th>Trigger Trip</th>' +
        '           <th>Conflict Trip</th>' +
        '       </tr>' +
        '   </thead>' +
        '    <tbody>' +
                conflictItem(data.conflictedList) +
        '    </tbody>' +
        '</table>' +
        '</div>';
}

function conflictItem(conflicts) {
    if(conflicts !== undefined) {
        return conflicts.map(item => {
            return '       <tr>' +
                '           <td class="text-warning">' + item.impactedTrip + '</td>' +
                '           <td>' + item.triggeredTrip +'</td>' +
                '       </tr>'
        }).join(' ');
    } else {
        return '';
    }

}

export {getNetworkChartConfig, convertToNetworkLink, generateVMNetwork, generateConflictNodes, generateBlockNodes}