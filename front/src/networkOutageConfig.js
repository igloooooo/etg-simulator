import ECharts from 'vue-echarts'

function networkOutageRender(params, api) {
    var start = api.coord([api.value(0), api.value(1)]);
    var end = api.coord([api.value(2), api.value(3)]);
    return {
        type: 'rect',
        shape: ECharts.graphic.clipRectByRect({
            x: start[0],
            y: start[1],
            width: Math.abs(end[0] - start[0]),
            height: Math.abs(end[1] - start[1]) <= 0 ? 1 : Math.abs(end[1] - start[1])
        }, {
            x: params.coordSys.x,
            y: params.coordSys.y,
            width: params.coordSys.width,
            height: params.coordSys.height
        }),
        style: api.style()
    };
}

function generateOutageData(outage) {
    return {
        name:outage.name,
        value:[outage.startX, outage.startY, outage.endX, outage.endY, outage.name, outage.description],
        itemStyle: {
            normal: {
                color: '#ff0000',
                borderColor: '#ff0000',
                borderWidth: 1,
                opacity: 0.2
            }
        }
    }
}

export {networkOutageRender, generateOutageData}