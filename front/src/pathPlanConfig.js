import {updateSeriesData} from './utils.js'
let pathControlPoints = [];
let pathDropPoints = [];
let currentPathDrag = {};
let currentMouseOverPoint = {};
function initPathPlanControl(etgChart, data, seriesIndex, onDragEnd, onDropEnd, updateMouseOverTripNode) {
    removeAllControlPoints(etgChart);
    let dragPoints = data.map((item, index) => {
        if (item.isVertices || item.availableLinks.length < 0) {
            pathControlPoints.push(null);
            return null;
        } else {
            let id = item.trip + "_" + index;
            let controlPoint =  {
                type: 'ring',
                id:id,
                position: etgChart.convertToPixel({geoIndex: 0}, [item.locX, item.locY]),
                shape: {
                    cx: 0,
                    cy: 0,
                    r: 5,
                    r0:1
                },
                style: {
                    fill: '#19bf03',
                    stoke:'#19bf03',
                    lineWidth:1
                },
                info:{
                    trip: item.trip
                },
                invisible: false,
                draggable: true,
                ondrag: echarts.util.curry(onPointDragging, etgChart, id, index, seriesIndex, item.stopRef),
                ondragstart:echarts.util.curry(onPointDragStart, etgChart, id, index, seriesIndex, item.stopRef),
                ondragend:echarts.util.curry(onPointDragEnd, etgChart, id, index, seriesIndex, item.stopRef, onDragEnd),
                onmouseover: echarts.util.curry(overTripNode, etgChart, id, index, seriesIndex,item.stopRef, updateMouseOverTripNode),
                onmouseout: echarts.util.curry(outTripNode, etgChart, id, index, seriesIndex,item.stopRef),
                z: 200
            };
            pathControlPoints.push({
                id: id,
                item: item
            });
            return controlPoint;
        }

    }).filter(n=>n);

    // init drop points
    let dropPoints = [];
    data.forEach((item, index) => {
        if (!item.isVertices && item.availableLinks.length > 0) {
            item.availableLinks.forEach((link, linkIndex) => {
                let id = item.trip + "_" + index;
                let linkId = id + "_" + linkIndex;
                pathDropPoints.push({
                    id:linkId,
                    item:link
                });
                dropPoints.push({
                    type: 'ring',
                    id:linkId,
                    position: etgChart.convertToPixel({geoIndex: 0}, [link.toLocX, link.toLocY]),
                    shape: {
                        cx: 0,
                        cy: 0,
                        r: 4,
                        r0:1
                    },
                    style: {
                        fill: '#ef7809',
                        stoke:'#ef7809',
                        lineWidth:1
                    },
                    info:{
                        trip: item.trip
                    },
                    invisible: false,
                    draggable: false,
                    ondrop:echarts.util.curry(onDrop, etgChart, id, linkId, linkIndex, link.toNode, onDropEnd),
                    onmousemove: echarts.util.curry(moveAvailableNode, etgChart, id, linkId, linkIndex,link.toNode),
                    onmouseout: echarts.util.curry(outAvailableNode, etgChart, id, linkId, linkIndex,link.toNode),
                    z: 200
                });
            })
        }
    });

    etgChart.setOption({
        graphic:dragPoints.concat(dropPoints),
        series:etgChart.getOption().series
    })
}
function overTripNode(etgChart, id, index, seriesIndex, stopRef, updateMouseOverTripNode, event) {
    console.log(seriesIndex + ":" +index);
    updateMouseOverTripNode(pathControlPoints[index]);
    etgChart.dispatchAction({
        type: 'showTip',
        seriesIndex: seriesIndex,
        dataIndex: 0,
        info:"tr",
        position:[event.offsetX, event.offsetY]
    });
}

function outTripNode(etgChart, id, index, seriesIndex, stopRef, dx, dy) {
    currentMouseOverPoint = {};
    // etgChart.dispatchAction({
    //     type: 'hideTip',
    //     seriesIndex: seriesIndex
    // });
}

function removeAllControlPoints(etgChart){
    if(pathControlPoints.length > 0) {
        let allControlPoints = pathControlPoints.filter(n=>n).concat(pathDropPoints).map(item => {
            return {
                id: item.id,
                $action: 'remove'
            }
        });
        etgChart.setOption({
            graphic:allControlPoints,
            series:etgChart.getOption().series
        })
    }
    pathControlPoints = [];
    currentPathDrag = {};
    pathDropPoints = [];
}

function moveAvailableNode(etgChart, id, linkId, linkIndex, stopRef, event) {
    if(currentPathDrag.id === id) {
        etgChart.setOption({
            graphic:{
                id: linkId,
                shape: {
                    cx: 0,
                    cy: 0,
                    r: 6,
                    r0:1
                },
            },
            series:etgChart.getOption().series
        })

    }
}

function outAvailableNode(etgChart, id, linkId, linkIndex, stopRef, event) {
    if(currentPathDrag.id === id) {
        etgChart.setOption({
            graphic:{
                id: linkId,
                shape: {
                    cx: 0,
                    cy: 0,
                    r: 4,
                    r0:1
                },
            },
            series:etgChart.getOption().series
        })
    }
}

function onDrop(etgChart, id, linkId, linkIndex, stopRef, endDropOn, event) {
    if(currentPathDrag.id === id) {
        let droppedStopRef = stopRef;
        let draggedStopRef = currentPathDrag.stopRef;
        endDropOn(draggedStopRef, droppedStopRef);
    }
}

function updateControlPosition(etgChart, seriesIndex) {
    let dragPositions = [];
    let dropPositions = [];
    pathControlPoints.filter(n=>n).forEach((item) => {
        dragPositions.push({
            id: item.id,
            position: etgChart.convertToPixel({seriesIndex: seriesIndex}, [item.item.locX, item.item.locY])
        });
    });
    pathDropPoints.forEach(item => {
        dropPositions.push({
            id: item.id,
            position: etgChart.convertToPixel({seriesIndex: seriesIndex}, [item.item.toLocX, item.item.toLocY])
        });
    });
    etgChart.setOption({
        graphic: dragPositions.concat(dropPositions),
        series:etgChart.getOption().series
    });
}

function generatePathData(links){
    let pathData = links.map(item => {
        return [item.locX, item.locY];
    });
    return {
        coords: pathData,
        symbol: 'arrow',
        symbolSize: 10
    };
}

function onPointDragging(etgChart, id, index, seriesIndex, stopRef, dx, dy) {
    console.log("dragging");
    let newPosition = etgChart.convertFromPixel({seriesIndex:seriesIndex}, this.position);
    let oldPathData = etgChart.getOption().series[seriesIndex].data;
    oldPathData[0].coords[index] = newPosition;
    console.log(newPosition);
    etgChart.setOption({
        series: updateSeriesData(etgChart.getOption().series, 'highlightlink', oldPathData)
    });
}

function onPointDragStart(etgChart, id, index, seriesIndex, stopRef, dx, dy) {
    console.log("dragging start");
    currentPathDrag = {
        id: id,
        stopRef: stopRef
    };
}

function onPointDragEnd(etgChart, id, index, seriesIndex, stopRef, onDragEnd, dx, dy ) {
    console.log("dragging end");
    let isDropped = onDragEnd();
    if(!isDropped) {
        // roll back the drag
        let oldControlPoint = pathControlPoints[index];
        let position = etgChart.convertToPixel({seriesIndex: seriesIndex}, [oldControlPoint.item.locX, oldControlPoint.item.locY]);
        let oldPathData = etgChart.getOption().series[seriesIndex].data;
        oldPathData[0].coords[index] = [oldControlPoint.item.locX, oldControlPoint.item.locY];
        etgChart.setOption({
            graphic:{
                id:oldControlPoint.id,
                position: position
            },
            series: updateSeriesData(etgChart.getOption().series, 'highlightlink', oldPathData)
        });
    }
}

export {initPathPlanControl, generatePathData, updateControlPosition}