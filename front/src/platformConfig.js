import {getYesterdaysDate, getTomorrowdaysDate, generateXaxisData} from "./utils.js";
import platformRenderItem from "./render/platformRender.js"
const moment = require('moment');

function generateInitPlatformConfig(stopRef, platformLane, data){
    let platformData = convertToPlatformData(data);
    let categories = platformLane.map(item => item.stopRef);
    return {
        tooltip: {
            formatter: function (params) {
                return params.marker + params.name + ': ' + params.value[3] + ' ms';
            }
        },
        title: {
            text: stopRef,
            left: 'center'
        },
        grid: {
            left: 120,
            right: 20
        },
        xAxis: {
            type: 'time',
            show: false,
            min:getYesterdaysDate(),
            max:getTomorrowdaysDate(),
            axisTick:{
                show: false
            },
            axisLabel:{
                show: true
            },
            splitLine: {
                show: false
            }
        },
        yAxis: {
            data: categories,
            axisLabel: {
                color: '#42a7f4',
                formatter: function (value, index) {
                    return platformLane[index].longName;
                }
            }
        },
        dataZoom: [
            {
                type: 'inside',
                start: 40,
                end: 55
            }
        ],
        series: [{
            name: 'platform',
            type: 'custom',
            renderItem: platformRenderItem,
            itemStyle: {
                normal: {
                    color: '#06a342',
                    borderColor: '#000000',
                    borderWidth: 1,
                    opacity: 0.8
                }
            },
            encode: {
                x: [1, 2, 4],
                y: 0
            },
            tooltip: {
                position: 'top',
                formatter: function (params, ticket, callback) {
                    return platFormHtmlContent(params.data.value);
                },
            },
            data: platformData
        },{
            name: 'ten-0',
            type: 'line',
            silent: true,
            markLine : {
                silent: true,
                symbol: ['',''],
                label: {
                    show:false
                },
                lineStyle:{
                    color: '#42a7f4',
                    type: 'dashed',
                    width:1,
                    opacity: 0.5
                },
                zlevel: 1,
                animation:false,
                data : generateXaxisData(10, 0, 0, false)
            }
        },{
            name: 'sixty-0',
            type: 'line',
            silent: true,
            markLine : {
                silent: true,
                symbol: ['',''],
                lineStyle:{
                    color: '#42a7f4',
                    type: 'solid',
                    width:1
                },
                zlevel: 1,
                animation:false,
                data : generateXaxisData(60, 0, 0, true)
            }
        },{
            name: 'thirty-0',
            type: 'line',
            silent: true,
            markLine : {
                silent: true,
                symbol: ['',''],
                lineStyle:{
                    color: '#42a7f4',
                    type: 'dashed',
                    width:2,
                    opacity: 0.5
                },
                zlevel: 1,
                animation:false,
                data : generateXaxisData(30, 0, 0, true)
            }
        }]
    }
}

function convertToPlatformData(data) {
    let platFormData = data.map((item) => {
        return {
            value:[
                item.stopRef,
                item.startTime,
                item.endTime,
                item.duration,
                item.trip,
                item.isActual],
            itemStyle: {
                normal: {
                    color: item.isActual ? '#06a342' : '#8f9399',
                    borderColor: '#000000',
                    borderWidth: 1,
                    opacity: 0.8
                }
            },};
    });
    return platFormData;
}

function onPointDragStart(etgChart, index, dx, dy) {
    data[index].itemStyle = {
        normal: {
            color: '#ff00ff'
        }
    };
    etgChart.setOption({
        graphic:{
            id:index,
            invisible: false
        },
        series: {
            name: 'try',
            data: data
        }
    });
}
function onPointDragging(etgChart, id, dx, dy) {
    var newPosition = etgChart.convertFromPixel({seriesIndex: 0}, this.position);
    console.log(newPosition);
}
function onPointDragEnd(etgChart, index, dx, dy) {
    console.log('end drag');
    var newPosition = etgChart.convertFromPixel({seriesIndex: 0}, this.position);
    console.log(this.position);
    etgChart.setOption({
        graphic:{
            id:index,
            invisible: true
        },
        series: {
            name: 'try',
            data: data
        }
    });
}

function platFormHtmlContent(data) {
    return '<div>' +
        '<h4>'+data[0]+'</h4>' +
        '<table>' +
        '    <tbody>' +
        '       <tr>' +
        '           <td class="text-warning">Trip:</td>' +
        '           <td>' + data[4] +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">Arrival:</td>' +
        '           <td>' + moment(data[1]).format('dddd, MMMM Do YYYY, h:mm:ss a') +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">Departure:</td>' +
        '           <td>' + moment(data[2]).format('dddd, MMMM Do YYYY, h:mm:ss a') +'</td>' +
        '       </tr>' +
        '       <tr>' +
        '           <td class="text-warning">Duration:</td>' +
        '           <td>' + data[3]/1000 +' Sec</td>' +
        '       </tr>' +
        '    </tbody>' +
        '</table>' +
        '</div>'
}

export {generateInitPlatformConfig}