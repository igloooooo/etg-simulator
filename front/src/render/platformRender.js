import ECharts from 'vue-echarts'
export default function platformRenderItem(params, api) {
    var categoryIndex = api.value(0);
    var start = api.coord([api.value(1), categoryIndex]);
    var end = api.coord([api.value(2), categoryIndex]);
    var overlap = api.coord([api.value(4), categoryIndex]);
    var height = api.size([0, 1])[1] * 0.6;
    console.log(api);
    return {
        type: 'group',
        draggable: true,
        children: [
            {
                type: 'rect',
                shape: ECharts.graphic.clipRectByRect({
                    x: start[0],
                    y: start[1] - height / 2,
                    width: end[0] - start[0],
                    height: height
                }, {
                    x: params.coordSys.x,
                    y: params.coordSys.y,
                    width: params.coordSys.width,
                    height: params.coordSys.height
                }),
                draggable: true,
                style: api.style()
            }
        ]
    };
}