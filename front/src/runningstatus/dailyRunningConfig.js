import {getYesterdaysDate, getTomorrowdaysDate} from "../utils.js";
export function dailyRunningInitConfig() {
    let option = {
        title : {
            text: 'Daily Running',
            subtext: 'Data coming from Playback',
            x: 'center',
            align: 'right',
            textStyle: {
                color: '#eee'
            }
        },
        grid: {
            bottom: 80,
            show:false
        },
        toolbox: {
            // feature: {
            //     dataZoom: {
            //         yAxisIndex: 'none'
            //     },
            //     restore: {},
            //     saveAsImage: {}
            // }
        },
        tooltip : {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                animation: false,
                label: {
                    backgroundColor: '#505765'
                }
            }
        },
        legend: {
            data:['Trips','Train Actual'],
            x: 'left'
        },
        dataZoom: [
            {
                type: 'inside',
                realtime: true
            }
        ],
        xAxis : [
            {
                type: 'time',
                min:getYesterdaysDate(),
                max:getTomorrowdaysDate(),
                boundaryGap : false,
                axisLine: {onZero: false},
                axisLabel:{
                    color:'#eee'
                }
            }
        ],
        yAxis: [
            {
                name: 'Train Actual(k)',
                type: 'value',
                max: 800,
                axisLabel:{
                    color:'#eee'
                },
                splitLine: {
                    show: false
                }
            },
            {
                name: 'Trip(#)',
                nameLocation: 'start',
                type: 'value',
                max: 300,
                axisLabel:{
                    color:'#eee'
                },
                splitLine: {
                    show: false
                },
                inverse: true
            }
        ],
        series: [
            {
                name:'Train Actual',
                type:'line',
                animation: false,
                showSymbol:false,
                areaStyle: {
                    normal: {}
                },
                lineStyle: {
                    normal: {
                        width: 1,
                        color: '#ff0000'
                    }
                },
                markArea: {
                    silent: true,
                    data: []
                },
                data:[]
            },
            {
                name:'Trips',
                type:'line',
                yAxisIndex:1,
                animation: false,
                showSymbol:false,
                areaStyle: {
                    normal: {}
                },
                lineStyle: {
                    normal: {
                        width: 1,
                        color: '#577ceb'
                    }
                },
                data: []
            }
        ]
    };
    return option;
};
