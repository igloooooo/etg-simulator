import ECharts from 'vue-echarts'

let delay_color = '#EFF923';
let skipStop_color = '#F68989';

export function generateDelayTrips(data) {
    let mapData = data.map(item => {
        return [
            item.lon,
            item.lat,
            item.tripId,
            item.trip,
            item.stopRef
        ]
    });
    return mapData;
};

export function generateSkipStops(data) {
    let mapData = data.map(item => {
        return [
            item.lon,
            item.lat,
            item.tripId,
            item.trip,
            item.stopRef
        ]
    });
    return mapData;
    return mapData;
};

function generateInitConfig() {
    let option = {
        backgroundColor: new ECharts.graphic.RadialGradient(0.5, 0.5, 0.4, [{
            offset: 0,
            color: '#4b5769'
        }, {
            offset: 1,
            color: '#404a59'
        }]),
        title: {
            text: 'Sydney Train Running',
            subtext: 'data from Playback',
            sublink: '/#/setting',
            left: 'center',
            top: 5,
            itemGap: 0,
            textStyle: {
                color: '#eee'
            },
            z: 200
        },
        tooltip: {
            trigger: 'item',
            formatter: function (params) {
                var value = (params.value + '').split('.');
                value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,') + '.' + value[1];
                return params.seriesName + '<br/>' + params.name + ' : ' + value;
            }
        },
        // toolbox: {
        //     show: true,
        //     left: 'right',
        //     iconStyle: {
        //         normal: {
        //             borderColor: '#ddd'
        //         }
        //     },
        //     feature: {
        //     },
        //     z: 202
        // },
        geo: {
            map: 'AUS',
            silent: true,
            regions: [{
                name: 'New South Wales',
                itemStyle: {
                    areaColor: '#eee',
                    color: 'red'
                }
            }],
            center: [151.005020, -33.808250],
            zoom:19,
            itemStyle: {
                normal: {
                    borderWidth: 0.2,
                    borderColor: '#4b5769',
                    areaColor: '#eee'
                }
            },
            top: 100,
            right:'5',
            height: '38%',
            roam: true,
            gridIndex: 1,
            // itemStyle: {
            //     normal: {
            //         areaColor: '#323c48',
            //         borderColor: '#111'
            //     },
            //     emphasis: {
            //         areaColor: '#2a333d'
            //     }
            // }
        },
        grid: [{
            show: true,
            left: 0,
            right: 0,
            top: 0,
            height: 40,
            backgroundColor: '#404a59',
            borderColor:'#404a59',
            z:200
        },{
            show: true,
            left: 0,
            right: 0,
            bottom: 0,
            height: 20,
            backgroundColor: '#404a59',
            borderColor:'#404a59',
            z:200
        }],
        series: [
            {
                name: 'delayTrip',
                type: 'effectScatter',
                coordinateSystem: 'geo',
                symbolSize: 8,
                data: [],
                activeOpacity: 1,
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: false
                    },
                    emphasis: {
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        borderColor: '#fff',
                        color: delay_color,
                    }
                }
            },
            {
                name: 'skipStop',
                type: 'effectScatter',
                coordinateSystem: 'geo',
                symbolSize: 8,
                data: [],
                activeOpacity: 1,
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        show: false
                    },
                    emphasis: {
                        show: true
                    }
                },
                itemStyle: {
                    normal: {
                        borderColor: '#fff',
                        color: skipStop_color,
                    }
                }
            }
        ]
    };

    return option;
}

export {generateInitConfig}

