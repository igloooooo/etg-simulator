let base_color= '#577ceb';
let sectorList = {
    1: ['Lidcombe-Macarthur', 'Marrickville Jct-Macarthur', 'Flemington S Jct-Hornsby', 'Flemington S Jct-Penrith', 'Granville-Macarthur','Marrickville Jct-Sutherland'],
    2: ['Bondi Jct-Erskineville', 'Airport Line','Illawarra Main', 'Illawarra Local', 'Bankstown Line', 'East Hills Main', 'East Hills Local', 'Sutherland-Cronulla','City Inner'],
    3: ['Main A Main', 'Main A Suburban', 'Main A Local', 'City Outer', 'Lidcombe-Olympic Park', 'Flemington-Olympic Park'],
    4: ['Main North', 'North Shore', 'ECRL'],
    5: ['North'],
    6: ['Merrylands-Parramatta', 'Granville-Cabramatta', 'Main South', 'SWRL', 'West Fast Lines', 'West Slow Lines', 'Carlingford','Richmond'],
    7: ['Sutherland-Nowra', 'Port Kembla'],
    8: ['West']

};
let dataDelayTrips = {
    1: [],
    2: [],
    3: [],
    4: [],
    5: [],
    6: [],
    7: [],
    8: []

};
let dataSkipStops = {
    1: [],
    2: [],
    3: [],
    4: [],
    5: [],
    6: [],
    7: [],
    8: []

};
let timeLineData = [1,2,3,4,5,6,7,8];
let borderName = {
    1: 'Freight', 2: 'Illawarra', 3:'Main A', 4:'Main B', 5:'North', 6:'Outer', 7:'South Coast', 8:'West'
};

function updateStatisticData(data) {
    data.forEach((item, index) => {
        dataDelayTrips[item.borderId] = item.delayCount;
        dataSkipStops[item.borderId] = item.skipStopCount;
    })
}

export function sectorStatusInitConfig(data) {
    if(data !== undefined) {
        updateStatisticData(data)
    }
    let option = {
        baseOption: {
            backgroundColor: '#404a59',
            timeline: {
                show: true,
                axisType: 'category',
                tooltip: {
                    show: true,
                    formatter: function(params) {
                        return borderName[params];
                    }
                },
                autoPlay: true,
                currentIndex: 6,
                playInterval: 10000,
                label: {
                    normal: {
                        show: true,
                        interval: 'auto',
                        formatter: function(params){
                            return borderName[params]
                        },
                        color:base_color
                    },
                },
                lineStyle: {
                    color: base_color
                },
                itemStyle: {
                    color: base_color
                },
                controlStyle: {
                    borderColor: base_color
                },
                data: [],
            },
            title: {
                textStyle: {
                    color: '#fff',
                    fontSize: 16,
                },
                subtext: 'sector view',
            },
            legend: {
                data: ['Delays', 'Skip Stops'],
                top: 4,
                right: '20%',
                textStyle: {
                    color: '#fff',
                },
            },
            tooltip: {
                show: true,
                trigger: 'axis',
                formatter: '{b}<br/>{a}: {c}',
                axisPointer: {
                    type: 'shadow',
                }
            },
            grid: [{
                show: false,
                left: '4%',
                top: 60,
                bottom: 60,
                containLabel: true,
                width: '35%',
            }, {
                show: false,
                left: '50.5%',
                top: 80,
                bottom: 60,
            }, {
                show: false,
                right: '4%',
                top: 60,
                bottom: 60,
                containLabel: true,
                width: '35%',
            }, ],

            xAxis: [
                {
                    type: 'value',
                    inverse: true,
                    axisLine: {
                        show: false,
                    },
                    axisTick: {
                        show: false,
                    },
                    position: 'top',
                    axisLabel: {
                        show: true,
                        textStyle: {
                            color: '#B2B2B2',
                            fontSize: 12,
                        },
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#1F2022',
                            width: 1,
                            type: 'solid',
                        },
                    },
                }, {
                    gridIndex: 1,
                    show: false,
                }, {
                    gridIndex: 2,
                    type: 'value',
                    axisLine: {
                        show: false,
                    },
                    axisTick: {
                        show: false,
                    },
                    position: 'top',
                    axisLabel: {
                        show: true,
                        textStyle: {
                            color: '#B2B2B2',
                            fontSize: 12,
                        },
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#1F2022',
                            width: 1,
                            type: 'solid',
                        },
                    },
                }, ],
            series: [],

        },

        options: [],


    };

    for (var i = 0; i < timeLineData.length; i++) {
        option.baseOption.timeline.data.push(timeLineData[i]);
        option.options.push({
            title: {
                text: 'Sydney Train: [' + borderName[timeLineData[i]] + ']',
            },
            yAxis: [{
                type: 'category',
                inverse: true,
                position: 'right',
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    show: false,
                    margin: 8,
                    textStyle: {
                        color: '#9D9EA0',
                        fontSize: 12,
                    },

                },
                data: sectorList[timeLineData[i]],
            }, {
                gridIndex: 1,
                type: 'category',
                inverse: true,
                position: 'left',
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: '#9D9EA0',
                        fontSize: 12,
                    },

                },
                data: sectorList[timeLineData[i]].map(function(value) {
                    return {
                        value: value,
                        textStyle: {
                            align: 'center',
                        }
                    }
                }),
            }, {
                gridIndex: 2,
                type: 'category',
                inverse: true,
                position: 'left',
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    show: false,
                    textStyle: {
                        color: '#9D9EA0',
                        fontSize: 12,
                    },

                },
                data: sectorList[timeLineData[i]],
            }, ],
            series: [{
                name: 'Delays',
                type: 'bar',
                barGap: 20,
                barWidth: 20,
                label: {
                    normal: {
                        show: false,
                    },
                    emphasis: {
                        show: true,
                        position: 'left',
                        offset: [0, 0],
                        textStyle: {
                            color: '#fff',
                            fontSize: 14,
                        },
                    },
                },
                itemStyle: {
                    normal: {
                        color: '#EFF923',
                    },
                    emphasis: {
                        color: '#C2CB07',
                    },
                },
                data: dataDelayTrips[timeLineData[i]],
            },{
                    name: 'Skip Stops',
                    type: 'bar',
                    barGap: 20,
                    barWidth: 20,
                    xAxisIndex: 2,
                    yAxisIndex: 2,
                    label: {
                        normal: {
                            show: false,
                        },
                        emphasis: {
                            show: true,
                            position: 'right',
                            offset: [0, 0],
                            textStyle: {
                                color: '#fff',
                                fontSize: 14,
                            },
                        },
                    },
                    itemStyle: {
                        normal: {
                            color: '#F68989',
                        },
                        emphasis: {
                            color: '#F94646',
                        },
                    },
                    data: dataSkipStops[timeLineData[i]],
                }
            ]
        });
    }

    return option;
}
