const moment = require('moment');

export default {
    state: {
        playTime:moment(),
        playStatus:'Stop',
        startDate:moment(),
        endDate:moment(),
        startTime:moment().startOf("day"),
        endTime:moment().endOf("day"),
        speed:1,
        isPlay:false,
        channelStatus:{
            vm:{
                progress:10,
                sentCount:0
            },
            pt:{
                progress:10,
                sentCount: 0,
                lastRequestTime: moment(),
                requestDate: "2018-08-10"
            },
            fap:{
                progress:10,
                sentCount:0
            },
            far:{
                lastRequestTime: moment(),
                requestDate: "2018-08-10"
            },
            im:{
                progress:10,
                sentCount: 0,
            },
            no:{
                lastRequestTime: null,
                requestDate: "2018-08-10"
            }
        },
        channelConfig:{
            vm: {
                name:'vm',
                dataSetName:'',
                totalCount:0,
                sentCount:0,
                isLoad:false
            },
            pt: {
                name:'pt',
                dataSetName:'',
                totalCount:0,
                sentCount:0,
                isLoad:false
            },
            far: {
                name:'far',
                dataSetName:'',
                totalCount:0,
                sentCount:0,
                isLoad:false
            },
            fap: {
                name:'fap',
                dataSetName:'',
                totalCount:0,
                sentCount:0,
                isLoad:false
            },
            no: {
                name:'no',
                dataSetName:'',
                totalCount:0,
                sentCount:0,
                isLoad:false
            },
            im: {
                name:'im',
                dataSetName:'',
                totalCount:0,
                sentCount:0,
                isLoad:false
            }

        },
        appendETGData:{}
    }


}