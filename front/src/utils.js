function getYesterdaysDate() {
    let date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    return date ;
}

function getTomorrowdaysDate() {
    let date = new Date();
    date.setDate(date.getDate()+1);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(1);
    return date;
}

function generateXaxisData(intervalMin, index, totalIndex, isShow) {
    var startDate = getYesterdaysDate();
    var endDate = getTomorrowdaysDate();
    var data = [];
    var startMin = 0;
    do {
        if(intervalMin == 10) {
            if(startMin % 60 !== 0 && startMin % 30 !== 0) {
                if(totalIndex === 0) {
                    // both
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('start', isShow)
                    });
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('end', isShow)
                    });
                } else if(index === 0) {
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('end', isShow)
                    });
                } else if (index === totalIndex) {
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('start', isShow)
                    });
                } else {
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('', false)
                    });
                }

            }
        } else if (intervalMin == 30) {
            if(startMin % 60 !== 0) {
                if(totalIndex === 0) {
                    // both
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('start', isShow)
                    });
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('end', isShow)
                    });
                } else if(index === 0) {
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('end', isShow)
                    });
                } else if (index === totalIndex) {
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('start', isShow)
                    });
                } else {
                    data.push({
                        xAxis: startDate,
                        label: generateXaxisDataLabel('', false)
                    });
                }
            }
        } else if (intervalMin == 60) {
            if(totalIndex === 0) {
                // both
                data.push({
                    xAxis: startDate,
                    label: generateXaxisDataLabel('start', isShow)
                });
                data.push({
                    xAxis: startDate,
                    label: generateXaxisDataLabel('end', isShow)
                });
            } else if(index === 0) {
                data.push({
                    xAxis: startDate,
                    label: generateXaxisDataLabel('end', isShow)
                });
            } else if (index === totalIndex) {
                data.push({
                    xAxis: startDate,
                    label: generateXaxisDataLabel('start', isShow)
                });
            } else {
                data.push({
                    xAxis: startDate,
                    label: generateXaxisDataLabel('', false)
                });
            }
        }

        startMin = startMin + intervalMin;
        startDate = addMinutes(startDate, intervalMin);
    } while (startDate < endDate);
    return data;
}

function generateXaxisDataLabel(position, isShow) {
    if (!isShow) {
        return {
            show: false
        }
    } else {
        return {
            show: true,
            position: position,
            formatter: function(params) {
                var date = new Date(params.value);
                return date.getHours() + ':' + date.getMinutes();
            }
        }
    }
}

function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
}

function convertToETGDisplayData(data) {
    return data.map(item => {
        if(item[1]<0) {
            return [item[0], '-'];
        } else {
            return item;
        }
    });
}

function checkDataWindow(startOfDay, zoomStart, zoomEnd, currentTimeWindow) {
    // console.log(startOfDay);
    // console.log(zoomStart);
    // console.log(zoomEnd);
    let minDataWindowWidth = 4*60*60*1000;
    let startTime = startOfDay + Math.round(24*60*60*1000*zoomStart/100);
    let endTime = startOfDay + Math.round(24*60*60*1000*zoomEnd/100);
    let isReload = false;
    // console.log(startTime);
    // console.log(endTime);
    // console.log(curStartTime);
    // console.log(curEndTime);
    if(startTime < currentTimeWindow.startTime) {
        isReload = true;
    } else if(endTime > currentTimeWindow.endTime) {
        isReload = true;
    } else if(endTime - startTime > minDataWindowWidth && (zoomEnd - zoomStart) < (currentTimeWindow.end - currentTimeWindow.start)) {
        // check if zoom in to the min data window width & it is zoom in
        console.log(endTime - startTime);
        console.log(minDataWindowWidth);
        isReload = true;
    }
    if(isReload) {
        startTime = startTime - 2*60*60*1000;
        endTime = endTime + 2*60*60*1000;
    }
    return {
        isReload:isReload,
        startTime: startTime,
        endTime:endTime,
        start:zoomStart,
        end:zoomEnd
    }
}

function initDataWindow(startOfDay) {
    return {
        startTime: startOfDay + Math.round(24*60*60*1000*40/100) - 2*60*60*1000,
        endTime:startOfDay + Math.round(24*60*60*1000*55/100) + 2*60*60*1000,
        start:40,
        end:55
    }
}

function groupByProperty(objectArray, property) {
    return objectArray.reduce(function (acc, obj) {
        let key = obj[property];
        if (!acc[key]) {
            acc[key] = [];
        }
        acc[key].push(obj);
        return acc;
    }, {});
}

/* the echarts 4.1.0 has a bug for update map data, so we have to update all series*/
function updateSeriesData(series, name, data) {
    let index = series.findIndex(function(e){
        return e.name === name;
    });
    if(index < 0) {
        console.log("wrong name: " + name);
    }
    series[index].data = data;
    return series
}

function findSeriesIndexByName(series, name) {
    return series.findIndex(function(e){
        return e.name === name;
    });
}

function updateNodeLabel(series, name, isShow) {
    let index = series.findIndex(function(e){
        return e.name === name;
    });
    series[index].label.show = isShow;
    return series;
}

export {getYesterdaysDate, getTomorrowdaysDate, generateXaxisData, convertToETGDisplayData, checkDataWindow, initDataWindow, groupByProperty, updateSeriesData, findSeriesIndexByName, updateNodeLabel}